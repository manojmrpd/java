package com.java.map.hashmap.racecondition;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class HashMapRaceCondition {

    public static void main(String[] args) throws InterruptedException {

        Map<String, Integer> map = new HashMap<>();
        int threadSize = 5;
        ExecutorService executorService = Executors.newFixedThreadPool(threadSize);

        for (int i = 0; i < threadSize; i++) {
            executorService.submit(() -> {
                for (int j = 0; j < 100; j++) {
                    map.put(Thread.currentThread().getName() + j, j);
                }
            });
        }
        executorService.shutdown();
        if (!executorService.isTerminated()) {
            Thread.sleep(2000);
        }
        System.out.println(map.size());
    }
}
