package com.java.map.hashmap.racecondition;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class HashMapConcurrentModificationException {

    public static void main(String[] args) {

        Map<Integer, String> map = new ConcurrentHashMap<>();
        map.put(9, "RJ");
        map.put(4, "SJ");
        map.put(7, "VJ");
        map.put(3, "DJ");

        Iterator<Map.Entry<Integer, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<Integer, String> next = iterator.next();
            map.put(5, "MJ");
        }
        System.out.println(map);
    }
}
