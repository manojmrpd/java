package com.java.map.hashmap.operation;

import java.util.HashMap;
import java.util.Map;

public class HashMapExample {

    public static void main(String[] args) {
        // Create a HashMap
        HashMap<String, Integer> map = new HashMap<>();

        // Add key-value pairs to the HashMap
        map.put("apple", 10);
        map.put("banana", 20);
        map.put("orange", 30);

        // Retrieve a value based on a key
        Integer value = map.get("apple");
        System.out.println("Value for key 'apple': " + value);

        // Check if a key exists
        boolean hasKey = map.containsKey("banana");
        System.out.println("HashMap contains key 'banana': " + hasKey);

        // Check if a value exists
        boolean hasValue = map.containsValue(30);
        System.out.println("HashMap contains value 30: " + hasValue);

        // Iterate over key-value pairs using entrySet
        System.out.println("Entries in the HashMap:");
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }

        // Remove a key-value pair
        map.remove("orange");
        System.out.println("Entries after removing 'orange':");
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }

        // Handle null keys and values
        map.put(null, 40);
        map.put("grape", null);
        System.out.println("Entries after adding null key and value:");
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
    }
}
