package com.java.map.hashtable.operations;

import java.util.Hashtable;
import java.util.Enumeration;

public class HashtableExample {

    public static void main(String[] args) {
        // Create a Hashtable
        Hashtable<String, Integer> hashtable = new Hashtable<>();

        // Add key-value pairs to the Hashtable
        hashtable.put("apple", 10);
        hashtable.put("banana", 20);
        hashtable.put("orange", 30);

        // Retrieve a value based on a key
        Integer value = hashtable.get("apple");
        System.out.println("Value for key 'apple': " + value);

        // Check if a key exists
        boolean hasKey = hashtable.containsKey("banana");
        System.out.println("Hashtable contains key 'banana': " + hasKey);

        // Check if a value exists
        boolean hasValue = hashtable.containsValue(30);
        System.out.println("Hashtable contains value 30: " + hasValue);

        // Iterate over keys using Enumeration
        Enumeration<String> keys = hashtable.keys();
        System.out.println("Keys in hashtable:");
        while (keys.hasMoreElements()) {
            System.out.println(keys.nextElement());
        }

        // Remove a key-value pair
        hashtable.remove("orange");

        // Iterate over key-value pairs
        System.out.println("Remaining entries in hashtable:");
        hashtable.forEach((k, v) -> System.out.println(k + ": " + v));
    }
}