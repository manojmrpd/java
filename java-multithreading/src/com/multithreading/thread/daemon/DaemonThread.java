package com.multithreading.thread.daemon;

public class DaemonThread {

    public static void main(String[] args) {
        Thread t1 = new Thread(new MyRunnable(), "Thread-1");
        t1.setDaemon(true); //Marking the thread as daemon
        t1.start(); //Start the daemon thread
        System.out.println("Main thread completes.");
    }
}

class MyRunnable implements Runnable {

    @Override
    public void run() {
        while (true) {
            System.out.println(Thread.currentThread().getName() + " is running.");
            try {
                Thread.sleep(1000); //Simulate some work
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
