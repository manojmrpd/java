package com.multithreading.thread.run;

class MyRunnable implements Runnable {
    public void run() {
        for (int i = 0; i < 5; i++) {
            final long threadId = Thread.currentThread().getId();
            final String threadName = Thread.currentThread().getName();
            System.out.println(threadName + "-" + threadId + " Value is: " + i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static void main(String[] args) {
        Thread t1 = new Thread(new MyRunnable());
        Thread t2 = new Thread(new MyRunnable());
        t1.start();
        t2.start();
    }
}
