package com.multithreading.thread.synchronization.blocks;

public class ThreadSynchronizedBlocks {
    private int balance = 0;

    public void deposit(int amount) {
        synchronized (this) {
            balance += amount;
            System.out.println(Thread.currentThread().getName() + " deposited " + amount + " ,balance: " + balance);
        }
    }

    public void withdraw(int amount) {
        synchronized (this) {
            if (balance >= amount) {
                balance -= amount;
                System.out.println(Thread.currentThread().getName() + " withdraw " + amount + " ,balance: " + balance);
            } else {
                System.out.println(Thread.currentThread().getName() + " tried to withdraw " + amount + " ,but only " + balance + " balance is available");
            }
        }
    }

    public int getBalance() {
        return balance;
    }

    public static void main(String[] args) {
        ThreadSynchronizedBlocks bankAccount = new ThreadSynchronizedBlocks();

        Thread thread1 = new Thread(() -> {
            for (int i = 1; i <= 10; i++) {
                bankAccount.deposit(10);
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }, "Thread-1");

        Thread thread2 = new Thread(() -> {
            for (int i = 1; i <= 10; i++) {
                bankAccount.withdraw(10);
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }, "Thread-1");
        thread1.start();
        thread2.start();

        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        System.out.println("Final balance: " + bankAccount.getBalance());

    }
}
