package com.multithreading.thread.volatiles;

public class VolatileCounterExample {
    private volatile int counter = 0;

    public void increment() {
        counter++;
    }

    public int getCounter() {
        return counter;
    }

    public static void main(String[] args) {
        VolatileCounterExample example = new VolatileCounterExample();

        // Thread to increment the counter
        Thread incrementThread = new Thread(() -> {
            for (int i = 0; i < 1000; i++) {
                example.increment();
            }
        });

        // Thread to read the counter
        Thread readThread = new Thread(() -> {
            while (example.getCounter() < 1000) {
                // Busy-wait until the counter reaches 1000
            }
            System.out.println("Counter reached 1000.");
        });

        // Start threads
        incrementThread.start();
        readThread.start();
    }
}
