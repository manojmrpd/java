package com.multithreading.thread.sleep;

public class ThreadSleepNanos {
    public static void main(String[] args) {
        Thread t1 = new Thread(new MyRunnableInstance(), "Thread-1");
        t1.start();
    }
}

class MyRunnableInstance implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            System.out.println(Thread.currentThread().getName() + " - Count: " + i);
            try {
                Thread.sleep(1000, 500000); // Sleep for 1 second and 500,000 nanoseconds
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
