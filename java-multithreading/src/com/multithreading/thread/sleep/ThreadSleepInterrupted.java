package com.multithreading.thread.sleep;

public class ThreadSleepInterrupted {

    public static void main(String[] args) {
        Thread t1 = new Thread(new MyRunnable(), "Thread-1");
        t1.start();
        try {
            Thread.sleep(3000); // Main thread sleeps for 3 seconds
            t1.interrupt(); // Interrupt the other thread after 3 seconds
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

class MyRunnable implements Runnable {
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println(Thread.currentThread().getName() + " - Count: " + i);
            try {
                Thread.sleep(1000); // Sleep for 1 second
            } catch (InterruptedException e) {
                System.out.println(Thread.currentThread().getName() + " was interrupted");
                return; // Exit the loop if interrupted
            }
        }
    }
}