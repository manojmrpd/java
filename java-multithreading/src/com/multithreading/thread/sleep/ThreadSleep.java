package com.multithreading.thread.sleep;

public class ThreadSleep implements Runnable {

    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            long threadId = Thread.currentThread().getId();
            String threadName = Thread.currentThread().getName();
            System.out.println(threadName + "-" + threadId + " Value is: " + i);
            try {
                Thread.sleep(1000); // Sleep for 1 second
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        Thread t1 = new Thread(new ThreadSleep());
        Thread t2 = new Thread(new ThreadSleep());
        t1.start();
        t2.start();
    }


}
