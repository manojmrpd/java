package com.multithreading.thread.start;

class MyThread extends Thread {

    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            final long threadId = Thread.currentThread().getId();
            final String threadName = Thread.currentThread().getName();
            System.out.println(threadName + "-" + threadId + " Value is: " + i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static void main(String[] args) {
        MyThread t1 = new MyThread();
        MyThread t2 = new MyThread();
        t1.start();
        t2.start();
    }
}