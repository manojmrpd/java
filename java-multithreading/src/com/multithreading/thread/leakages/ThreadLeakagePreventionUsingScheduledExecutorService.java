package com.multithreading.thread.leakages;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ThreadLeakagePreventionUsingScheduledExecutorService {

    public static void main(String[] args) {

        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(5);

        scheduler.scheduleAtFixedRate(() -> {
            System.out.println("Scheduled task running...");
        }, 0, 1, TimeUnit.SECONDS);

        // Schedule the shutdown after 10 seconds
        scheduler.schedule(() -> {
            scheduler.shutdown();
            try {
                if (!scheduler.awaitTermination(5, TimeUnit.SECONDS)) {
                    scheduler.shutdownNow();
                }
            } catch (InterruptedException e) {
                scheduler.shutdownNow();
            }
        }, 10, TimeUnit.SECONDS);
    }

}
