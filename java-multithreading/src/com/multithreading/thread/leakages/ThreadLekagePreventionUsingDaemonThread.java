package com.multithreading.thread.leakages;

public class ThreadLekagePreventionUsingDaemonThread {
    public static void main(String[] args) {
        Thread t1 = new Thread(new MyDeamonRunnable(), "Thread-1");
        t1.setDaemon(true);
        t1.start();
        System.out.println("Main thread completes.");
    }
}

class MyDeamonRunnable implements Runnable {

    @Override
    public void run() {
        while (true) {
            System.out.println(Thread.currentThread().getName() + " - Count: " + Thread.currentThread().getPriority());
            try {
                Thread.sleep(1000); // Sleep for 1 second
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                break;
            }
        }
    }

}
