package com.multithreading.thread.leakages;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ThreadLeakagePreventionUsingTryWithResource {

    public static void main(String[] args) {
        Thread thread = new Thread(() -> {
            try (BufferedReader reader = new BufferedReader(new FileReader("file.txt"))) {
                String line;
                while ((line = reader.readLine()) != null) {
                    System.out.println(line);
                }
            } catch (IOException e) {
                System.err.println("Exception occurred while reading file: " + e.getMessage());
            }
        });
        thread.start();
    }

}
