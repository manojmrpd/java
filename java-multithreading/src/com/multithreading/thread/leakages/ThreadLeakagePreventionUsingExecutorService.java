package com.multithreading.thread.leakages;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ThreadLeakagePreventionUsingExecutorService {

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        for (int i = 0; i < 10; i++) {
            executorService.submit(new MyThread());
        }
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(60, TimeUnit.SECONDS)) {
                executorService.shutdownNow();
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}

class MyThread implements Runnable {
    @Override
    public void run() {
        try {
            Thread.sleep(1000); // Sleep for 1 second
            System.out.println(Thread.currentThread().getName() + " completes.");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
