package com.multithreading.thread.leakages;

public class ThreadLeakagePreventionAvoidingInfiniteLoops {

    private static boolean running = true;

    public static void main(String[] args) {
        Thread worker = new Thread(() -> {
            while (running) {
                System.out.println("Worker thread running...");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        });

        worker.start();

        // Let the worker thread run for 5 seconds
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

        running = false; // Proper exit condition for the loop
    }


}
