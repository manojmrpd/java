package com.multithreading.thread.leakages;

public class ThreadLeakageCauses {

    public static void main(String[] args) throws InterruptedException {
        while (true) {
            Thread t1 = new Thread(new MyRunnable(), "Thread-1");
            t1.start();
            t1.join();
        }
    }
}

class MyRunnable implements Runnable {
    @Override
    public void run() {
        try {
            Thread.sleep(1000); // Sleep for 1 second
            System.out.println(Thread.currentThread().getName() + " completes.");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
