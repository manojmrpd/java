package com.multithreading.thread.leakages;

public class ThreadLeakagesPreventionWithExceptionHandling {
    public static void main(String[] args) {
        Thread t1 = new Thread(new MyRunnableInstance(), "Thread-1");
        t1.setUncaughtExceptionHandler((t, e) -> System.out.println("Exception in Thread " + t.getName() + ": " + e.getMessage()));
        t1.start();
    }
}

class MyRunnableInstance implements Runnable {
    @Override
    public void run() {
        try {
            Thread.sleep(1000); // Sleep for 1 second
            System.out.println(Thread.currentThread().getName() + " completes.");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

