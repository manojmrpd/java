package com.multithreading.thread.lifecycle;

public class WaitingState {

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(() -> {
            synchronized (WaitingState.class) {
                try {
                    Thread.sleep(1000);
                    WaitingState.class.wait(); // t1 moves to Waiting state
                    System.out.println(Thread.currentThread().getName() + " state is " + Thread.currentThread().getState());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t1.start();

        Thread.sleep(1000); // Ensure t1 is waiting
        synchronized (WaitingState.class) {
            WaitingState.class.notify(); // t1 moves back to Runnable state
            System.out.println(Thread.currentThread().getName() + " state is " + Thread.currentThread().getState());
        }
    }
}
