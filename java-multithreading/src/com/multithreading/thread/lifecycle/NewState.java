package com.multithreading.thread.lifecycle;

public class NewState implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            final long threadId = Thread.currentThread().getId();
            final String threadName = Thread.currentThread().getName();
            System.out.println(threadName + "-" + threadId + " Value is: " + i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static void main(String[] args) {
        Thread t1 = new Thread(new NewState()); //Thread is in new state
        Thread t2 = new Thread(new NewState());
        System.out.println(t1.getState()); //NEW
    }
}


