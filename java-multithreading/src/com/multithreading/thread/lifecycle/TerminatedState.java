package com.multithreading.thread.lifecycle;

public class TerminatedState {

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(() -> {
            System.out.println("Thread state is " + Thread.currentThread().getState()); //RUNNABLE
        });
        t1.start();
        t1.join(); // Wait for t1 to die
        System.out.println("Thread state after join() is " +t1.getState()); //TERMINATED
    }
}
