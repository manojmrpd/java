package com.multithreading.thread.lifecycle;

public class RunnableState implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            final long threadId = Thread.currentThread().getId();
            final String threadName = Thread.currentThread().getName();
            System.out.println(threadName + "-" + threadId + " Value is: " + i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static void main(String[] args) {
        Thread t1 = new Thread(new RunnableState());
        Thread t2 = new Thread(new RunnableState());
        t1.start();
        System.out.println(t1.getState()); //RUNNABLE
        t2.start();
        System.out.println(t2.getState()); //RUNNABLE
    }
}
