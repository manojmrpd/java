package com.multithreading.thread.lifecycle;

public class BlockedState implements Runnable {
    @Override
    public void run() {
        synchronized (this) {
            for (int i = 0; i < 5; i++) {
                final long threadId = Thread.currentThread().getId();
                final String threadName = Thread.currentThread().getName();
                System.out.println(threadName + "-" + threadId + " Value is: " + i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(new BlockedState());
        Thread t2 = new Thread(new BlockedState());
        t1.start();
        Thread.sleep(1000);
        t2.start();
        System.out.println(t1.getState()); //RUNNABLE
        System.out.println(t2.getState()); //BLOCKED
    }
}
