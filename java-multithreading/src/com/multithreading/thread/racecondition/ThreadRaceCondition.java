package com.multithreading.thread.racecondition;

public class ThreadRaceCondition {

    private static int counter = 0;

    public static void main(String[] args) throws InterruptedException {

        Thread thread1 = new Thread(() -> {
            increment();
        });
        Thread thread2 = new Thread(() -> {
            increment();
        });
        thread1.start();
        thread2.start();
        System.out.println("Final counter value is: " + getCount());
    }

    private static void increment() {
        for (int i = 1; i <= 1000; i++) {
            counter++;
        }
    }

    private static int getCount() {
        return counter;
    }
}
