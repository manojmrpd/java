package com.multithreading.thread.racecondition;

import java.util.concurrent.atomic.AtomicInteger;

public class PreventRaceConditionUsingAtomicOperations {
    private static AtomicInteger count = new AtomicInteger(0);

    public static void main(String[] args) {
        Thread thread1 = new Thread(() -> {
            increment();
        });
        Thread thread2 = new Thread(() -> {
            increment();
        });
        thread1.start();
        thread2.start();
        System.out.println("Final counter value is: " + getCount());
    }

    private synchronized static void increment() {
        for (int i = 1; i <= 1000; i++) {
            count.incrementAndGet();
        }
    }

    private synchronized static int getCount() {
        return count.get();
    }
}
