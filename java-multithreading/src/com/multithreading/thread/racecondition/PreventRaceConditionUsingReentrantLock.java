package com.multithreading.thread.racecondition;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class PreventRaceConditionUsingReentrantLock {

    private static Lock lock = new ReentrantLock();
    private static int counter = 0;

    public static void main(String[] args) throws InterruptedException {

        Thread thread1 = new Thread(() -> {
            increment();
        });
        Thread thread2 = new Thread(() -> {
            increment();
        });
        thread1.start();
        thread2.start();
        System.out.println("Final counter value is: " + getCount());
    }

    private static void increment() {
        lock.lock();
        try {
            for (int i = 1; i <= 1000; i++) {
                counter++;
            }
        } finally {
            lock.unlock();
        }
    }

    private static int getCount() {
        lock.lock();
        try {
            return counter;
        } finally {
            lock.unlock();
        }
    }
}
