package com.multithreading.thread.threadlocal;

import java.util.concurrent.TimeUnit;

public class ThreadLocalExample {
    private static ThreadLocal<Integer> threadLocal = ThreadLocal.withInitial(() -> 0);

    public static void main(String[] args) {
        // Create and start multiple threads
        for (int i = 0; i < 3; i++) {
            Thread thread = new Thread(() -> {
                // Increment and print the thread-local variable
                int value = threadLocal.get() + 1;
                threadLocal.set(value);
                System.out.println("Thread " + Thread.currentThread().getId() + ": " + value);

                // Simulate some computation time
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
            thread.start();
        }
    }
}
