package com.multithreading.thread.contextswitching;

public class ThreadContextSwitching {

    public static void main(String[] args) {
        Thread t1 = new Thread(new MyTask(), "Thread-1");
        Thread t2 = new Thread(new MyTask(), "Thread-2");

        t1.start();
        t2.start();
    }
}

class MyTask implements Runnable {
    public void run() {
        for (int i = 0; i < 5; i++) {
            System.out.println(Thread.currentThread().getName() + " is running: " + i);
            try {
                Thread.sleep(500); // Simulate some work and allow context switch
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
