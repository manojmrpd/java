package com.multithreading.thread.livelock;

public class ThreadLiveLock {
    static class Friend {
        private final String name;
        private final boolean polite;

        Friend(String name, boolean polite) {
            this.name = name;
            this.polite = polite;
        }

        public String getName() {
            return name;
        }

        public boolean isPolite() {
            return polite;
        }

        public void bow(Friend bower) {
            while (polite) {
                System.out.println(name + ": After you, " + bower.getName());
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                this.bowBack(this);
            }

        }

        public void bowBack(Friend bower) {
            System.out.println(name + ": You first, " + bower.getName());
            try {
                Thread.sleep(100); // Simulate some work
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static void main(String[] args) {
        Friend tom = new Friend("tom", true);
        Friend jerry = new Friend("jerry", true);

        Thread thread1 = new Thread(() -> tom.bow(jerry), "Thread-1");
        Thread thread2 = new Thread(() -> jerry.bow(tom), "Thread-1");
        thread1.start();
        thread2.start();
    }
}
