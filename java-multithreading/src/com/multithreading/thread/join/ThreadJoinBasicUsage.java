package com.multithreading.thread.join;

public class ThreadJoinBasicUsage {
    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(new MyRunnable(), "Thread-1");
        t1.start();
        t1.join();
        System.out.println("Main thread completes.");
    }
}

class MyRunnable implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            final String threadName = Thread.currentThread().getName();
            System.out.println(threadName + " Value is: " + i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println(Thread.currentThread().getName() + " completes.");
        }

    }
}
