package com.multithreading.thread.join;

public class ThreadJoinTimeout {
    public static void main(String[] args) {
        Thread t1 = new Thread(new MyThread(), "Thread-1");
        t1.start();
        try {
            t1.join(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("Main thread completes.");

    }
}

class MyThread implements Runnable {
    @Override
    public void run() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println(Thread.currentThread().getName() + " completes.");
    }
}
