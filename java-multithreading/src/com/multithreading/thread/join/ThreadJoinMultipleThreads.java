package com.multithreading.thread.join;

public class ThreadJoinMultipleThreads {
    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(new MyThreading(), "Thread-1");
        Thread t2 = new Thread(new MyThreading(), "Thread-2");
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        System.out.println("Main thread completes.");
    }
}

class MyThreading implements Runnable {
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " is running.");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println(Thread.currentThread().getName() + " completes.");
    }
}
