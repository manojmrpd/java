package com.multithreading.thread.concurrency.cyclicbarrier;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class BankAccount {
    private double balance;

    public BankAccount(double initialBalance) {
        this.balance = initialBalance;
    }

    public synchronized void deposit(double amount) {
        if (amount > 0) {
            balance += amount;
            System.out.println(Thread.currentThread().getName() + " deposited " + amount + ", new balance: " + balance);
        }
    }

    public synchronized void withdraw(double amount) {
        if (amount > 0 && amount <= balance) {
            balance -= amount;
            System.out.println(Thread.currentThread().getName() + " withdrew " + amount + ", new balance: " + balance);
        } else {
            System.out.println(Thread.currentThread().getName() + " attempted to withdraw " + amount + ", but insufficient funds. Current balance: " + balance);
        }
    }

    public double getBalance() {
        return balance;
    }

    public static void main(String[] args) {
        BankAccount account = new BankAccount(1000.0);
        int numberOfThreads = 4; // Number of transaction threads
        CyclicBarrier barrier = new CyclicBarrier(numberOfThreads, () -> {
            // Action to be performed when all threads reach the barrier
            System.out.println("All threads have reached the barrier. Current balance: " + account.getBalance());
        });

        Runnable depositTask = () -> {
            try {
                for (int i = 0; i < 3; i++) {
                    account.deposit(100);
                    barrier.await(); // Wait for other threads to reach the barrier
                }
            } catch (InterruptedException | BrokenBarrierException e) {
                e.printStackTrace();
            }
        };

        Runnable withdrawTask = () -> {
            try {
                for (int i = 0; i < 3; i++) {
                    account.withdraw(50);
                    barrier.await(); // Wait for other threads to reach the barrier
                }
            } catch (InterruptedException | BrokenBarrierException e) {
                e.printStackTrace();
            }
        };

        // Creating and starting transaction threads
        Thread t1 = new Thread(depositTask, "Thread-1");
        Thread t2 = new Thread(withdrawTask, "Thread-2");
        Thread t3 = new Thread(depositTask, "Thread-3");
        Thread t4 = new Thread(withdrawTask, "Thread-4");

        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }
}

