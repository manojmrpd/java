package com.multithreading.thread.concurrency.cyclicbarrier;

import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class CyclicBarrierExample {

    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(4);
        CyclicBarrier barrier = new CyclicBarrier(3, new Runnable() {
            @Override
            public void run() {
                // This task will be executed once all threads reach the barrier
                System.out.println("All threads have reached the barrier and are now released.");
            }
        });
        for (int i = 1; i <= 3; i++) {
            executorService.submit(() -> {
                try {
                    System.out.println(Thread.currentThread().getName() + " is waiting at the barrier.");
                    barrier.await(); // Wait at the barrier
                    System.out.println(Thread.currentThread().getName() + " has crossed the barrier.");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
        // Shutdown the thread pool
        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.MINUTES);
    }
}

