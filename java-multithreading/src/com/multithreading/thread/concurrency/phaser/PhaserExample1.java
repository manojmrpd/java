package com.multithreading.thread.concurrency.phaser;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Phaser;

public class PhaserExample1 {

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(4);
        Phaser phaser = new Phaser(3);

        for (int i = 1; i <= 3; i++) {
            executorService.submit(() -> {
                System.out.println(Thread.currentThread().getName() + " is running.");
                phaser.arrive();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                System.out.println(Thread.currentThread().getName() + " is completed.");
            });
        }
        phaser.awaitAdvance(0);
        System.out.println(Thread.currentThread().getName() + " started");
    }
}
