package com.multithreading.thread.concurrency.phaser;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Phaser;

public class PhaserExample2 {
    
    public static void main(String[] args) throws InterruptedException {

        ExecutorService executorService = Executors.newFixedThreadPool(4);
        Phaser phaser = new Phaser(3);

        for (int i = 1; i <= 3; i++) {
            executorService.submit(() -> {
                System.out.println(Thread.currentThread().getName() + " is running");
                phaser.arriveAndAwaitAdvance();
                System.out.println(Thread.currentThread().getName() + " reached the phase " + phaser.getPhase());
            });
        }
        Thread.sleep(1000);
        System.out.println(Thread.currentThread().getName() + " started");
    }
}
