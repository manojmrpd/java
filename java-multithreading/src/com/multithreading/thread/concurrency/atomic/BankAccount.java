package com.multithreading.thread.concurrency.atomic;

import java.util.concurrent.atomic.AtomicLong;

public class BankAccount {

    private final AtomicLong balance;

    public BankAccount(long initialBalance) {
        this.balance = new AtomicLong(initialBalance);
    }

    //Get balance
    public long getBalance() {
        return balance.get();
    }

    public void deposit(long amount) {
        if (amount > 0) {
            balance.addAndGet(amount);
            System.out.println(Thread.currentThread().getName() + " deposited: " + amount + " Current balance: " + balance.get());
        }
    }

    public boolean withdraw(long amount) {
        if (amount > 0) {
            long currentBalance;
            do {
                currentBalance = balance.get();
                if (currentBalance < amount) {
                    System.out.println(Thread.currentThread().getName() + " tried to withdraw " + amount + ", but only " + currentBalance + " is available. Withdrawal failed.");
                    return false;
                }
            } while (balance.compareAndSet(currentBalance, currentBalance - amount));
            System.out.println(Thread.currentThread().getName() + " withdrew " + amount + ". Current balance: " + balance.get());
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        BankAccount account = new BankAccount(1000);
        Runnable depositTask = () -> {
            for (int i = 0; i < 5; i++) {
                account.deposit(100);
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        };
        Runnable withdrawTask = () -> {
            for (int i = 0; i < 5; i++) {
                account.withdraw(150);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        };
        Thread depositThread1 = new Thread(depositTask);
        Thread depositThread2 = new Thread(depositTask);
        Thread withdrawThread1 = new Thread(withdrawTask);
        Thread withdrawThread2 = new Thread(withdrawTask);

        depositThread1.start();
        depositThread2.start();
        withdrawThread1.start();
        withdrawThread2.start();
    }
}
