package com.multithreading.thread.concurrency.future.futuretask;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class FutureTaskExample {
    
    public static void main(String[] args) {
        // Create a Callable task
        Callable<Integer> task = () -> {
            Thread.sleep(2000); // Simulate some computation
            return 42; // Return a result
        };

        // Create a FutureTask with the Callable task
        FutureTask<Integer> futureTask = new FutureTask<>(task);

        // Submit the FutureTask to an ExecutorService for execution (in a separate thread)
        Thread thread = new Thread(futureTask);
        thread.start();

        // Do other work while the task is running asynchronously
        System.out.println("Doing other work...");

        try {
            // Retrieve the result of the task (blocking until it's complete)
            int result = futureTask.get();
            System.out.println("Task result: " + result);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
}