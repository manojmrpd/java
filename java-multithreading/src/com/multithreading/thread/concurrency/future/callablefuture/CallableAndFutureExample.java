package com.multithreading.thread.concurrency.future.callablefuture;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class CallableAndFutureExample {
    
    public static void main(String[] args) {
        // Create a Callable task
        Callable<Integer> task = () -> {
            Thread.sleep(2000); // Simulate some computation
            return 42; // Return a result
        };

        // Create a thread pool
        ExecutorService executor = Executors.newFixedThreadPool(1);

        // Submit the Callable task to the thread pool
        Future<Integer> future = executor.submit(task);

        // Do other work while the task is running asynchronously
        System.out.println("Doing other work...");

        try {
            // Retrieve the result of the task (blocking until it's complete)
            int result = future.get();
            System.out.println("Task result: " + result);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Shutdown the executor service
        executor.shutdown();
    }
}
