package com.multithreading.thread.concurrency.future.completablefuture;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class BankAccountExample {
    private double balance;

    public BankAccountExample(double initialBalance) {
        this.balance = initialBalance;
    }

    // Asynchronous deposit operation
    public CompletableFuture<Void> deposit(double amount) {
        return CompletableFuture.runAsync(() -> {
            // Simulate processing time
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            balance += amount;
            System.out.println("Deposited: " + amount);
        });
    }

    // Asynchronous withdraw operation
    public CompletableFuture<Double> withdraw(double amount) {
        return CompletableFuture.supplyAsync(() -> {
            // Simulate processing time
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (balance >= amount) {
                balance -= amount;
                System.out.println("Withdrawn: " + amount);
                return amount;
            } else {
                throw new IllegalArgumentException("Insufficient balance");
            }
        });
    }

    public double getBalance() {
        return balance;
    }

    public static void main(String[] args) {
        int initialBalance = 1000;
        BankAccountExample account = new BankAccountExample(initialBalance);
        System.out.println("Initial Balance: " + initialBalance);
        // Deposit $500 asynchronously
        CompletableFuture<Void> depositFuture = account.deposit(500);

        // Withdraw $200 asynchronously
        CompletableFuture<Double> withdrawFuture = account.withdraw(200);

        // Chain deposit and withdraw operations
        depositFuture.thenComposeAsync(v -> withdrawFuture).thenAcceptAsync(amount -> {
            System.out.println("New Balance: " + account.getBalance());
            System.out.println("Completed withdrawal of: " + amount);
        }).exceptionally(throwable -> {
            System.out.println("Error occurred: " + throwable.getMessage());
            return null;
        });

        // Wait for all operations to complete
        try {
            CompletableFuture.allOf(depositFuture, withdrawFuture).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
}