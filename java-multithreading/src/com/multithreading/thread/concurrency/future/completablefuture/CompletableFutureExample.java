package com.multithreading.thread.concurrency.future.completablefuture;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class CompletableFutureExample {

    public static void main(String[] args) {
        // Example 1: Asynchronous computation
        CompletableFuture<Integer> future1 = CompletableFuture.supplyAsync(() -> {
            // Simulate some computation
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return 42; // Return a result
        });

        // Example 2: Chaining and composition
        CompletableFuture<String> future2 = future1.thenApplyAsync(result -> {
            // Transform the result asynchronously
            return "Result: " + result;
        });

        // Example 3: Exception handling
        CompletableFuture<Void> future3 = future2.thenAcceptAsync(result -> {
            // Process the result asynchronously
            System.out.println(result);
            throw new RuntimeException("Error occurred!");
        }).exceptionally(throwable -> {
            // Handle exception asynchronously
            System.out.println("Exception occurred: " + throwable.getMessage());
            return null;
        });

        // Example 4: Combining results
        CompletableFuture<Void> combinedFuture = CompletableFuture.allOf(future1, future2, future3);

        try {
            // Wait for all asynchronous computations to complete
            combinedFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
}