package com.multithreading.thread.concurrency.reentrantlock;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;

public class ReentrantLockFairExample {

    public static void main(String[] args) {

        ExecutorService executorService = Executors.newFixedThreadPool(4);
        ReentrantLock lock = new ReentrantLock(true);
        for (int i = 1; i <= 3; i++) {
            executorService.submit(() -> {
                try {
                    lock.lock();
                    System.out.println(Thread.currentThread().getName() + " booking started..");
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } finally {
                    lock.unlock();
                    System.out.println(Thread.currentThread().getName() + " booking completed");
                }
            });
        }
        executorService.shutdown();
    }
}
