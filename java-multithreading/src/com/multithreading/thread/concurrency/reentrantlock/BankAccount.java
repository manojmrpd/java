package com.multithreading.thread.concurrency.reentrantlock;

import java.util.concurrent.locks.ReentrantLock;

public class BankAccount {
    private double balance;
    private final ReentrantLock lock = new ReentrantLock(true);

    public BankAccount(double initialBalance) {
        this.balance = initialBalance;
    }

    // Method to deposit money
    public void deposit(double amount) {
        lock.lock();
        try {
            if (amount > 0) {
                balance += amount;
                System.out.println(Thread.currentThread().getName() + " deposited " + amount + ", new balance: " + balance);
            }
        } finally {
            lock.unlock();
        }
    }

    // Method to withdraw money
    public void withdraw(double amount) {
        lock.lock();
        try {
            if (amount > 0 && amount <= balance) {
                balance -= amount;
                System.out.println(Thread.currentThread().getName() + " withdrew " + amount + ", new balance: " + balance);
            } else {
                System.out.println(Thread.currentThread().getName() + " attempted to withdraw " + amount + ", but insufficient funds. Current balance: " + balance);
            }
        } finally {
            lock.unlock();
        }
    }

    // Method to check balance
    public double getBalance() {
        lock.lock();
        try {
            return balance;
        } finally {
            lock.unlock();
        }
    }

    public static void main(String[] args) {
        BankAccount account = new BankAccount(1000.0);

        Runnable depositTask = () -> {
            for (int i = 0; i < 5; i++) {
                account.deposit(100);
                try {
                    Thread.sleep(100); // Simulate time taken to perform deposit
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        Runnable withdrawTask = () -> {
            for (int i = 0; i < 5; i++) {
                account.withdraw(50);
                try {
                    Thread.sleep(150); // Simulate time taken to perform withdrawal
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        Thread t1 = new Thread(depositTask, "Thread-1");
        Thread t2 = new Thread(withdrawTask, "Thread-2");
        Thread t3 = new Thread(depositTask, "Thread-3");
        Thread t4 = new Thread(withdrawTask, "Thread-4");

        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }
}
