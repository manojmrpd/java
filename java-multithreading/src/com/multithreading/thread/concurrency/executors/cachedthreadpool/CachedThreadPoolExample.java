package com.multithreading.thread.concurrency.executors.cachedthreadpool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CachedThreadPoolExample {

    public static void main(String[] args) {
        // Create a CachedThreadPool
        ExecutorService executor = Executors.newCachedThreadPool();

        // Submit tasks to the executor service
        for (int i = 0; i < 10; i++) {
            Runnable task = new Task("Task-" + i);
            executor.execute(task);
        }

        // Shutdown the executor service after all tasks are completed
        executor.shutdown();
    }

    static class Task implements Runnable {
        private final String name;

        public Task(String name) {
            this.name = name;
        }

        @Override
        public void run() {
            System.out.println("Executing task: " + name + " on thread: " + Thread.currentThread().getName());
            // Perform task execution logic
        }
    }
}