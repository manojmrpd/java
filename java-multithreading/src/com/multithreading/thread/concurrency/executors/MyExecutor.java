package com.multithreading.thread.concurrency.executors;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class MyExecutor implements Runnable {
    public void run() {
        for (int i = 0; i < 5; i++) {
            long threadId = Thread.currentThread().getId();
            String threadName = Thread.currentThread().getName();
            System.out.println(threadName + "-" + threadId + " Value is: " + i);
            try {
                Thread.sleep(1000); // Sleep for 1 second
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        ExecutorService executor = Executors.newFixedThreadPool(2); // Create a thread pool with 2 threads
        executor.submit(new MyExecutor());
        executor.submit(new MyExecutor());
        executor.shutdown(); // Shutdown the executor
    }
}