package com.multithreading.thread.concurrency.executors.scheduledexecutorservice;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ScheduledExecutorServiceExample {
    
    public static void main(String[] args) {
        // Create a ScheduledExecutorService with a pool size of 1
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);

        // Schedule a task to run after a delay of 5 seconds
        executor.schedule(() -> System.out.println("Task executed after 5 seconds"),
                5, TimeUnit.SECONDS);

        // Schedule a task to run repeatedly every 3 seconds, starting after an initial delay of 1 second
        executor.scheduleAtFixedRate(() -> System.out.println("Task executed every 3 seconds"),
                1, 3, TimeUnit.SECONDS);

        // Shutdown the executor service after scheduling tasks
        executor.shutdown();
    }
}