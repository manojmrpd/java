package com.multithreading.thread.concurrency.executors.threadpoolexecutor;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadPoolExecutorExample {

    public static void main(String[] args) {
        // Create a ThreadPoolExecutor with fixed-size pool of 5 threads
        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(5);

        // Submit tasks to the thread pool
        for (int i = 0; i < 10; i++) {
            Runnable task = new Task("Task-" + i);
            executor.execute(task);
        }

        // Shutdown the thread pool after all tasks are completed
        executor.shutdown();

        // Wait for all tasks to complete or timeout after 30 seconds
        try {
            executor.awaitTermination(30, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    static class Task implements Runnable {
        private final String name;

        public Task(String name) {
            this.name = name;
        }

        @Override
        public void run() {
            System.out.println("Executing task: " + name + " on thread: " + Thread.currentThread().getName());
            // Perform task execution logic
        }
    }
}