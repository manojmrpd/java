package com.multithreading.thread.concurrency.forkjoin;

import java.util.concurrent.RecursiveTask;
import java.util.concurrent.ForkJoinPool;

public class ForkJoinExample {

    public static void main(String[] args) {
        ForkJoinPool forkJoinPool = new ForkJoinPool();

        // Create a task to compute the sum of an array
        SumTask task = new SumTask(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10});

        // Execute the task and get the result
        int result = forkJoinPool.invoke(task);

        System.out.println("Sum: " + result);
    }

    static class SumTask extends RecursiveTask<Integer> {
        private int[] array;
        private int start;
        private int end;

        public SumTask(int[] array, int start, int end) {
            this.array = array;
            this.start = start;
            this.end = end;
        }

        public SumTask(int[] array) {
            this(array, 0, array.length);
        }

        @Override
        protected Integer compute() {
            if (end - start <= 3) {
                int sum = 0;
                for (int i = start; i < end; i++) {
                    sum += array[i];
                }
                return sum;
            } else {
                int mid = start + (end - start) / 2;
                SumTask leftTask = new SumTask(array, start, mid);
                SumTask rightTask = new SumTask(array, mid, end);
                leftTask.fork();
                int rightResult = rightTask.compute();
                int leftResult = leftTask.join();
                return leftResult + rightResult;
            }
        }
    }
}