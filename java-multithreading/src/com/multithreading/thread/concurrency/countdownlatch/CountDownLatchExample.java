package com.multithreading.thread.concurrency.countdownlatch;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class CountDownLatchExample {

    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(4);
        int workerCount = 3;
        CountDownLatch latch = new CountDownLatch(workerCount);

        // Create and start worker threads
        for (int i = 0; i < workerCount; i++) {
            executorService.submit(() -> {
                System.out.println(Thread.currentThread().getName() + " is performing its task.");
                try {
                    // Simulate some work
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + " has finished its task.");
                latch.countDown(); // Signal that this worker thread has finished its task
            });
        }

        // Main thread waits for worker threads to finish
        latch.await();
        System.out.println("All worker threads have finished. Main thread can proceed.");
        // Shutdown the thread pool
        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.MINUTES);
    }
}