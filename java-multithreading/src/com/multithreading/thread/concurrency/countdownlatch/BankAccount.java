package com.multithreading.thread.concurrency.countdownlatch;

import java.util.concurrent.CountDownLatch;

public class BankAccount {
    private double balance;

    public BankAccount(double initialBalance) {
        this.balance = initialBalance;
    }

    public synchronized void deposit(double amount) {
        if (amount > 0) {
            balance += amount;
            System.out.println(Thread.currentThread().getName() + " deposited " + amount + ", new balance: " + balance);
        }
    }

    public synchronized void withdraw(double amount) {
        if (amount > 0 && amount <= balance) {
            balance -= amount;
            System.out.println(Thread.currentThread().getName() + " withdrew " + amount + ", new balance: " + balance);
        } else {
            System.out.println(Thread.currentThread().getName() + " attempted to withdraw " + amount + ", but insufficient funds. Current balance: " + balance);
        }
    }

    public double getBalance() {
        return balance;
    }

    public static void main(String[] args) {
        BankAccount account = new BankAccount(1000.0);
        int numberOfTransactions = 4; // Number of transaction threads
        CountDownLatch latch = new CountDownLatch(numberOfTransactions);

        Runnable depositTask = () -> {
            account.deposit(100);
            latch.countDown();
        };

        Runnable withdrawTask = () -> {
            account.withdraw(50);
            latch.countDown();
        };

        // Creating and starting transaction threads
        Thread t1 = new Thread(depositTask, "Thread-1");
        Thread t2 = new Thread(withdrawTask, "Thread-2");
        Thread t3 = new Thread(depositTask, "Thread-3");
        Thread t4 = new Thread(withdrawTask, "Thread-4");

        t1.start();
        t2.start();
        t3.start();
        t4.start();

        // Wait for all transactions to complete
        try {
            latch.await();
            System.out.println("All transactions are completed. Final balance: " + account.getBalance());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

