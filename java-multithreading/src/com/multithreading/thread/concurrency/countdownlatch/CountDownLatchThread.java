package com.multithreading.thread.concurrency.countdownlatch;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CountDownLatchThread {

    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(5);

        CountDownLatch latch = new CountDownLatch(3);
        for (int i = 1; i <= 3; i++) {
            executorService.submit(new DependentService(latch));
        }
        latch.await();
        System.out.println("All dependent service completed. " + Thread.currentThread().getName() + " thread started");
    }
}

class DependentService implements Runnable {
    private final CountDownLatch latch;

    DependentService(CountDownLatch latch) {
        this.latch = latch;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " is running.");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        latch.countDown();
        System.out.println(Thread.currentThread().getName() + " completed.");
    }
}
