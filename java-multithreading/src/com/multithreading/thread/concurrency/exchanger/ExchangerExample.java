package com.multithreading.thread.concurrency.exchanger;

import java.util.concurrent.Exchanger;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ExchangerExample {
    
    private static final Exchanger<String> exchanger = new Exchanger<>();

    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(4);
        executorService.submit(() -> {
            try {
                String message = "Data from Producer";
                System.out.println("Producer is waiting to exchange data: " + message);
                String receivedData = exchanger.exchange(message);
                System.out.println("Producer received data: " + receivedData);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        executorService.submit(() -> {
            try {
                String message = "Data from Consumer";
                System.out.println("Consumer is waiting to exchange data: " + message);
                String receivedData = exchanger.exchange(message);
                System.out.println("Consumer received data: " + receivedData);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        // Shutdown executor
        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.MINUTES);
    }
}
