package com.multithreading.thread.concurrency.condition;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConditionExample {
    private final Lock lock = new ReentrantLock();
    private final Condition condition = lock.newCondition();
    private boolean conditionMet = false;

    public void awaitCondition() throws InterruptedException {
        lock.lock();
        try {
            while (!conditionMet) {
                System.out.println(Thread.currentThread().getName() + " is waiting.");
                condition.await(); // Wait until condition is met
            }
            System.out.println(Thread.currentThread().getName() + " proceeds after condition is met.");
        } finally {
            lock.unlock();
        }
    }

    public void signalCondition() {
        lock.lock();
        try {
            conditionMet = true;
            condition.signalAll(); // Signal all waiting threads that condition is met
            System.out.println(Thread.currentThread().getName() + " signaled the condition.");
        } finally {
            lock.unlock();
        }
    }

    public static void main(String[] args) {
        ConditionExample example = new ConditionExample();
        ExecutorService executorService = Executors.newFixedThreadPool(3);

        // Task for threads that will wait for the condition
        Runnable awaitTask = () -> {
            try {
                example.awaitCondition();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        };

        // Task for a thread that will signal the condition
        Runnable signalTask = example::signalCondition;

        // Submit waiting tasks to the executor service
        executorService.submit(awaitTask);
        executorService.submit(awaitTask);

        // Submit the signaling task to the executor service
        executorService.submit(signalTask);

        executorService.shutdown();
    }
}
