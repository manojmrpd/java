package com.multithreading.thread.concurrency.semaphores;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class SemaphoreExample {
    
    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(50);
        Semaphore semaphore = new Semaphore(2);
        for (int i = 1; i <= 3; i++) {
            executorService.submit(() -> {
                try {
                    System.out.println(Thread.currentThread().getName() + " is waiting for a permit.");
                    semaphore.acquire(); // Acquire a permit
                    System.out.println(Thread.currentThread().getName() + " has acquired a permit.");
                    // Simulate some work with the resource
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } finally {
                    semaphore.release(); // Release the permit
                    System.out.println(Thread.currentThread().getName() + " released");
                }
            });
        }
        // Shutdown the thread pool
        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.MINUTES);

    }
}
