package com.multithreading.thread.concurrency.semaphores;

import java.util.concurrent.Semaphore;

public class BankAccount {
    private double balance;
    private final Semaphore semaphore;

    public BankAccount(double initialBalance, int permits) {
        this.balance = initialBalance;
        this.semaphore = new Semaphore(permits);
    }

    // Method to deposit money
    public void deposit(double amount) {
        try {
            semaphore.acquire();
            if (amount > 0) {
                System.out.println(Thread.currentThread().getName() + " is depositing " + amount);
                double newBalance = balance + amount;
                // Simulate some delay in processing the deposit
                Thread.sleep(100);
                balance = newBalance;
                System.out.println(Thread.currentThread().getName() + " deposited " + amount + ", new balance: " + balance);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            semaphore.release();
        }
    }

    // Method to withdraw money
    public void withdraw(double amount) {
        try {
            semaphore.acquire();
            if (amount > 0 && amount <= balance) {
                System.out.println(Thread.currentThread().getName() + " is withdrawing " + amount);
                double newBalance = balance - amount;
                // Simulate some delay in processing the withdrawal
                Thread.sleep(100);
                balance = newBalance;
                System.out.println(Thread.currentThread().getName() + " withdrew " + amount + ", new balance: " + balance);
            } else {
                System.out.println(Thread.currentThread().getName() + " attempted to withdraw " + amount + ", but insufficient funds. Current balance: " + balance);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            semaphore.release();
        }
    }

    // Method to check balance
    public double getBalance() {
        try {
            semaphore.acquire();
            return balance;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return 0;
        } finally {
            semaphore.release();
        }
    }

    public static void main(String[] args) {
        BankAccount account = new BankAccount(1000.0, 2); // Bank account with initial balance 1000.0 and 2 permits

        Runnable depositTask = () -> {
            for (int i = 0; i < 2; i++) {
                account.deposit(100);
                try {
                    Thread.sleep(50); // Simulate time taken between operations
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        Runnable withdrawTask = () -> {
            for (int i = 0; i < 2; i++) {
                account.withdraw(50);
                try {
                    Thread.sleep(50); // Simulate time taken between operations
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        Thread t1 = new Thread(depositTask, "Thread-1");
        Thread t2 = new Thread(withdrawTask, "Thread-2");

        t1.start();
        t2.start();
    }
}
