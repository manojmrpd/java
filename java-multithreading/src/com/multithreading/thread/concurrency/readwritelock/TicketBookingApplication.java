package com.multithreading.thread.concurrency.readwritelock;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class TicketBookingApplication {
    private int availableTickets;
    private final ReadWriteLock lock = new ReentrantReadWriteLock();

    public TicketBookingApplication(int totalTickets) {
        this.availableTickets = totalTickets;
    }

    // Method to check ticket availability
    public int getAvailableTickets() {
        lock.readLock().lock();
        try {
            return availableTickets;
        } finally {
            lock.readLock().unlock();
        }
    }

    // Method to book a ticket
    public boolean bookTicket() {
        lock.writeLock().lock();
        try {
            if (availableTickets > 0) {
                availableTickets--;
                System.out.println(Thread.currentThread().getName() + " booked a ticket. Tickets left: " + availableTickets);
                return true;
            } else {
                System.out.println(Thread.currentThread().getName() + " tried to book a ticket, but none are available.");
                return false;
            }
        } finally {
            lock.writeLock().unlock();
        }
    }

    // Method to cancel a ticket
    public boolean cancelTicket() {
        lock.writeLock().lock();
        try {
            availableTickets++;
            System.out.println(Thread.currentThread().getName() + " canceled a ticket. Tickets left: " + availableTickets);
            return true;
        } finally {
            lock.writeLock().unlock();
        }
    }

    public static void main(String[] args) {
        TicketBookingApplication bookingApp = new TicketBookingApplication(10);
        ExecutorService executorService = Executors.newFixedThreadPool(4);

        Runnable checkTask = () -> {
            for (int i = 0; i < 5; i++) {
                System.out.println(Thread.currentThread().getName() + " checked tickets. Available: " + bookingApp.getAvailableTickets());
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        Runnable bookTask = () -> {
            for (int i = 0; i < 5; i++) {
                bookingApp.bookTicket();
                try {
                    Thread.sleep(150);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        Runnable cancelTask = () -> {
            for (int i = 0; i < 5; i++) {
                bookingApp.cancelTicket();
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        // Submit tasks to the executor service
        executorService.submit(checkTask);
        executorService.submit(bookTask);
        executorService.submit(cancelTask);
        executorService.submit(checkTask);

        // Shut down the executor service
        executorService.shutdown();
    }
}
