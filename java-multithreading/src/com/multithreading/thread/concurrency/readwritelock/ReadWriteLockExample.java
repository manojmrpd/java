package com.multithreading.thread.concurrency.readwritelock;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ReadWriteLockExample {

    private static final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    private static int counter = 0;

    public static void main(String[] args) {

        ExecutorService executorService = Executors.newFixedThreadPool(4);

        for (int i = 1; i <= 3; i++) {
            executorService.submit(() -> {
                lock.writeLock().lock();
                try {
                    counter++;
                    System.out.println(Thread.currentThread().getName() + " is incremented: " + counter);
                } finally {
                    lock.writeLock().unlock();
                }
            });
        }

        for (int i = 1; i <= 3; i++) {
            executorService.submit(() -> {
                lock.readLock().lock();
                try {
                    System.out.println(Thread.currentThread().getName() + " returned counter: " + counter);
                    return counter;
                } finally {
                    lock.readLock().unlock();
                }
            });
        }
        executorService.shutdown();


    }

}
