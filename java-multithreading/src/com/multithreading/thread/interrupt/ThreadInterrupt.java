package com.multithreading.thread.interrupt;

public class ThreadInterrupt {
    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(new MyRunnable(), "Thread-1");
        t1.start(); //Start Thread-1
        // Main thread sleeps for 2 seconds
        Thread.sleep(2000);
        //Interrupt Thread-1
        t1.interrupt();
    }
}

class MyRunnable implements Runnable {

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println(Thread.currentThread().getName() + " is running.");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.out.println(Thread.currentThread().getName() + " was interrupted");
                return;
            }
        }
    }
}
