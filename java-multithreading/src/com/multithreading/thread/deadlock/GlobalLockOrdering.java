package com.multithreading.thread.deadlock;

public class GlobalLockOrdering {
    private static final Object resourceA = new Object();
    private static final Object resourceB = new Object();

    public static void methodOne() {
        synchronized (resourceA) {
            System.out.println(Thread.currentThread().getName() + " acquires lock on resource A");
            synchronized (resourceB) {
                System.out.println(Thread.currentThread().getName() + " acquires lock on resource B");
            }
        }
    }

    public static void methodTwo() {
        synchronized (resourceA) {
            System.out.println(Thread.currentThread().getName() + " acquires lock on resource A");
            synchronized (resourceB) {
                System.out.println(Thread.currentThread().getName() + " acquires lock on resource B");
            }
        }
    }

    public static void main(String[] args) {
        Thread thread1 = new Thread(() -> {
            methodOne();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });
        Thread thread2 = new Thread(() -> {
            methodTwo();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });
        thread1.start();
        thread2.start();
    }
}
