package com.multithreading.thread.deadlock;

public class AvoidNestedLocks {

    private final Object lock1 = new Object();

    public void safeMethod() {
        synchronized (lock1) {
            // Perform operations without acquiring additional locks
        }
    }
}
