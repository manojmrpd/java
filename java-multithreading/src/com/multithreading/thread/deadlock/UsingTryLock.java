package com.multithreading.thread.deadlock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class UsingTryLock {

    private static Lock lock1 = new ReentrantLock();
    private static Lock lock2 = new ReentrantLock();

    public static void firstTask() {
        if (acquireLock(lock1, lock2)) {
            try {
                System.out.println("Lock is acquired");
            } finally {
                lock1.unlock();
                lock2.unlock();
            }
        }
    }

    public static void secondTask() {
        if (acquireLock(lock2, lock1)) {
            try {
                System.out.println("Lock is acquired");
            } finally {
                lock2.unlock();
                lock1.unlock();
            }
        }
    }

    private static boolean acquireLock(Lock lock1, Lock lock2) {
        while (true) {
            // Try to acquire the first lock
            boolean tryFirstLock = lock1.tryLock();
            // Try to acquire the second lock
            boolean trySecondLock = lock2.tryLock();

            if (tryFirstLock && trySecondLock) {
                return true;
            }
            // If unable to get both locks, release any acquired locks
            if (tryFirstLock) {
                lock1.unlock();
            }
            // If unable to get both locks, release any acquired locks
            if (trySecondLock) {
                lock2.unlock();
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static void main(String[] args) {
        Thread thread1 = new Thread(UsingTryLock::firstTask);
        Thread thread2 = new Thread(UsingTryLock::secondTask);
        thread1.start();
        thread2.start();
    }
}
