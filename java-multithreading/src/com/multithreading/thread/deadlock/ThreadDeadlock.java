package com.multithreading.thread.deadlock;

public class ThreadDeadlock {

    private static final Object resourceA = new Object();
    private static final Object resourceB = new Object();

    public static void main(String[] args) {
        Thread thread1 = new Thread(() -> {
            synchronized (resourceA) {
                System.out.println(Thread.currentThread().getName() + " acquired lock on resource A");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                synchronized (resourceB) {
                    System.out.println(Thread.currentThread().getName() + " acquired lock on resource B");
                }
            }
        }, "Thread-1");

        Thread thread2 = new Thread(() -> {
            synchronized (resourceB) {
                System.out.println(Thread.currentThread().getName() + " acquired lock on resource B");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                synchronized (resourceA) {
                    System.out.println(Thread.currentThread().getName() + " acquired lock on resource A");
                }
            }
        }, "Thread-2");

        thread1.start();
        thread2.start();
    }

}
