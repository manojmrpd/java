package com.multithreading.thread.yield;

public class ThreadYield {

    public static void main(String[] args) {
        Thread t1 = new Thread(new MyRunnable(), "Thread-1");
        t1.setPriority(Thread.MAX_PRIORITY);
        Thread t2 = new Thread(new MyRunnable(), "Thread-2");
        t2.setPriority(Thread.MAX_PRIORITY);
        t1.start();
        t2.start();
    }
}

class MyRunnable implements Runnable {

    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            System.out.println(Thread.currentThread().getName() + " - Count: " + i);
            Thread.yield();
        }
    }
}
