package com.java.list.copyonwritearraylist.timecomplexity;

import java.util.concurrent.CopyOnWriteArrayList;

public class CopyOnWriteArrayListExample {

    public static void main(String[] args) {
        // Create a CopyOnWriteArrayList
        CopyOnWriteArrayList<Integer> cowList = new CopyOnWriteArrayList<>();

        // Add elements to the list
        cowList.add(1); // O(n) - List is copied for each add operation
        cowList.add(2); // O(n)
        cowList.add(3); // O(n)

        System.out.println("After adding elements: " + cowList);

        // Get an element
        Integer element = cowList.get(1); // O(1) - Direct access to the array
        System.out.println("Element at index 1: " + element);

        // Check size
        int size = cowList.size(); // O(1) - Direct access to the size
        System.out.println("Size of list: " + size);

        // Check if list contains an element
        boolean contains = cowList.contains(2); // O(n) - Linear search
        System.out.println("List contains 2: " + contains);

        // Iterate over the list
        System.out.println("Iterating over list:");
        for (Integer elem : cowList) { // O(n) - Iterates over the array
            System.out.println(elem);
        }

        // Set an element
        cowList.set(1, 4); // O(n) - List is copied and then element is set
        System.out.println("After setting element at index 1 to 4: " + cowList);

        // Remove an element
        cowList.remove(Integer.valueOf(4)); // O(n) - List is copied and then element is removed
        System.out.println("After removing element 4: " + cowList);

        // Remove element at index
        cowList.remove(0); // O(n) - List is copied and then element is removed
        System.out.println("After removing element at index 0: " + cowList);

        // Clear the list
        cowList.clear(); // O(n) - List is copied and then cleared
        System.out.println("After clearing the list: " + cowList);
    }
}

