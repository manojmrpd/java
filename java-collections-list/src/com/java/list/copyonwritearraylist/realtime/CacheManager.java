package com.java.list.copyonwritearraylist.realtime;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

public class CacheManager {

    private final List<CacheProvider> providers = new CopyOnWriteArrayList<>();

    public void addProvider(CacheProvider provider) {
        providers.add(provider);
    }

    public Object get(String key) {
        for (CacheProvider provider : providers) {
            Object value = provider.get(key);
            if (value != null) {
                return value;
            }
        }
        return null;
    }

    public static void main(String[] args) {
        CacheProvider cacheProvider = new CacheProvider();
        cacheProvider.put("firstName", "manoj");
        cacheProvider.put("lastName", "kumar");
        CacheManager cacheManager = new CacheManager();
        cacheManager.addProvider(cacheProvider);
        System.out.println("Retrieved value for key1: " + cacheManager.get("firstName"));
        System.out.println("Retrieved value for key2: " + cacheManager.get("lastName"));
    }
}

class CacheProvider {

    private final Map<String, Object> cache = new HashMap<>();

    public void put(String key, Object value) {
        cache.put(key, value);
    }

    public Object get(String key) {
        return cache.get(key);
    }

}
