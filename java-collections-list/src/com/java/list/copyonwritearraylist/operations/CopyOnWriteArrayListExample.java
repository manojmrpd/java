package com.java.list.copyonwritearraylist.operations;

import java.util.concurrent.CopyOnWriteArrayList;

public class CopyOnWriteArrayListExample {

    public static void main(String[] args) {
        CopyOnWriteArrayList<String> list = new CopyOnWriteArrayList<>();

        // Add elements
        list.add("Java");
        list.add("Python");
        list.add("C++");

        // Create an iterator
        for (String language : list) {
            System.out.println(language);
            // No ConcurrentModificationException will be thrown
            list.add("JavaScript");
        }

        System.out.println("Size after iteration: " + list.size()); // Prints 6
    }
}
