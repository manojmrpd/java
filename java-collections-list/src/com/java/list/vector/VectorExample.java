package com.java.list.vector;

import java.util.Vector;
import java.util.Enumeration;

public class VectorExample {

    public static void main(String[] args) {
        // Create a Vector
        Vector<Integer> vector = new Vector<>();

        // Add elements to the Vector
        vector.add(1); // O(1)
        vector.add(2); // O(1)
        vector.add(3); // O(1)
        System.out.println("Vector after adding elements: " + vector);

        // Insert an element at a specific position
        vector.add(1, 4); // O(n)
        System.out.println("Vector after inserting element at index 1: " + vector);

        // Get an element at a specific position
        Integer element = vector.get(2); // O(1)
        System.out.println("Element at index 2: " + element);

        // Remove an element at a specific position
        vector.remove(2); // O(n)
        System.out.println("Vector after removing element at index 2: " + vector);

        // Check if the Vector contains a specific element
        boolean contains = vector.contains(2); // O(n)
        System.out.println("Vector contains 2: " + contains);

        // Get the size of the Vector
        int size = vector.size(); // O(1)
        System.out.println("Size of the vector: " + size);

        // Iterate over the elements in the Vector
        System.out.println("Iterating over vector using Enumeration:");
        Enumeration<Integer> enumeration = vector.elements();
        while (enumeration.hasMoreElements()) {
            System.out.println(enumeration.nextElement());
        }

        // Clear the Vector
        vector.clear(); // O(n)
        System.out.println("Vector after clearing: " + vector);
    }
}