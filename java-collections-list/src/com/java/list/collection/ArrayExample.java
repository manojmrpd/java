package com.java.list.collection;

public class ArrayExample {

    public static void main(String[] args) {
        // Creating an array of integers
        int[] numbers = new int[5];

        // Initializing the array
        numbers[0] = 10;
        numbers[1] = 20;
        numbers[2] = 30;
        numbers[3] = 40;
        numbers[4] = 50;

        System.out.println("Size of array: " +numbers.length);
        // Accessing and printing array elements
        for (int i = 0; i < numbers.length; i++) {
            System.out.println("Element at index " + i + ": " + numbers[i]);
        }

    }
}
