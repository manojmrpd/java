package com.java.list.collection;

import java.util.ArrayList;
import java.util.List;

public class CollectionExample {

    public static void main(String[] args) {
        // Creating a collection (ArrayList) of integers
        List<Integer> numberList = new ArrayList<>();

        // Adding elements to the collection
        numberList.add(10);
        numberList.add(20);
        numberList.add(30);
        numberList.add(40);
        numberList.add(50);

        // Accessing and printing collection elements
        for (int number : numberList) {
            System.out.println("Element: " + number);
        }

        // Removing an element
        numberList.remove(2); // Removes the element at index 2 (30)

        System.out.println("After removal:");
        for (int number : numberList) {
            System.out.println("Element: " + number);
        }
    }
}
