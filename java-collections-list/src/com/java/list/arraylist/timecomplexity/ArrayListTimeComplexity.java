package com.java.list.arraylist.timecomplexity;

import java.util.ArrayList;
import java.util.List;

public class ArrayListTimeComplexity {

    public static void main(String[] args) {
        // Create instance of ArrayList with initial capacity is 10.
        List<Integer> arrayList = new ArrayList<>();

        // Add elements (O(1) amortized)
        arrayList.add(10);
        arrayList.add(20);
        arrayList.add(30);

        // Accessing elements in the arraylist took (O(1))
        int element = arrayList.get(1);

        // Add elements at index took (O(n))
        arrayList.add(2, 25);

        // Update the elements at index took (O(1))
        arrayList.set(1, 30);

        // Contains search took (O(n))
        boolean contains = arrayList.contains(20);

        // indexOf search took (O(n))
        int indexOfElement = arrayList.indexOf(30);

        // Remove elements from the end of the ArrayList took (O(1))
        arrayList.remove(arrayList.size() - 1); // O(1)

        // Remove element took (O(n))
        arrayList.remove((Integer) 20);

        // Remove element based on index took (O(n))
        arrayList.remove(1);

        // Size took (O(1))
        arrayList.size();
    }
}