package com.java.list.arraylist.operations;

import java.util.ArrayList;
import java.util.List;

/**
 * Best suitable for random access and faster in accessing elements based on index.
 */
public class ArrayListExample {

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();

        // Adding elements
        list.add("Apple");
        list.add("Banana");
        list.add("Cherry");
        list.add("Apple"); // Duplicates allowed

        // Accessing elements
        System.out.println("Element at index 1: " + list.get(1));

        // Iterating over elements
        System.out.println("List elements:");
        for (String fruit : list) {
            System.out.println(fruit);
        }

        // Removing an element
        list.remove("Banana");

        // Checking if the list contains an element
        boolean containsCherry = list.contains("Cherry");
        System.out.println("List contains 'Cherry': " + containsCherry);

        // Getting the size of the list
        int size = list.size();
        System.out.println("List size: " + size);
    }
}

