package com.java.list.arraylist.realtime;

import java.util.ArrayList;

public class ShoppingCart {

    private ArrayList<String> items;

    public ShoppingCart() {
        items = new ArrayList<>();
    }

    public void addItem(String item) {
        items.add(item);
        System.out.println(item + " added to the cart.");
    }

    public void removeItem(String item) {
        if (items.remove(item)) {
            System.out.println(item + " removed from the cart.");
        } else {
            System.out.println(item + " not found in the cart.");
        }
    }

    public void viewCart() {
        System.out.println("Shopping Cart:");
        for (String item : items) {
            System.out.println(item);
        }
    }

    public static void main(String[] args) {
        ShoppingCart cart = new ShoppingCart();
        cart.addItem("Laptop");
        cart.addItem("Smartphone");
        cart.viewCart();
        cart.removeItem("Laptop");
        cart.viewCart();
    }
}
