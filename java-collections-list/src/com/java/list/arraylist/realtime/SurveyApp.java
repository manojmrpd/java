package com.java.list.arraylist.realtime;

import java.util.ArrayList;
import java.util.Scanner;

public class SurveyApp {

    public static void main(String[] args) {
        ArrayList<String> responses = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter your responses (type 'done' to finish):");
        while (true) {
            String response = scanner.nextLine();
            if ("done".equalsIgnoreCase(response)) {
                break;
            }
            responses.add(response);
        }

        System.out.println("Survey responses:");
        for (String response : responses) {
            System.out.println(response);
        }
    }
}
