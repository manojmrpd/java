package com.java.list.arraylist.realtime;

import java.util.ArrayList;

public class ChatRoom {

    private ArrayList<String> activeUsers;

    public ChatRoom() {
        activeUsers = new ArrayList<>();
    }

    public void userJoined(String username) {
        activeUsers.add(username);
        System.out.println(username + " joined the chat.");
    }

    public void userLeft(String username) {
        if (activeUsers.remove(username)) {
            System.out.println(username + " left the chat.");
        } else {
            System.out.println(username + " not found in the chat.");
        }
    }

    public void displayActiveUsers() {
        System.out.println("Active Users:");
        for (String user : activeUsers) {
            System.out.println(user);
        }
    }

    public static void main(String[] args) {
        ChatRoom chatRoom = new ChatRoom();
        chatRoom.userJoined("Alice");
        chatRoom.userJoined("Bob");
        chatRoom.displayActiveUsers();
        chatRoom.userLeft("Alice");
        chatRoom.displayActiveUsers();
    }
}
