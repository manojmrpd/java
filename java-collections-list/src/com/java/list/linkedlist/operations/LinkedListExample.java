package com.java.list.linkedlist.operations;

import java.util.LinkedList;
import java.util.List;

/**
 * Best suitable where frequent insertions and deletions are required, especially
 * at beginning or end of the list.
 */
import java.util.LinkedList;

public class LinkedListExample {

    public static void main(String[] args) {
        
        LinkedList<String> list = new LinkedList<>();

        // Adding elements
        list.add("Apple");
        list.add("Banana");
        list.add("Cherry");

        // Adding elements at specific positions
        list.addFirst("Mango");
        list.addLast("Orange");

        // Accessing elements
        System.out.println("First element: " + list.getFirst());
        System.out.println("Last element: " + list.getLast());

        // Removing elements
        list.removeFirst();
        list.removeLast();

        // Iterating over elements
        for (String fruit : list) {
            System.out.println(fruit);
        }

        // Stack operations
        list.push("Grapes"); // Adds at the head
        System.out.println("Popped element: " + list.pop()); // Removes from the head

        // Queue operations
        list.offer("Pineapple"); // Adds at the tail
        System.out.println("Polled element: " + list.poll()); // Removes from the head
    }
}
