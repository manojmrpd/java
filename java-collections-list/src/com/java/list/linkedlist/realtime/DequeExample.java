package com.java.list.linkedlist.realtime;

import java.util.LinkedList;

public class DequeExample {

    private LinkedList<Integer> deque;

    public DequeExample() {
        deque = new LinkedList<>();
    }

    public void addFront(int element) {
        deque.addFirst(element);
        System.out.println("Added to front: " + element);
    }

    public void addRear(int element) {
        deque.addLast(element);
        System.out.println("Added to rear: " + element);
    }

    public Integer removeFront() {
        return deque.pollFirst();
    }

    public Integer removeRear() {
        return deque.pollLast();
    }

    public static void main(String[] args) {
        DequeExample deque = new DequeExample();
        deque.addFront(10);
        deque.addRear(20);
        deque.addFront(5);

        System.out.println("Removed from front: " + deque.removeFront());
        System.out.println("Removed from rear: " + deque.removeRear());
    }
}
