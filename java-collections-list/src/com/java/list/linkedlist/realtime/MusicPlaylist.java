package com.java.list.linkedlist.realtime;

import java.util.LinkedList;

public class MusicPlaylist {

    private LinkedList<String> playlist;

    public MusicPlaylist() {
        playlist = new LinkedList<>();
    }

    public void addSong(String song) {
        playlist.add(song);
        System.out.println("Added song: " + song);
    }

    public String playNext() {
        if (!playlist.isEmpty()) {
            return playlist.removeFirst();
        }
        return "No more songs in the playlist.";
    }

    public static void main(String[] args) {
        MusicPlaylist playlist = new MusicPlaylist();
        playlist.addSong("Song 1");
        playlist.addSong("Song 2");
        playlist.addSong("Song 3");

        System.out.println("Playing: " + playlist.playNext());
        System.out.println("Playing: " + playlist.playNext());
        System.out.println("Playing: " + playlist.playNext());
    }
}
