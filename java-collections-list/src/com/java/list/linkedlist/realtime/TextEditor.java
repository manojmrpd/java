package com.java.list.linkedlist.realtime;

import java.util.LinkedList;

public class TextEditor {

    private LinkedList<String> textStates;
    private int currentIndex;

    public TextEditor() {
        textStates = new LinkedList<>();
        textStates.add("");
        currentIndex = 0;
    }

    public void type(String text) {
        while (textStates.size() > currentIndex + 1) {
            textStates.removeLast();
        }
        textStates.add(text);
        currentIndex++;
    }

    public String undo() {
        if (currentIndex > 0) {
            currentIndex--;
            return textStates.get(currentIndex);
        }
        return textStates.getFirst();
    }

    public String redo() {
        if (currentIndex < textStates.size() - 1) {
            currentIndex++;
            return textStates.get(currentIndex);
        }
        return textStates.getLast();
    }

    public static void main(String[] args) {
        
        TextEditor editor = new TextEditor();
        editor.type("Hello");
        editor.type("Hello, World");
        System.out.println("Current: " + editor.undo());
        System.out.println("Current: " + editor.redo());
        editor.type("Hello, World!");
        System.out.println("Current: " + editor.undo());
    }
}