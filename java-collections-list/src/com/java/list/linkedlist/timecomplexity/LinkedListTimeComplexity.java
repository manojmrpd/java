package com.java.list.linkedlist.timecomplexity;

import java.util.LinkedList;

public class LinkedListTimeComplexity {

    public static void main(String[] args) {

        // Create an instance of LinkedList
        LinkedList<Integer> linkedList = new LinkedList<>();

        // Add elements to the beginning of the LinkedList
        linkedList.addFirst(10); // O(1)
        linkedList.addFirst(20); // O(1)
        linkedList.addFirst(30); // O(1)

        // Add elements to the end of the LinkedList
        linkedList.addLast(40); // O(1)
        linkedList.addLast(50); // O(1)

        // Add elements at a specific position in the LinkedList
        linkedList.add(2, 25); // O(n)

        // Accessing elements in the LinkedList
        int elementAtIndex2 = linkedList.get(2); // O(n)

        // Remove elements from the beginning of the LinkedList
        linkedList.removeFirst(); // O(1)

        // Remove elements from the end of the LinkedList
        linkedList.removeLast(); // O(1)

        // Remove elements by index in the LinkedList
        linkedList.remove(2); // O(n)

        // Searching for elements in the LinkedList
        boolean containsElement = linkedList.contains(20); // O(n)
        int indexOfElement = linkedList.indexOf(30); // O(n)

    }
}
