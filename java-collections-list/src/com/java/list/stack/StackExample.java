package com.java.list.stack;

import java.util.Stack;

public class StackExample {

    public static void main(String[] args) {
        // Create a stack
        Stack<Integer> stack = new Stack<>();

        // Push elements onto the stack
        stack.push(1); // O(1)
        stack.push(2); // O(1)
        stack.push(3); // O(1)
        System.out.println("Stack after pushes: " + stack);

        // Peek at the top element without removing it
        Integer topElement = stack.peek(); // O(1)
        System.out.println("Top element (peek): " + topElement);

        // Pop elements from the stack
        Integer poppedElement1 = stack.pop(); // O(1)
        System.out.println("Popped element: " + poppedElement1);
        Integer poppedElement2 = stack.pop(); // O(1)
        System.out.println("Popped element: " + poppedElement2);

        // Check if the stack is empty
        boolean isEmpty = stack.isEmpty(); // O(1)
        System.out.println("Is stack empty: " + isEmpty);

        // Search for an element in the stack
        stack.push(4); // O(1)
        stack.push(5); // O(1)
        int position = stack.search(4); // O(n)
        System.out.println("Position of element 4 (1-based index): " + position);

        // Final state of the stack
        System.out.println("Final stack: " + stack);
    }
}
