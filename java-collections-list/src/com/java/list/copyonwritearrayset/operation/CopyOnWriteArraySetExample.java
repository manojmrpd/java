package com.java.list.copyonwritearrayset.operation;

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArraySet;

public class CopyOnWriteArraySetExample {

    public static void main(String[] args) {
        CopyOnWriteArraySet<String> set = new CopyOnWriteArraySet<>();

        // Add elements
        set.add("Java");
        set.add("Python");
        set.add("C++");

        // Create an iterator
        Iterator<String> iterator = set.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
            // No ConcurrentModificationException will be thrown
            set.add("JavaScript");
        }

        System.out.println("Size after iteration: " + set.size()); // Prints 4
    }
}

