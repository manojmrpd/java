package com.java.list.copyonwritearrayset.realtime;

import java.util.concurrent.CopyOnWriteArraySet;

public class CopyOnWriteArraySetExample {

    public static void main(String[] args) {
        // Create a CopyOnWriteArraySet
        CopyOnWriteArraySet<Integer> cowSet = new CopyOnWriteArraySet<>();

        // Create and start threads to perform operations concurrently
        Thread addThread = new Thread(new AddTask(cowSet));
        Thread removeThread = new Thread(new RemoveTask(cowSet));
        Thread iterateThread = new Thread(new IterateTask(cowSet));

        addThread.start();
        removeThread.start();
        iterateThread.start();

        // Wait for threads to complete
        try {
            addThread.join();
            removeThread.join();
            iterateThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Final state of the set
        System.out.println("Final state of the set: " + cowSet);
    }
}

// Task to add elements to the set
class AddTask implements Runnable {
    private final CopyOnWriteArraySet<Integer> cowSet;

    public AddTask(CopyOnWriteArraySet<Integer> cowSet) {
        this.cowSet = cowSet;
    }

    @Override
    public void run() {
        for (int i = 1; i <= 10; i++) {
            cowSet.add(i);
            System.out.println("Added: " + i);
            try {
                Thread.sleep(100); // Simulate some delay
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

// Task to remove elements from the set
class RemoveTask implements Runnable {
    private final CopyOnWriteArraySet<Integer> cowSet;

    public RemoveTask(CopyOnWriteArraySet<Integer> cowSet) {
        this.cowSet = cowSet;
    }

    @Override
    public void run() {
        for (int i = 1; i <= 10; i++) {
            if (cowSet.remove(i)) {
                System.out.println("Removed: " + i);
            }
            try {
                Thread.sleep(150); // Simulate some delay
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

// Task to iterate over the set
class IterateTask implements Runnable {
    private final CopyOnWriteArraySet<Integer> cowSet;

    public IterateTask(CopyOnWriteArraySet<Integer> cowSet) {
        this.cowSet = cowSet;
    }

    @Override
    public void run() {
        try {
            for (int i = 0; i < 3; i++) {
                System.out.println("Current state of the set: " + cowSet);
                Thread.sleep(200); // Simulate some delay
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

