package com.java.list.copyonwritearrayset.timecomplexity;

import java.util.concurrent.CopyOnWriteArraySet;
import java.util.Iterator;

public class CopyOnWriteArraySetExample {
    
    public static void main(String[] args) {
        // Create a CopyOnWriteArraySet
        CopyOnWriteArraySet<Integer> cowSet = new CopyOnWriteArraySet<>();

        // Add elements to the set
        cowSet.add(1); // O(n)
        cowSet.add(2); // O(n)
        cowSet.add(3); // O(n)

        System.out.println("After adding elements: " + cowSet);

        // Check if the set contains an element
        boolean contains = cowSet.contains(2); // O(n)
        System.out.println("Set contains 2: " + contains);

        // Get the size of the set
        int size = cowSet.size(); // O(1)
        System.out.println("Size of set: " + size);

        // Iterate over the set
        System.out.println("Iterating over set:");
        for (Integer elem : cowSet) { // O(n)
            System.out.println(elem);
        }

        // Remove an element
        cowSet.remove(2); // O(n)
        System.out.println("After removing element 2: " + cowSet);

        // Clear the set
        cowSet.clear(); // O(n)
        System.out.println("After clearing the set: " + cowSet);
    }
}

