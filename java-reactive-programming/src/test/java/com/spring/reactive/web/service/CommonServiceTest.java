package com.spring.reactive.web.service;

import com.spring.reactive.web.dto.retail.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

@ExtendWith(SpringExtension.class)
public class CommonServiceTest {

    @InjectMocks
    public RetailService commonService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetProductList() {
        commonService.category = "category";
        List<Product> productList = commonService.getProductList();
    }
}
