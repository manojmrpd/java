package com.spring.reactive.web.flux.publishOn;

import com.spring.reactive.web.flux.parallel.FluxParallel;
import com.spring.reactive.web.flux.retryWhen.FluxRetryWhen;
import com.spring.reactive.web.flux.retryWhen.FluxRetryWhen2;
import com.spring.reactive.web.flux.subscribeOn.FluxSubscribeOnBoundedElastic;
import com.spring.reactive.web.flux.subscribeOn.FluxSubscribeOnParallel;
import org.junit.jupiter.api.Test;
import reactor.test.StepVerifier;


class FluxPublishOnTest {

    @Test
    public void testFluxRetryWhen_backOff() {
        FluxRetryWhen fluxRetryWhen = new FluxRetryWhen();
        StepVerifier.create(fluxRetryWhen.getStockPrice())
                .expectNextCount(16)
                .expectErrorMessage("Retries exhausted: 3/3")
                .verify();
    }

    @Test
    public void testFluxRetryWhen_fixedDelay() {
        FluxRetryWhen2 fluxRetryWhen = new FluxRetryWhen2();
        fluxRetryWhen.getStockPrice();
        StepVerifier.create(fluxRetryWhen.getStockPrice())
                .expectNextCount(16)
                .expectErrorMessage("Invalid stock price")
                .verify();
    }

    @Test
    public void testFluxPublishOnParallel() {
        FluxPublishOnParallel parallel = new FluxPublishOnParallel();
        StepVerifier.create(parallel.publishOnParallel())
                .expectNextCount(6)
                .verifyComplete();
    }

    @Test
    public void testFluxPublishOnBoundedElastic() {
        FluxPublishOnBoundedElastic boundedElastic = new FluxPublishOnBoundedElastic();
        StepVerifier.create(boundedElastic.publishOnBoundedElastic())
                .expectNextCount(6)
                .verifyComplete();
    }

    @Test
    public void testFluxSubscribeOnBoundedElastic() {
        FluxSubscribeOnBoundedElastic boundedElastic = new FluxSubscribeOnBoundedElastic();
        StepVerifier.create(boundedElastic.subscribeOnBoundedElastic())
                .expectNextCount(6)
                .verifyComplete();
    }

    @Test
    public void testFluxSubscribeOnParallel() {
        FluxSubscribeOnParallel parallel = new FluxSubscribeOnParallel();
        StepVerifier.create(parallel.subcribeOnParallel())
                .expectNextCount(6)
                .verifyComplete();
    }

    @Test
    public void testFluxPublishOn() {
        FluxParallel fluxParallel = new FluxParallel();
        StepVerifier.create(fluxParallel.publishOn())
                .expectNextCount(3)
                .verifyComplete();
    }

    @Test
    public void testFluxSubscribeOn() {
        FluxParallel fluxParallel = new FluxParallel();
        StepVerifier.create(fluxParallel.subscribeOn())
                .expectNextCount(3)
                .verifyComplete();
    }

    @Test
    public void testFluxParallel() {
        FluxParallel fluxParallel = new FluxParallel();
        StepVerifier.create(fluxParallel.parallel())
                .expectNextCount(3)
                .verifyComplete();
    }

    @Test
    public void testFluxParallel_sequential() {
        FluxParallel fluxParallel = new FluxParallel();
        StepVerifier.create(fluxParallel.parallel_sequential())
                .expectNextCount(3)
                .verifyComplete();
    }

    @Test
    public void testFluxParallel_flatMap() {
        FluxParallel fluxParallel = new FluxParallel();
        StepVerifier.create(fluxParallel.parallel_flatMap())
                .expectNextCount(3)
                .verifyComplete();
    }

    @Test
    public void testFluxParallel_flatMapSequential() {
        FluxParallel fluxParallel = new FluxParallel();
        StepVerifier.create(fluxParallel.parallel_flatMapSequential())
                .expectNextCount(3)
                .verifyComplete();
    }
}