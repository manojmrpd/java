package com.spring.reactive.web.concepts.combiningreactivestreams.concatMap;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class FluxConcatMap1 {
    public static void main(String[] args) {
        Flux<Integer> numbers = Flux.just(1, 2, 3);
        numbers.concatMap(number -> Mono.just(number * 2))
                .map(number -> "result: "+ number)
                .subscribe(System.out::println);
    }
}
