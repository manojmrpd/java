package com.spring.reactive.web.concepts.dooncallbackoperators.doOnSuccess;

import com.spring.reactive.web.dto.retail.Product;
import com.spring.reactive.web.service.RetailService;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;

public class MonoDoOnSuccess1 {

    public static final String ELECTRONICS = "Electronics";

    public static void main(String[] args) {
        // Use doOnSuccess() to perform any actions that need to happen after the Mono finishes emitting its data successfully.
        // This could involve closing connections, releasing resources, or updating external state based on the successful completion.
        Mono.fromCallable(() -> RetailService.getProducts())
                .doOnSuccess(products -> getElectronicProducts(products).subscribe(System.out::println))
                .subscribe(System.out::println);
    }

    private static Mono<List<Product>> getElectronicProducts(List<Product> products) {
        List<Product> productList = products.stream().filter(product -> product.getCategory().equalsIgnoreCase(ELECTRONICS)).collect(Collectors.toList());
        System.out.println(productList);
        return Mono.just(productList);
    }
}
