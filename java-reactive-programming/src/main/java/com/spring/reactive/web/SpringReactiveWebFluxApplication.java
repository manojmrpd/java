package com.spring.reactive.web;

import lombok.Generated;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@Generated
@SpringBootApplication
public class SpringReactiveWebFluxApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringReactiveWebFluxApplication.class, args);
    }
}
