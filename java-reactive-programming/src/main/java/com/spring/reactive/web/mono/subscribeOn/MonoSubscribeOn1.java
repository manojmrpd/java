package com.spring.reactive.web.mono.subscribeOn;

import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

public class MonoSubscribeOn1 {
    public static void main(String[] args) {
        Mono<String> dataMono = Mono.just("Manoj");
        // Subscribe on a single-threaded scheduler for thread safety
        dataMono.subscribeOn(Schedulers.single())
                .map(String::toUpperCase)
                .subscribe(System.out::println);
    }
}
