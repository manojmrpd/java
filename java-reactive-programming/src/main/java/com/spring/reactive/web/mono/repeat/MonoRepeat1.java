package com.spring.reactive.web.mono.repeat;

import reactor.core.publisher.Mono;

public class MonoRepeat1 {
    public static void main(String[] args) {
        // Mono.repeat() is useful for creating infinite streams or retrying operations with basic backoff logic.
        // You might need to combine it with operators like take to limit the number of emissions.
        Mono.fromCallable(() -> "manoj")
                .repeat().log()
                .take(4)
                .subscribe(System.out::println);
    }
}
