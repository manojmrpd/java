package com.spring.reactive.web.flux.repeat;

import com.spring.reactive.web.exceptions.StockPriceException;
import reactor.core.Exceptions;
import reactor.core.publisher.Flux;
import reactor.util.retry.Retry;

import java.time.Duration;

public class FluxRepeat1 {
    public static void main(String[] args) {

        getStockPrice().subscribe(integer -> System.out.println(integer),
                ex -> System.out.println("Exception occurred: " + ex.getMessage()),
                () -> System.out.println("Completed"));
    }

    // This code defines a method getStockPrice that generates a sequence of integers representing stock prices. If the price equals 5, it throws an IllegalStateException.
    // It then merges the generated prices with another sequence, maps any errors to a StockPriceException, retries using a backoff strategy defined in getRetryBackoffSpec, and logs the resulting sequence.
    private static Flux<Integer> getStockPrice() {
        return Flux.range(1, 10)
                .map(price -> {
                    if (price.equals(5)) {
                        throw new IllegalStateException("Invalid stock price");
                    }
                    return price;
                })
                .mergeWith(Flux.range(11, 10))
                .onErrorMap(ex -> new StockPriceException(ex.getMessage()))
                .retryWhen(getRetryBackoffSpec())
                .repeat(3)
                .log();
    }

    //This Java method getRetryBackoffSpec() returns a Retry object that specifies a retry strategy with a fixed delay of 500 milliseconds and a maximum number of retries of 3.
    //The filter method is used to specify that the retry should only occur if the exception is an instance of StockPriceException.
    //The onRetryExhaustedThrow method is used to specify what should happen when the maximum number of retries is reached. In this case, it propagates the original exception that caused the retry to be exhausted.
    private static Retry getRetryBackoffSpec() {
        return Retry.fixedDelay(3, Duration.ofMillis(500))
                .filter(ex -> ex instanceof StockPriceException)
                .onRetryExhaustedThrow((retryBackoffSpec, retrySignal) -> Exceptions.propagate(retrySignal.failure()));
    }
}
