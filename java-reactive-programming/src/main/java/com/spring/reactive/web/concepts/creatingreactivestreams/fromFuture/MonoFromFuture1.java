package com.spring.reactive.web.concepts.creatingreactivestreams.fromFuture;

import reactor.core.publisher.Mono;

import java.util.concurrent.CompletableFuture;

public class MonoFromFuture1 {
    public static void main(String[] args) {
        CompletableFuture<String> future = CompletableFuture.supplyAsync(() -> "Manoj kumar");
        Mono<String> userNameMono = Mono.fromFuture(future);
        userNameMono.subscribe(s -> System.out.println(s),
                throwable -> System.out.println(throwable.getMessage()),
                () -> System.out.println("Completed"));
    }
}
