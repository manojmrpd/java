package com.spring.reactive.web.concepts.combiningreactivestreams.merge;

import reactor.core.publisher.Mono;

public class FluxMerge2 {
    public static void main(String[] args) {

        //Combine Mono reactive stream with Mono or Flux using ConcatWith
        Mono.just("alex").mergeWith(Mono.just("slice"))
                .map(String::toUpperCase)
                .filter(s -> s.length() <= 9)
                .subscribe(System.out::println);
    }
}
