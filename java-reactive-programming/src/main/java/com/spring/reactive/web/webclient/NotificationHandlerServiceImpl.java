package com.spring.reactive.web.webclient;

package com.manulife.ap.notification.handler.service.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.manulife.ap.notification.handler.config.NotificationHandlerConfig;
import com.manulife.ap.notification.handler.entity.CustomerDocument;
import com.manulife.ap.notification.handler.entity.PolicyDocument;
import com.manulife.ap.notification.handler.helper.MessageSender;
import com.manulife.ap.notification.handler.mapper.NotificationHandlerMapper;
import com.manulife.ap.notification.handler.model.CWSRequest;
import com.manulife.ap.notification.handler.model.EmailRequest;
import com.manulife.ap.notification.handler.repository.customer.CustomerRepository;
import com.manulife.ap.notification.handler.repository.policy.PolicyRepository;
import com.manulife.ap.notification.handler.service.NotificationHandlerService;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
@Slf4j
@Service
public class NotificationHandlerServiceImpl implements NotificationHandlerService {
    @Autowired
    private PolicyRepository policyRepository;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private NotificationHandlerConfig notificationHandlerConfig;
    @Autowired
    private NotificationHandlerMapper notificationHandlerMapper;
    @Autowired
    private MessageSender messageSender;
    @Override
    public Mono<Boolean> startNotificationHandler() {
        getPolicyBySmallCodeAndPolicyStatus(notificationHandlerConfig.getSmallCode(),
                notificationHandlerConfig.getPolicyStatus()).flatMap((final PolicyDocument policy) -> {
                    this.getCustomerDetialsByPolicyNumber(String.valueOf(policy.getPolicyInfo().getPolicyNumber()))
                            .flatMap((final CustomerDocument customer) -> {
                                EmailRequest emailRequest = notificationHandlerMapper.mapNotificationRequest(customer);
                                if (emailRequest != null) {
                                    messageSender.postNotificationRequest(emailRequest);
                                }
                                CWSRequest cwsRequest = notificationHandlerMapper.mapCWSRequest(policy, customer);
                                if (cwsRequest != null) {
                                    messageSender.postCWSRequest(cwsRequest);
                                }
                                return Mono.just(true);
                            }).doOnError(e -> log.error("startNotificationHandler(): Customer data error: {}",
                                    e.getMessage()))
                            .subscribe();
                    return Mono.just(true);
                }).doOnError(e -> log.error("startNotificationHandler(): Policy data error: {}", e.getMessage()))
                .doOnComplete(() -> log.info("Notification handler completed")).subscribe();
        return Mono.just(true);
    }
    public Flux<PolicyDocument> getPolicyBySmallCodeAndPolicyStatus(int smallCode, String policyStatus) {
        return policyRepository.findBySmallCodeAndPolicyStatus(smallCode, policyStatus);
    }
    public Flux<CustomerDocument> getCustomerDetialsByPolicyNumber(String policyNumber) {
        return customerRepository.findByPolicyNumber(policyNumber);
    }
}
