package com.spring.reactive.web.concepts.creatingreactivestreams.fromCallable;

import reactor.core.publisher.Mono;

import java.nio.file.Files;
import java.nio.file.Paths;

public class MonoFromCallable1 {
    public static void main(String[] args) {
        Mono<String> readFileContentMono = Mono.fromCallable(() -> {
            return new String(Files.readAllBytes(Paths.get("myFile.txt")));
        });
        readFileContentMono.subscribe(text -> System.out.println(text),
                throwable -> System.out.println(throwable.getMessage()),
                () -> System.out.println("Completed"));

    }
}
