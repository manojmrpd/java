package com.spring.reactive.web.concepts.transformingreactivestreams.flatMapSequential;

import com.spring.reactive.web.flux.parallel.FluxParallel;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import static java.lang.Thread.sleep;

public class FlatMapSequential {

    public static void main(String[] args) {
        getNamesList().subscribe(System.out::println);
    }

    public static Flux<String> getNamesList() {
        return Flux.just("Alex", "Ben", "Charlie")
                .flatMapSequential(name -> Mono.just(name))
                .log();
    }

    private static String toUpperCase(String s) {
        return s.toUpperCase();
    }
}
