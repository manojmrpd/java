package com.spring.reactive.web.concepts.transformingreactivestreams.switchIfEmpty;

import com.spring.reactive.web.service.RetailService;
import reactor.core.publisher.Flux;

public class FluxSwitchIfEmpty1 {
    public static void main(String[] args) {
        Flux.fromIterable(RetailService.getOrders())
                .flatMapIterable(order -> order.getProducts())
                .filter(product -> product.getPrice() > 1000)
                .switchIfEmpty(Flux.error(new RuntimeException("No products found")))
                .subscribe(System.out::println);
    }
}
