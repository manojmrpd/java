package com.spring.reactive.web.mono.transform;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.function.Function;

public class MonoTransform1 {
    public static void main(String[] args) {
        Function<Mono<String>, Mono<String>> filterMap
                = name -> name.map(String::toUpperCase);

        Mono.just("Alex").transform(filterMap)
                .flatMap(MonoTransform1::splitText)
                .subscribe(System.out::println);
    }

    private static Mono<?> splitText(String s) {
        List<String> charlist = List.of(s.split(""));
        return Mono.just(charlist);
    }
}
