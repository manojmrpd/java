package com.spring.reactive.web.concepts.executionmodelreactivestreams.publishOn;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

import static java.lang.Thread.sleep;

@Slf4j
public class FluxPublishOnParallel {

    public Flux<String> publishOnParallel() {
        Flux<String> flux1 = Flux.just("Alex", "Bin", "Charlie")
                .publishOn(Schedulers.parallel())
                .map(FluxPublishOnParallel::toUpperCase)
                .log();

        Flux<String> flux2 = Flux.just("Ded", "Egg", "Francis")
                .publishOn(Schedulers.parallel())
                .map(FluxPublishOnParallel::toUpperCase)
                .log();
        return flux1.mergeWith(flux2);
    }


    private static String toUpperCase(String s) {
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return s.toUpperCase();
    }
}
