package com.spring.reactive.web.mono.single;

import reactor.core.publisher.Mono;

public class MonoSingle1 {
    public static void main(String[] args) {
        //Mono.single() for ensuring a Mono emits exactly one value and signaling specific errors for other scenarios (no elements or multiple elements).
        Mono<String> userName = Mono.fromCallable(() -> "Manoj").single().log();
        userName.subscribe(System.out::println);
    }
}
