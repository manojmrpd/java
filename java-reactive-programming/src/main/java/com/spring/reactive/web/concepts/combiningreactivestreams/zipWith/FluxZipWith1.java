package com.spring.reactive.web.concepts.combiningreactivestreams.zipWith;

import reactor.core.publisher.Flux;

public class FluxZipWith1 {

    public static void main(String[] args) {
        Flux<Integer> flux1 = Flux.just(1, 2, 3);
        Flux<Integer> flux2 = Flux.just(4, 5, 6, 7, 8);
        flux1.zipWith(flux2).subscribe(System.out::println);
    }
}
