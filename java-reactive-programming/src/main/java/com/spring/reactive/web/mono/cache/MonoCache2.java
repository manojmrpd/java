package com.spring.reactive.web.mono.cache;

import com.spring.reactive.web.service.RetailService;
import reactor.core.publisher.Mono;

import java.time.Duration;

public class MonoCache2 {
    public static void main(String[] args) {
        //Use Mono.cache(Duration ttl) for caching with controlled expiration to balance performance and data freshness.
        Mono.fromCallable(() -> RetailService.getProducts())
                .cache(Duration.ofMillis(100))
                .subscribe(products -> System.out.println(products),
                        throwable -> System.out.println(throwable.getMessage()),
                        () -> System.out.println("Completed"));
    }
}
