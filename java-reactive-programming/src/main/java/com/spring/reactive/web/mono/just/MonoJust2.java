package com.spring.reactive.web.mono.just;

import com.spring.reactive.web.dto.retail.Product;
import com.spring.reactive.web.service.RetailService;
import reactor.core.publisher.Mono;

import java.util.List;

public class MonoJust2 {
    public static void main(String[] args) {
        getProducts().subscribe(products -> System.out.println(products));
    }

    private static Mono<List<Product>> getProducts() {
        List<Product> products = RetailService.getProducts();
        return Mono.just(products);
    }
}
