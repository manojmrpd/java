package com.spring.reactive.web.mono.toFuture;

import reactor.core.publisher.Mono;

import java.util.concurrent.ExecutionException;

public class MonoToFuture1 {
    public static void main(String[] args) {
        try {
            String userName = Mono.fromCallable(() -> "Manoj Kumar").toFuture().get();
            System.out.println(userName);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
    }
}
