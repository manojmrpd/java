package com.spring.reactive.web.dto.stocks;

import lombok.*;

import java.time.Instant;

@Getter
@Setter
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
@ToString
public class ExchangeRate {

    private String baseCurrency;
    private String targetCurrency;
    private double rate;
    private Instant timestamp;

}
