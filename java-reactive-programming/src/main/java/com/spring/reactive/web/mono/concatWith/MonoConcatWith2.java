package com.spring.reactive.web.mono.concatWith;

import reactor.core.publisher.Mono;

public class MonoConcatWith2 {

    public static void main(String[] args) {

        //Combine Mono reactive stream with Mono or Flux using ConcatWith
        Mono.just("alex").concatWith(Mono.just("slice"))
                .map(String::toUpperCase)
                .filter(s -> s.length() <= 9)
                .subscribe(System.out::println);
    }
}
