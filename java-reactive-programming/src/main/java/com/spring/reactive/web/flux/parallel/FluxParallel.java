package com.spring.reactive.web.flux.parallel;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.ParallelFlux;
import reactor.core.scheduler.Schedulers;

import static java.lang.Thread.sleep;

@Slf4j
public class FluxParallel {

    /**
     * This publishOn() will execute only on 1 CPU core
     * It takes 3 seconds overall time and reduces performance
     * @return
     */
    public static Flux<String> publishOn() {
        return Flux.just("Alex", "Ben", "Charlie")
                .publishOn(Schedulers.parallel())
                .map(FluxParallel::toUpperCase)
                .log();

    }

    /**
     * This subscribeOn() will execute only on 1 CPU core
     * It takes 3 seconds overall time and reduces performance
     * @return
     */
    public static Flux<String> subscribeOn() {
        return Flux.just("Alex", "Ben", "Charlie")
                .subscribeOn(Schedulers.parallel())
                .map(FluxParallel::toUpperCase)
                .log();

    }

    /**
     * This parallel() will execute on all available 8 CPU cores
     * Reduces overall time and improves performance
     * Doesn't guarantee the order of elements
     * @return
     */
    public static ParallelFlux<String> parallel() {
        var noOfCpuCores = Runtime.getRuntime().availableProcessors();
        log.info("No of CPU cores {}", noOfCpuCores);
        return Flux.just("Alex", "Ben", "Charlie")
                .parallel()
                .runOn(Schedulers.parallel())
                .map(FluxParallel::toUpperCase)
                .log();
    }

    /**
     * This parallel() will execute on all available CPU cores
     * Reduces overall time and improves performance
     * Will guarantee the order of elements
     * @return
     */
    public static Flux<String> parallel_sequential() {
        return Flux.just("Alex", "Ben", "Charlie")
                .parallel()
                .runOn(Schedulers.parallel())
                .map(FluxParallel::toUpperCase)
                .sequential()
                .log();
    }

    /**
     * This parallel() will execute on all available CPU cores
     * Reduces overall time and improves performance
     * Doesn't guarantee the order of elements
     * @return
     */
    public static Flux<String> parallel_flatMap() {
        return Flux.just("Alex", "Ben", "Charlie")
                .flatMap(name -> Mono.just(name)
                        .map(FluxParallel::toUpperCase)
                        .subscribeOn(Schedulers.parallel()))
                .log();
    }

    /**
     * This parallel() will execute on all available CPU cores
     * Reduces overall time and improves performance
     * Will guarantee the order of elements
     * @return
     */
    public static Flux<String> parallel_flatMapSequential() {
        return Flux.just("Alex", "Ben", "Charlie")
                .flatMapSequential(name -> Mono.just(name)
                        .map(FluxParallel::toUpperCase)
                        .subscribeOn(Schedulers.parallel()))
                .log();
    }

    private static String toUpperCase(String s) {
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return s.toUpperCase();
    }
}
