package com.spring.reactive.web.concepts.executionmodelreactivestreams.onBackPressureError;

import com.spring.reactive.web.exceptions.StockPriceException;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

public class FluxOnBackPressureError {
    public static void main(String[] args) {
        // Simulated stream emitting elements at a high rate
        Flux<Integer> fastProducer = Flux.range(1, 1000)
                .map(integer -> {
                    if (integer == 500) {
                        throw new StockPriceException("Invalid value: " + integer);
                    } else {
                        return integer;
                    }
                })
                .subscribeOn(Schedulers.parallel()).log();

        // Slow consumer that processes each element with a delay of 10 milliseconds
        Flux<Integer> slowConsumer = Flux.range(1, 10)
                .delayElements(java.time.Duration.ofMillis(10));

        // Applying onBackpressureError() to handle backpressure by throwing an error
        fastProducer.onBackpressureError()
                .subscribe(System.out::println,
                        error -> System.err.println("Exception caught by onBackPressureError: " + error));

        // Subscribing the slow consumer
        slowConsumer.subscribe();

        // Wait for a while to see the backpressure handling in action
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
