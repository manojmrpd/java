package com.spring.reactive.web.flux.concat;

import reactor.core.publisher.Flux;

public class FluxConcat1 {
    public static void main(String[] args) {

        Flux.concat(Flux.just(1,2,3), Flux.just(4,5,6))
                .subscribe(System.out::println);
    }
}
