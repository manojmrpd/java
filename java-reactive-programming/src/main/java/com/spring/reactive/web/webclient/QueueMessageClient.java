package com.spring.reactive.web.webclient;

package com.manulife.ap.eventHandler;
import com.azure.messaging.servicebus.ServiceBusClientBuilder;
import com.azure.messaging.servicebus.ServiceBusProcessorClient;
import com.azure.messaging.servicebus.ServiceBusReceivedMessageContext;
import com.manulife.ap.config.ServiceBusConfig;
import com.manulife.ap.service.MessageService;
import com.manulife.ap.service.impl.IngRouterServiceImpl;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import static com.manulife.ap.constants.ApplicationConstants.JP_JCUS_ING_ROUTER_SERVICE;
@Component
@Slf4j
public class QueueMessageClient {
    @Autowired
    ServiceBusConfig serviceBusConfig;
    private ServiceBusProcessorClient serviceBusProcessorClient;
    @Autowired
    MessageService messageService;
    @PostConstruct
    public void initializeConnection()
    {
        if (serviceBusProcessorClient != null) {
            log.info("skipping this process trigger as previous process is still running");
            return;
        }
        log.info(JP_JCUS_ING_ROUTER_SERVICE + "Initialize Connection");
        serviceBusProcessorClient = new ServiceBusClientBuilder()
                .connectionString(serviceBusConfig.getListenConnectionString()).processor().disableAutoComplete()
                .queueName(serviceBusConfig.getQueueName())
                .processMessage(this::processMessage).processError(context -> messageService.processError(context))
                .buildProcessorClient();
        serviceBusProcessorClient.start();
    }
    private void processMessage(ServiceBusReceivedMessageContext context){
        messageService.processMessage(context);
    }
    public void connectionClose() {
        log.info("Closing Processor as there are no messages left in queue");
        serviceBusProcessorClient.close();
        serviceBusProcessorClient = null;
    }
}