package com.spring.reactive.web.concepts.combiningreactivestreams.merge;

import com.spring.reactive.web.service.RetailService;
import reactor.core.publisher.Flux;

public class FluxMerge3 {

    public static void main(String[] args) {

        //Combine Flux reactive stream with Mono or Flux using Merge
        var merge = Flux.merge(Flux.fromIterable(RetailService.getProducts()),
                Flux.fromIterable(RetailService.getOrders()));
        merge.subscribe(System.out::println);
    }
}
