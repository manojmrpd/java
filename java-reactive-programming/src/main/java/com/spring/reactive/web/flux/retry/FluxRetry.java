package com.spring.reactive.web.flux.retry;

import com.spring.reactive.web.exceptions.StockPriceException;
import reactor.core.publisher.Flux;

public class FluxRetry {
    public static void main(String[] args) {

        getStockPrice().subscribe(System.out::println);
    }

    private static Flux<Integer> getStockPrice() {
        return Flux.range(1, 10)
                .map(price -> {
                    if (price.equals(5)) {
                        throw new IllegalStateException("Invalid stock price");
                    }
                    return price;
                })
                .mergeWith(Flux.range(11, 10))
                .onErrorMap(ex -> new StockPriceException(ex.getMessage()))
                .retry(3)
                .log();
    }
}
