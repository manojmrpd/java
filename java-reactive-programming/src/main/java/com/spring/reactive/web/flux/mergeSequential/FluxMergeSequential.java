package com.spring.reactive.web.flux.mergeSequential;

import reactor.core.publisher.Flux;

public class FluxMergeSequential {

    public static void main(String[] args) {

        Flux<Integer> flux1 = Flux.just(1, 2, 3);
        Flux<Integer> flux2 = Flux.just(4, 5, 6);
        Flux.mergeSequential(flux1, flux2).subscribe(System.out::println);
    }
}
