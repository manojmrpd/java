package com.spring.reactive.web.concepts.creatingreactivestreams.fromSupplier;

import reactor.core.publisher.Mono;

public class MonoFromSupplier2 {
    public static void main(String[] args) {

        //You can use Mono.fromSupplier() with a caching mechanism (like cache() operator) to store the result of the supplier and avoid redundant calculations for subsequent subscribers.
        Mono<Long> systemUpTimeMono = Mono.fromSupplier(() -> System.currentTimeMillis()).cache();
        systemUpTimeMono.subscribe(time -> System.out.println(time),
                throwable -> System.out.println(throwable.getMessage()),
                () -> System.out.println("Completed"));
    }
}
