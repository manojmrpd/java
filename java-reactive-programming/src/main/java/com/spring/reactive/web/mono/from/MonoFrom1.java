package com.spring.reactive.web.mono.from;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class MonoFrom1 {
    public static void main(String[] args) {
        //Use Mono.from() to create a Mono from various sources like literal values, Callables, or existing Publishers.
        Flux<String> textFlux = Flux.just("manoj", "kumar", "sharma");
        Mono<String> textMono = Mono.from(textFlux.take(1));
        textMono.subscribe(text -> System.out.println(text),
                throwable -> System.out.println(throwable.getMessage()),
                () -> System.out.println("Completed"));
    }
}
