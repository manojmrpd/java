package com.spring.reactive.web.flux.subscribeOn;

import com.spring.reactive.web.flux.publishOn.FluxPublishOnBoundedElastic;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

import static java.lang.Thread.sleep;
@Slf4j
public class FluxSubscribeOnBoundedElastic {

    public Flux<String> subscribeOnBoundedElastic() {
        Flux<String> flux1 = Flux.just("Alex", "Bin", "Charlie")
                .map(FluxSubscribeOnBoundedElastic::toUpperCase)
                .subscribeOn(Schedulers.boundedElastic())
                .map(name -> {
                    log.info("Name in Flux1 {}", name);
                    return name;
                })
                .log();

        Flux<String> flux2 = Flux.just("Ded", "Egg", "Francis")
                .map(FluxSubscribeOnBoundedElastic::toUpperCase)
                .subscribeOn(Schedulers.boundedElastic())
                .map(name -> {
                    log.info("Name in Flux2 {}", name);
                    return name;
                }).log();

        return flux1.mergeWith(flux2);
    }

    private static String toUpperCase(String s) {
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return s.toUpperCase();
    }

}
