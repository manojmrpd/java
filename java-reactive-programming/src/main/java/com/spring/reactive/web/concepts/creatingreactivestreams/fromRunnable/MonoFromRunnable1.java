package com.spring.reactive.web.concepts.creatingreactivestreams.fromRunnable;

import com.spring.reactive.web.dto.retail.Product;
import com.spring.reactive.web.service.RetailService;
import reactor.core.publisher.Mono;

import java.util.List;

public class MonoFromRunnable1 {
    public static void main(String[] args) {
        Mono.fromRunnable(() -> {
            long start = System.currentTimeMillis();
            List<Product> products = RetailService.getProducts();
            System.out.println(products);
            long end = System.currentTimeMillis();
            long executionTime = end - start;
            System.out.println(executionTime);
        }).subscribe();
    }
}
