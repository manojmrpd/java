package com.spring.reactive.web.exceptions;


public class StockPriceException  extends RuntimeException {

    public StockPriceException(String message) {
        super(message);
    }
}
