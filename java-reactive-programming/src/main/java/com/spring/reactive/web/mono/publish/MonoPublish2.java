package com.spring.reactive.web.mono.publish;

import com.spring.reactive.web.service.RetailService;
import reactor.core.publisher.Mono;

public class MonoPublish2 {
    public static void main(String[] args) {
        Mono.fromCallable(() -> RetailService.getProducts())
                .publish(listMono -> listMono.single()).log()
                .subscribe(System.out::println);
    }
}
