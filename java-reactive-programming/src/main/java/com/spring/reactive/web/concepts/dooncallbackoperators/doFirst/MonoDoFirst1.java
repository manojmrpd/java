package com.spring.reactive.web.concepts.dooncallbackoperators.doFirst;

import com.spring.reactive.web.service.RetailService;
import reactor.core.publisher.Mono;

public class MonoDoFirst1 {
    public static void main(String[] args) {
        // Use doFirst() when you want to execute a specific action before a Mono starts processing its data
        Mono.fromCallable(() -> RetailService.getOrders())
                .doFirst(() -> {
                    System.out.println("Batch Job started");

                }).subscribe(System.out::println);
    }
}
