package com.spring.reactive.web.concepts.creatingreactivestreams.fromCallable;

import reactor.core.publisher.Mono;

public class MonoFromCallable3 {
    public static void main(String[] args) {
        Mono<String> textMono = Mono.fromCallable(() -> {
            Thread.sleep(1000);
            return "Hello User!";
        });
        textMono.subscribe(text -> System.out.println(text),
                throwable -> System.out.println(throwable.getMessage()),
                () -> System.out.println("Completed"));
    }
}
