package com.spring.reactive.web.concepts.creatingreactivestreams.fromIterable;

import reactor.core.publisher.Flux;

import java.util.List;

public class FluxFromIterable1 {
    public static void main(String[] args) {
        Flux.fromIterable(List.of("Alex", "Slice", "Lucid"))
                .flatMap(FluxFromIterable1::splitText)
                .subscribe(System.out::println);
    }

    private static Flux<?> splitText(String s) {
        List<String> charlist = List.of(s.split(""));
        return Flux.fromIterable(charlist);
    }
}
