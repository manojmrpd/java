package com.spring.reactive.web.flux.zipWith;

import reactor.core.publisher.Flux;
import reactor.util.function.Tuple2;

public class FluxZipWith1 {

    public static void main(String[] args) {
        Flux<Integer> flux1 = Flux.just(1, 2, 3);
        Flux<Integer> flux2 = Flux.just(4, 5, 6, 7, 8);
        flux1.zipWith(flux2).subscribe(System.out::println);
    }
}
