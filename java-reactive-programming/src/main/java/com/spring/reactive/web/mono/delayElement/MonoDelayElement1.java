package com.spring.reactive.web.mono.delayElement;

import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;

public class MonoDelayElement1 {
    public static void main(String[] args) {
        //Mono.delay() used to delay before emitting the value or error signal from a Mono.
        Mono.fromCallable(() -> "manoj@gmail.com").log()
                .delayElement(Duration.ofMillis(10)).log()
                .map(String::toUpperCase)
                .subscribe(System.out::println);
    }
}
