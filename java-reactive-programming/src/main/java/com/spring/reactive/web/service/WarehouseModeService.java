package com.spring.reactive.web.service;

import com.costco.mobile.bff.whmode.commons.constants.WarehouseModeConstants;
import com.costco.mobile.bff.whmode.commons.util.WarehouseModeServiceUtil;
import com.costco.mobile.bff.whmode.domain.entity.IOHData;
import com.costco.mobile.bff.whmode.domain.entity.IOHNearByWarehouseRequest;
import com.costco.mobile.bff.whmode.domain.entity.IOHSearchRequest;
import com.costco.mobile.bff.whmode.domain.entity.ioh.IOHItem;
import com.costco.mobile.bff.whmode.domain.entity.ioh.IOHNearByWarehouse;
import com.costco.mobile.bff.whmode.domain.service.adapter.BrandFolderAdapterService;
import com.costco.mobile.bff.whmode.domain.service.adapter.IOHNearByItemAdapterService;
import com.costco.mobile.bff.whmode.domain.service.adapter.IOHSearchByDescriptionAdapterService;
import com.costco.mobile.bff.whmode.domain.service.adapter.IOHSearchByIdAdapterService;
import com.costco.mobile.bff.whmode.domain.service.adapter.WarehouseSearchByIdAdapterService;
import com.costco.mobile.bff.whmode.domain.service.adapter.util.BrandFolderUtil;
import com.costco.mobile.bff.whmode.domain.service.adapter.vo.ioh.IOHNearByItemResponse;
import com.costco.mobile.bff.whmode.domain.service.adapter.vo.ioh.IOHSearchResponse;
import com.costco.mobile.bff.whmode.infrastructure.cache.service.CacheService;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


import static com.costco.mobile.bff.whmode.commons.constants.WarehouseModeConstants.REGION_CD_BD;
import static com.costco.mobile.bff.whmode.commons.constants.WarehouseModeConstants.REGION_CD_USBC;
import static com.costco.mobile.bff.whmode.commons.constants.WarehouseModeConstants.REGION_CD_USBD;


@Slf4j
@Service
public class WarehouseModeService {

    private IOHSearchByDescriptionAdapterService iohSearchService;
    private IOHNearByItemAdapterService iohNearByItemService;
    private WarehouseSearchByIdAdapterService warehouseSearchByIdAdapterService;
    private CacheService cacheService;
    private BrandFolderAdapterService brandFolderService;
    private WarehouseModeServiceUtil warehouseServiceUtil;
    private final IOHSearchByIdAdapterService searchByIdAdapterService;

    @Value("${spring.data.redis.redisEnabled}")
    private boolean redisEnabled;

    @Value("${service.brandfolder.enabled}")
    private boolean isBrandFolderFlowEnabled;
    @Autowired
    BrandFolderUtil brandFolderUtil;
    private static final String LOCALE_US = "EN-US";
    private static final String LOCALE_FR = "FR-CA";
    private static final String IMAGE_URL_EN_US = "https://cdn.bfldr.com/U447IH35/at/jvmq497f3mhhbb86q3wx349w/no-image-available-en.png?auto=webp&format=jpg";
    private static final String IMAGE_URL_FR_CA = "https://cdn.bfldr.com/U447IH35/at/qpbjt4kk6swfz984qxwn6b/no-image-available-fr.png?auto=webp&format=jpg";
    private final Scheduler scheduler = Schedulers.newParallel("WarehouseModeService", 10);

    public WarehouseModeService(IOHSearchByDescriptionAdapterService iohSearchService,
                                IOHNearByItemAdapterService iohNearByItemService, CacheService cacheService,
                                BrandFolderAdapterService brandFolderService, WarehouseModeServiceUtil warehouseServiceUtil,
                                WarehouseSearchByIdAdapterService warehouseSearchByIdAdapterService,
                                IOHSearchByIdAdapterService searchByIdAdapterService, BrandFolderUtil brandFolderUtil) {
        this.iohSearchService = iohSearchService;
        this.iohNearByItemService = iohNearByItemService;
        this.cacheService = cacheService;
        this.brandFolderService = brandFolderService;
        this.warehouseServiceUtil = warehouseServiceUtil;
        this.warehouseSearchByIdAdapterService = warehouseSearchByIdAdapterService;
        this.searchByIdAdapterService = searchByIdAdapterService;
        this.brandFolderUtil = brandFolderUtil;
    }

    public Mono<IOHData> getIOHData(IOHSearchRequest itemIOHRequest) {


        return getIOHDataItems(itemIOHRequest).flatMap(iohData -> {
            if (CollectionUtils.isEmpty(iohData.getItems())) {
                return getIOHDataErrorScenario();
            }
            return fetchImageData(iohData.getItems(), itemIOHRequest.getLocale()).map(
                    data -> {
                        iohData.setItems(data);
                        return iohData;
                    }
            );
        });
    }

    public Mono<IOHData> getIOHDataItems(IOHSearchRequest itemIOHRequest) {
        itemIOHRequest.setSearchTerm(itemIOHRequest.getSearchTerm().trim());
        if (!itemIOHRequest.getSearchTerm().matches("\\d+")) {
            log.info("AlphaNumeric search term. Invoking Search by Description.");
            return invokeSearchByDescription(itemIOHRequest)
                    .publishOn(scheduler);
        }
        log.info("Numeric search term. Invoking Search by Id.");
        return searchByIdAdapterService.getSearchByIdData(itemIOHRequest)
                .flatMap(response -> {
                    if (CollectionUtils.isEmpty(response.getItem())) {
                        log.info("IOH Search By Id returned an empty result. Invoking Search by Description as fallback");
                        return invokeSearchByDescription(itemIOHRequest);
                    } else {
                        if (!StringUtils.hasText(itemIOHRequest.getSelectedWarehouse())) {
                            return Mono.just(populateIOHData(response));
                        }
                        return populateIOHAndWarehouseData(itemIOHRequest, populateIOHData(response));
                    }


                })
                .onErrorResume(this::handleIOHSearchServiceError)
                .publishOn(scheduler);


    }

    private Mono<IOHData> invokeSearchByDescription(IOHSearchRequest itemIOHRequest) {
        return iohSearchService.getItemsForDescription(itemIOHRequest)
                .map(this::populateIOHData)
                .onErrorResume(this::handleIOHSearchServiceError);
    }

    private Mono<IOHData> populateIOHAndWarehouseData(IOHSearchRequest itemIOHRequest, IOHData response) {
        if (StringUtils.hasText(itemIOHRequest.getSelectedWarehouse())) {
            return Mono.zip(getWarehouseDetails(itemIOHRequest.getSelectedWarehouse()), getWarehouseDetails(itemIOHRequest.getCurrentWarehouse())).
                    map(tuple -> populateWarehouseDetails(tuple.getT1(), tuple.getT2(), response));
        } else {
            return getWarehouseDetails(itemIOHRequest.getCurrentWarehouse()).map(
                    data -> populateWarehouseDetails(data, null, response)
            );
        }


    }

    private IOHData populateWarehouseDetails(JsonNode itemWarehouseDetails, JsonNode defaultWarehouseDetails, IOHData response) {
        if (itemWarehouseDetails != null) {
            warehouseServiceUtil.populateWarehouseDataInResponse(itemWarehouseDetails, defaultWarehouseDetails, response);
        }


        return response;
    }

    private Mono<JsonNode> getWarehouseDetails(String warehouseNumber) {
        return cacheService.findById(WarehouseModeConstants.WAREHOUSE_CACHE_PREFIX, warehouseNumber)
                .map(cachedData -> {
                    if (cachedData != null) {
                        log.info("Found warehouse entry in cache for warehouse {}", warehouseNumber);
                    }
                    return cachedData;
                })
                .onErrorResume(throwable -> {
                    log.error("Error while fetching the warehouse data from cache" + throwable);
                    return Mono.empty();
                }).switchIfEmpty(
                        Mono.defer(() -> warehouseSearchByIdAdapterService.getWHSearchData(warehouseNumber)
                                .flatMap(warehouseAPIResponse -> {
                                    log.info("Fetched warehouse information from origin for : {}", warehouseNumber);
                                    ObjectNode addressJson = (ObjectNode) warehouseAPIResponse
                                            .get(WarehouseModeConstants.ADDRESS);
                                    return cacheService
                                            .save(WarehouseModeConstants.WAREHOUSE_CACHE_PREFIX,
                                                    warehouseNumber, addressJson,
                                                    WarehouseModeConstants.WH_REDIS_EXPIRY_MINS)
                                            .doOnError(
                                                    throwable -> log.error("Error saving warehouse: " + throwable))
                                            .onErrorResume(throwable -> Mono.empty())
                                            .thenReturn(addressJson);
                                })
                                .onErrorResume(error -> Mono.just(JsonNodeFactory.instance.objectNode()))));
    }


    private IOHData populateIOHData(IOHSearchResponse response) {
        IOHData iOHResponse = new IOHData();
        if (response != null && !CollectionUtils.isEmpty(response.getItem())) {
            iOHResponse.setItems(warehouseServiceUtil.mapIOHData(response.getItem()));
            iOHResponse.setCount(response.getTotalResultCount());
        } else {
            iOHResponse.setItems(new ArrayList<>());
            iOHResponse.setErrorMessage("No Item Found for Search Term");
        }
        return iOHResponse;
    }


    private Mono<IOHData> handleIOHSearchServiceError(Throwable error) {
        log.error("An Error occurred in invoking IOH Search Service", error);
        return getIOHDataErrorScenario();
    }


    @NotNull
    private static Mono<IOHData> getIOHDataErrorScenario() {
        IOHData errorResponse = new IOHData();
        errorResponse.setItems(new ArrayList<>());
        errorResponse.setErrorMessage("Could not retrieve data from IOH service");
        return Mono.just(errorResponse);
    }


    private List<IOHItem> populateIOHResponse(IOHSearchResponse response) {
        if (response != null && !CollectionUtils.isEmpty(response.getItem())) {
            return warehouseServiceUtil.mapIOHData(response.getItem());
        }
        return new ArrayList<>();
    }

    private List<IOHNearByWarehouse> populateIOHNearByItemResponse(IOHNearByItemResponse response) {
        if (response != null && !CollectionUtils.isEmpty(response.getItem())) {
            return warehouseServiceUtil.mapIOHNearByWareHouseData(response.getItem());
        }
        return new ArrayList<>();
    }

    private Mono<List<IOHItem>> handleIOHSearchError(Throwable error) {
        log.error("An Error occurred in invoking IOH Search Service", error);
        IOHData errorResponse = new IOHData();
        errorResponse.setErrorMessage("Could not retrieve data from IOH Search service");
        return Mono.just(new ArrayList<>());
    }


    private Mono<List<IOHItem>> fetchImageData(List<IOHItem> itemList, String locale) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        mapper.setVisibility(VisibilityChecker.Std.defaultInstance().withFieldVisibility(JsonAutoDetect.Visibility.ANY));
        return Flux.fromIterable(itemList.stream().filter(item -> item.getItemNumber() != null).toList())
                .concatMap(item -> brandFolderService.getItemImageData(deriveRegion(item.getRegion()), item.getItemNumber())
                        .map(brandFolderItemResponse -> {
                            if (!ObjectUtils.isEmpty(brandFolderItemResponse)) {
                                Optional<String> cdnURL = brandFolderUtil.findCdnURLForPriorityOneHash(brandFolderItemResponse);
                                item.setImageUrl((cdnURL.isPresent()) ? cdnURL.get() : getDefaultImage(locale));
                            }
                            return item;
                        })
                        .onErrorResume(error -> handleBrandFolderServiceError(item, error, locale)))
                .collectList();
    }

    public String deriveRegion(String region) {
        region = (null != region) ? region : REGION_CD_USBC;
        return (region.equals(REGION_CD_BD)) ? REGION_CD_USBD : REGION_CD_USBC;
    }


    private String getDefaultImage(String locale) {
        return switch (locale.toUpperCase()) {
            case LOCALE_US -> IMAGE_URL_EN_US;
            case LOCALE_FR -> IMAGE_URL_FR_CA;
            default -> IMAGE_URL_EN_US;
        };
    }

    private Mono<IOHItem> handleBrandFolderServiceError(IOHItem item, Throwable error, String locale) {
        item.setImageUrl(getDefaultImage(locale));
        log.error("ERROR: Could not retrieve data from Brand Folder Service {}", error);
        return Mono.just(item);
    }

    private Mono<IOHNearByWarehouse> handleWarehouseSearchServiceError(IOHNearByWarehouse item, Throwable error) {
        log.error("ERROR: Could not retrieve data from Warehouse API", error);
        item.setError("ERROR: Could not populate warehouse address details due to an error");
        return Mono.just(item);
    }


    public Mono<IOHData> getNearByWareHouseData(IOHNearByWarehouseRequest nearByitemIOHRequest) {
        if (isValidCoordinatesInReq(nearByitemIOHRequest)) {
            log.info(
                    "Found latitude and longitude in the request,setting latLongFlow=true;for custom distance calculation");
            nearByitemIOHRequest.setLatLongFlow(true);
        }
        return iohNearByItemService
                .getNearByItem(nearByitemIOHRequest.getCurrentWarehouse(), nearByitemIOHRequest.getItemNumber())
                .map(this::populateIOHNearByItemResponse)
                .onErrorResume(error -> handleIOHNearByItemError(error))
                .flatMap(itemList -> {
                    IOHData iOHData = new IOHData();
                    if (CollectionUtils.isEmpty(itemList)) {
                        iOHData.setWarehouses(new ArrayList<>());
                        iOHData.setErrorMessage("Could not retrieve data from IOH Near By Warehouse Service");
                        return Mono.just(iOHData);
                    }
                    return fetchWarehouseData(itemList, nearByitemIOHRequest).map(data -> {
                        if (nearByitemIOHRequest.isLatLongFlow()) {
                            log.info("LatLongFlow=true,Valid coordinates present in the request,using custom sorting");
                            iOHData.setWarehouses(warehouseServiceUtil.sortWarehousesByDistance(data));
                        } else {
                            log.info("LatLongFlow=false,No coordinates present in the request,using IOH default sorting");
                            iOHData.setWarehouses(data);
                        }
                        return iOHData;
                    });


                });


    }

    public Mono<List<IOHNearByWarehouse>> fetchNearByItems(IOHNearByWarehouseRequest nearByItemIOHRequest) {
        return iohNearByItemService.getNearByItem(nearByItemIOHRequest.getCurrentWarehouse(), nearByItemIOHRequest.getItemNumber())
                .map(this::populateIOHNearByItemResponse)
                .onErrorResume(this::handleIOHNearByItemError);
    }


    private boolean isValidCoordinatesInReq(IOHNearByWarehouseRequest nearByReq) {
        return nearByReq.getLatitude() != 0 && nearByReq.getLongitude() != 0;
    }

    private Mono<List<IOHNearByWarehouse>> fetchWarehouseData(List<IOHNearByWarehouse> itemList,
                                                              IOHNearByWarehouseRequest nearByItemIOHRequest) {
        return Flux
                .fromIterable(itemList.stream().filter(item -> item.getLocation() != null).toList())
                .concatMap(item -> cacheService.findById(WarehouseModeConstants.WAREHOUSE_CACHE_PREFIX, item.getLocation())
                        .onErrorResume(throwable -> {
                            log.error("Error while fetching the warehouse data from cache", throwable);
                            return Mono.empty();
                        })
                        .flatMap(warehouseCachedResponse -> {
                            ObjectNode addressJson = (ObjectNode) warehouseCachedResponse;
                            log.info("Found warehouse entry in cache : " + item.getLocation());
                            return Mono.just(warehouseServiceUtil.processWarehouseResponse(nearByItemIOHRequest,
                                    item, addressJson));
                        }).switchIfEmpty(
                                Mono.defer(() ->
                                        warehouseSearchByIdAdapterService.getWHSearchData(item.getLocation())
                                                .flatMap(warehouseAPIResponse -> {
                                                    log.info("Fetched warehouse information(fetchWarehouseData) from origin for : {}", item.getLocation());
                                                    ObjectNode addressJson = (ObjectNode) warehouseAPIResponse
                                                            .get(WarehouseModeConstants.ADDRESS);
                                                    return cacheService
                                                            .save(WarehouseModeConstants.WAREHOUSE_CACHE_PREFIX,
                                                                    item.getLocation(), addressJson,
                                                                    WarehouseModeConstants.WH_REDIS_EXPIRY_MINS)
                                                            .doOnError(
                                                                    throwable -> log.error("Error saving warehouse: ", throwable))
                                                            .onErrorResume(throwable -> Mono.empty())
                                                            .thenReturn(warehouseServiceUtil.processWarehouseResponse(
                                                                    nearByItemIOHRequest, item, addressJson));


                                                }).onErrorResume(throwable -> Mono.empty())))).collectList();
    }

    private Mono<List<IOHNearByWarehouse>> handleIOHNearByItemError(Throwable error) {
        log.error("An Error occurred in invoking IOH Near By Warehouse Service", error);
        IOHData errorResponse = new IOHData();
        errorResponse.setErrorMessage("Could not retrieve data from IOH Near By Warehouse Service");
        return Mono.just(new ArrayList<>());
    }
}
