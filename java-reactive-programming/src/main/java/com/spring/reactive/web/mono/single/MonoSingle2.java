package com.spring.reactive.web.mono.single;

import com.spring.reactive.web.dto.retail.Product;
import com.spring.reactive.web.service.RetailService;
import reactor.core.publisher.Mono;

import java.util.List;

public class MonoSingle2 {

    public static final String ELECTRONICS = "Electronics";

    public static void main(String[] args) {
        Mono<List<Product>> firstElectronicProduct =
                Mono.fromCallable(() -> RetailService.getProducts())
                        .filter(products -> products.stream().anyMatch(product -> product.getCategory().equalsIgnoreCase(ELECTRONICS)))
                        .single().log();
        firstElectronicProduct.subscribe(products -> System.out.println(products),
                throwable -> throwable.getMessage(),
                () -> System.out.println("Completed"));
    }
}
