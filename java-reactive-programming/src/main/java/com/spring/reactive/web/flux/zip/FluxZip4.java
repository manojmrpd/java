package com.spring.reactive.web.flux.zip;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class FluxZip4 {

    public static void main(String[] args) {

        //Combine Flux reactive stream with Mono using Zip
        Flux<Integer> flux = Flux.just(1, 2, 3);
        Mono<Integer> mono = Mono.just(10);
        Flux<Integer> combinedFlux = Flux.zip(flux, mono)
                .map(tuple -> tuple.getT1() * tuple.getT2());
        combinedFlux.subscribe(System.out::println);
    }
}
