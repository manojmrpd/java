package com.spring.reactive.web.mono.fromSupplier;

import com.spring.reactive.web.dto.retail.Product;
import com.spring.reactive.web.service.RetailService;
import reactor.core.publisher.Mono;

public class MonoFromSupplier1 {
    public static void main(String[] args) {
        Mono<Product> productMono = getProductById(10);
        productMono.subscribe(product -> System.out.println(product),
                throwable -> System.out.println(throwable.getMessage()),
                () -> System.out.println("Completed"));
    }

    private static Mono<Product> getProductById(int i) {
        return Mono.fromSupplier(() -> RetailService.getProducts().stream().findFirst().orElseThrow());
    }
}
