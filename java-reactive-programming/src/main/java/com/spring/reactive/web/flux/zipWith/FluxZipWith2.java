package com.spring.reactive.web.flux.zipWith;

import com.spring.reactive.web.dto.stocks.ExchangeRate;
import com.spring.reactive.web.dto.stocks.StockPrice;
import com.spring.reactive.web.service.StockService;
import reactor.core.publisher.Flux;

import java.util.stream.Collectors;

public class FluxZipWith2 {

    public static void main(String[] args) {
        Flux.fromIterable(StockService.getStockPrices().stream().map(StockPrice::getPrice).collect(Collectors.toUnmodifiableList()))
                .zipWith(Flux.fromIterable(StockService.getExchangeRates().stream().map(ExchangeRate::getRate).collect(Collectors.toUnmodifiableList())))
                .map(tuple -> tuple.getT1() * tuple.getT2())
                .subscribe(System.out::println);
    }
}
