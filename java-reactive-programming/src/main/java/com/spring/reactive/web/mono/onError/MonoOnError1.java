package com.spring.reactive.web.mono.onError;

import com.spring.reactive.web.dto.retail.Product;
import com.spring.reactive.web.service.RetailService;
import reactor.core.publisher.Mono;

import java.util.List;

public class MonoOnError1 {
    public static void main(String[] args) {
        Mono<List<Product>> listMono = Mono.just(RetailService.getProducts());
        listMono.filter(products -> products.stream().anyMatch(product -> product.getCategory().equalsIgnoreCase("Electronics")))
                .flatMap(products -> Mono.just(products))
                .subscribe(products -> System.out.println(products),
                        throwable -> System.out.println(throwable.getMessage()),
                        () -> System.out.println("Completed Successfully"));
    }
}
