package com.spring.reactive.web.concepts.executionmodelreactivestreams.publishOn;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

import static java.lang.Thread.sleep;

@Slf4j
public class FluxPublishOnBoundedElastic {

    public Flux<String> publishOnBoundedElastic() {
        Flux<String> flux1 = Flux.just("Alex", "Bin", "Charlie")
                .publishOn(Schedulers.boundedElastic())
                .map(FluxPublishOnBoundedElastic::toUpperCase)
                .map(name -> {
                    log.info("Name in Flux1 {}", name);
                    return name;
                })
                .log();

        Flux<String> flux2 = Flux.just("Ded", "Egg", "Francis")
                .publishOn(Schedulers.boundedElastic())
                .map(FluxPublishOnBoundedElastic::toUpperCase)
                .map(name -> {
                    log.info("Name in Flux2 {}", name);
                    return name;
                }).log();

        return flux1.mergeWith(flux2);
    }

    private static String toUpperCase(String s) {
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return s.toUpperCase();
    }
}
