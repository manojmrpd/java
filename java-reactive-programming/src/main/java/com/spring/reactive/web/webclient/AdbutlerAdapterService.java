package com.spring.reactive.web.webclient;
import com.costco.mobile.bff.homepage.commons.constants.HomeBFFConstants;
import com.costco.mobile.bff.homepage.commons.exception.AdbutlerServiceException;
import com.costco.mobile.bff.homepage.commons.exception.AuthException;
import com.costco.mobile.bff.homepage.commons.exception.GlobalServiceException;
import com.costco.mobile.bff.homepage.domain.entity.HomePageEntity;
import com.fasterxml.jackson.databind.JsonNode;
import io.github.resilience4j.circuitbreaker.CallNotPermittedException;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import io.github.resilience4j.timelimiter.annotation.TimeLimiter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

/**
 * Adapter Service to Get adbutler response
 */
@Service
@Slf4j
public class AdbutlerAdapterService {
    private final WebClient webClient;

    private final Scheduler scheduler = Schedulers.newParallel("AdbutlerAdapterService", 10);
    @Autowired
    public AdbutlerAdapterService(@Value("${service.adbutler.url}") String url) {
        this.webClient = WebClient.builder()
                .clientConnector(new ReactorClientHttpConnector())
                .baseUrl(url).build();
    }


    @TimeLimiter(name = "adbutler", fallbackMethod = "handleTimeout")
    @CircuitBreaker(name = "adbutler", fallbackMethod = "handleCircuitBreaker")
    @Retry(name = "adbutler")
    public Mono<JsonNode> getAdbutlerData(JsonNode adbutlerRequest) {
        return webClient.post().body(BodyInserters.fromValue(adbutlerRequest))
                .accept(MediaType.APPLICATION_JSON)
                .exchangeToMono(response -> {
                    if (response.statusCode().is2xxSuccessful()) {
                        return response.bodyToMono(JsonNode.class)
                                .doOnNext(adButlerResponse -> log.debug("Adapter-api-response-Adbutler-received: {}", adButlerResponse));
                    } else if (response.statusCode().is4xxClientError()) {
                        return response.createException().map(ex -> {
                            log.error("AdapterException-Adbutler-Service 4xx : StatusCode - {}, Body - {}, Headers - {}",
                                    response.statusCode().value(), ex.getResponseBodyAsString(), ex.getHeaders());
                            if(response.statusCode().value() == 401 || response.statusCode().value() == 403){
                                throw new AuthException(HttpStatus.valueOf(response.statusCode().value()), "Adbutler Service 4xx Exception");
                            }else{
                                throw new GlobalServiceException(HttpStatus.valueOf(response.statusCode().value()), "Adbutler Service 4xx Exception");
                            }
                        });
                    } else if (response.statusCode().is5xxServerError()) {
                        return response.createException().map(ex -> {
                            log.error("AdapterException-Adbutler-Service 5xx : StatusCode - {}, Body - {}, Headers - {}",
                                    response.statusCode().value(), ex.getResponseBodyAsString(), ex.getHeaders());
                            throw new AdbutlerServiceException(HttpStatus.valueOf(response.statusCode().value()), "Adbutler Service 5xx Exception");
                        });
                    } else {
                        return response.createException()
                                .flatMap(exp -> {
                                    log.error("AdapterException-Adbutler-Service : StatusCode - {}, Body - {}, Headers - {}",
                                            response.statusCode().value(), exp.getResponseBodyAsString(), exp.getHeaders());
                                    return  Mono.error(exp);
                                });
                    }
                }).publishOn(scheduler);
    }

    public Mono<HomePageEntity> handleCircuitBreaker(CallNotPermittedException ex) {
        log.error("Call Not Permitted Exception - Adbutler Calls not Permitted this time. Circuit open",ex);
        return Mono.error(new AdbutlerServiceException(HttpStatus.INTERNAL_SERVER_ERROR, HomeBFFConstants.ADBUTLER_SERVICE_EXCEPTION));
    }

    public Mono<HomePageEntity> handleTimeout(java.util.concurrent.TimeoutException ex) {
        log.error("Timeout Exception - Adbutler api timing out",ex);
        return Mono.error(new AdbutlerServiceException(HttpStatus.GATEWAY_TIMEOUT, HomeBFFConstants.ADBUTLER_SERVICE_EXCEPTION));
    }
}
