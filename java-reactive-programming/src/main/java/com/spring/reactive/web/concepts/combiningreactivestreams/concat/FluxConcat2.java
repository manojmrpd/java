package com.spring.reactive.web.concepts.combiningreactivestreams.concat;

import com.spring.reactive.web.service.RetailService;
import reactor.core.publisher.Flux;

public class FluxConcat2 {
    public static void main(String[] args) {
        Flux.concat(Flux.just(RetailService.getCategories()),
                Flux.just(RetailService.getProducts()))
                .subscribe(System.out::println);
    }
}
