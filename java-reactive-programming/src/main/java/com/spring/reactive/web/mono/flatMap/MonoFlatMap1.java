package com.spring.reactive.web.mono.flatMap;

import com.spring.reactive.web.mono.map.MonoMap1;
import reactor.core.publisher.Mono;

import java.util.List;

public class MonoFlatMap1 {
    public static void main(String[] args) {
        Mono.just("text").map(String::toUpperCase)
                .flatMap(MonoFlatMap1::splitText)
                .subscribe(System.out::println);
    }

    private static Mono<?> splitText(String s) {
        List<String> charlist = List.of(s.split(""));
        return Mono.just(charlist);
    }
}
