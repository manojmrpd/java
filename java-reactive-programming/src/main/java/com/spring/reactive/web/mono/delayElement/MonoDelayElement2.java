package com.spring.reactive.web.mono.delayElement;

import com.spring.reactive.web.service.RetailService;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;

public class MonoDelayElement2 {
    public static void main(String[] args) {
        // Use Mono.delayElement() for delays within a Mono chain, allowing you to control the timing of emissions relative to other operations.
        Mono.fromCallable(() -> RetailService.getProducts()).log()
                .delayElement(Duration.ofMillis(1), Schedulers.single()).log()
                .subscribe();
    }
}
