package com.spring.reactive.web.concepts.exceptionhandlingreactivestreams.onErrorResume;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
@Slf4j
public class FluxOnErrorResume {
    public static void main(String[] args) {
        getStockPrice().subscribe(System.out::println);
    }

    private static Flux<Integer> getStockPrice() {
        return Flux.range(1, 10)
                .concatWith(Flux.error(new IllegalStateException("Invalid stock price")))
                .onErrorResume(ex -> {
                    log.error("Exception occurred", ex);
                    if (ex instanceof IllegalStateException) {
                        return Flux.range(11, 10);
                    } else {
                        return Flux.error(ex);
                    }
                }).log();
    }
}
