package com.spring.reactive.web.mono.timeout;

import reactor.core.publisher.Mono;

import java.time.Duration;

public class MonoTimeout {
    public static void main(String[] args) {
        // Use Mono.timeout() to establish time limits for Mono operations, preventing them from hanging indefinitely
        Mono.fromCallable(() -> "Manoj")
                .timeout(Duration.ofMillis(10)).log()
                .subscribe(System.out::println);
    }
}
