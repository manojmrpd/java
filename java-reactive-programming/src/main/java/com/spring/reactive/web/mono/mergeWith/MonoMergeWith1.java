package com.spring.reactive.web.mono.mergeWith;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class MonoMergeWith1 {

    public static void main(String[] args) {
        Mono.just(1).mergeWith(Flux.just(2, 3, 4, 5))
                .filter(number -> number % 2 == 0)
                .subscribe(System.out::println);
    }
}
