package com.spring.reactive.web.concepts.exceptionhandlingreactivestreams.onErrorMap;

import com.spring.reactive.web.exceptions.StockPriceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;

public class FluxOnErrorMap {

    private static final Logger log = LoggerFactory.getLogger(FluxOnErrorMap.class);

    public static void main(String[] args) {
        getStockPrice().subscribe(System.out::println);
    }

    private static Flux<Integer> getStockPrice() {
        return Flux.range(1, 10)
                .map(price -> {
                    if (price.equals(5)) {
                        throw new IllegalStateException("Invalid stock price");
                    }
                    return price;
                })
                .mergeWith(Flux.range(11, 10))
                .onErrorMap(ex -> {
                    return new StockPriceException(ex.getMessage());
                }).log();
    }

}
