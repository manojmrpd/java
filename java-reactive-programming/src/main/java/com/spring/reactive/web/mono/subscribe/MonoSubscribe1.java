package com.spring.reactive.web.mono.subscribe;

import reactor.core.publisher.Mono;

public class MonoSubscribe1 {
    public static void main(String[] args) {
        Mono<String> helloMono = Mono.just("Hello");
        helloMono.subscribe(
                value -> System.out.println("Received: " + value),
                error -> System.err.println("Error: " + error.getMessage()),
                () -> System.out.println("Completed!")
        );
    }
}
