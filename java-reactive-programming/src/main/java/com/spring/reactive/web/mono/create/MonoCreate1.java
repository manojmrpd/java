package com.spring.reactive.web.mono.create;

import com.spring.reactive.web.dto.retail.Product;
import com.spring.reactive.web.service.RetailService;
import reactor.core.publisher.Mono;

import java.util.List;

public class MonoCreate1 {
    public static void main(String[] args) {
        // Mono.create() offers flexibility for working with callback-based APIs or defining custom emission logic within your Mono
        Mono.create(monoSink -> {
            List<Product> products = RetailService.getProducts();
            Product product = products.stream().findFirst().orElseThrow();
            if (null != product) {
                Mono.just(product);
                System.out.println(product);
            } else {
                Mono.error(new RuntimeException("Runtime Exception"));
            }
        }).subscribe(System.out::println);
    }
}
