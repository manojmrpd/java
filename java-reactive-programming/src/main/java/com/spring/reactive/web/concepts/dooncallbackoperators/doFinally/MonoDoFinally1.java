package com.spring.reactive.web.concepts.dooncallbackoperators.doFinally;

import reactor.core.publisher.Mono;

public class MonoDoFinally1 {
    public static void main(String[] args) {
        // Use doFinally regardless of whether the Mono completes successfully, encounters an error, or gets cancelled.
        Mono.fromCallable(() -> "Manoj")
                .doOnSuccess(s -> System.out.println(s))
                .doFinally(signalType -> System.out.println("Completed"))
                .subscribe();
    }
}
