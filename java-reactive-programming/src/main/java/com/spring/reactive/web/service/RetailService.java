package com.spring.reactive.web.service;

import com.spring.reactive.web.dto.retail.Order;
import com.spring.reactive.web.dto.retail.Product;
import reactor.core.publisher.Flux;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class RetailService {

    String category;

    public static List<Product> getProducts() {
        Product product1 = new Product(1, "Laptop", 1000.0, "Electronics", 20, true);
        Product product2 = new Product(2, "Phone", 800.0, "Electronics", 35, true);
        Product product3 = new Product(3, "Speaker", 300.00, "Electronics", 0, false);
        Product product4 = new Product(4, "Ear Phones", 200.00, "Electronics", 0, false);
        Product product5 = new Product(5, "T-Shirt", 100.00, "Fashion", 30, true);
        Product product6 = new Product(6, "Jeans", 90.00, "Fashion", 45, true);
        Product product7 = new Product(7, "Watch", 290.00, "Fashion", 0, false);
        Product product8 = new Product(8, "T-Shirt", 120.00, "Fashion", 12, true);
        return Arrays.asList(product1, product2, product3, product4, product5, product6, product7
                , product8);
    }

    public static List<Order> getOrders() {
        Order order1 = new Order(101, "Apple Iphone-13", 1250.00, LocalDate.now(), getProducts());
        Order order2 = new Order(102, "Samsung S7 Edge", 950.00, LocalDate.now(), getProducts());
        return Arrays.asList(order1, order2);
    }

    public static Set<String> getCategories() {
        return RetailService.getProducts().stream().map(product -> product.getCategory()).collect(Collectors.toSet());
    }

    public static Flux<Void> saveProducts(Product product) {
        System.out.println("Saved products " + product);
        return Flux.empty();
    }

    public List<Product> getProductList() {
        Product product1 = new Product(1, "Laptop", 1000.0, category, 20, true);
        Product product2 = new Product(2, "Phone", 800.0, category, 35, true);
        Product product3 = new Product(3, "Speaker", 300.00, category, 0, false);
        Product product4 = new Product(4, "Ear Phones", 200.00, category, 0, false);
        return Arrays.asList(product1, product2, product3, product4);
    }
}
