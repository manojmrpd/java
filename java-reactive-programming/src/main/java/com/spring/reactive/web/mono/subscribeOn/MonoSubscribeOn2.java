package com.spring.reactive.web.mono.subscribeOn;

import com.spring.reactive.web.dto.retail.Product;
import com.spring.reactive.web.service.RetailService;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.List;

public class MonoSubscribeOn2 {
    public static void main(String[] args) {
        //  Use it to offload blocking operations, control thread scheduling, or improve performance when working with CPU-bound tasks.
        Mono<List<Product>> listMono = Mono.just(RetailService.getProducts());
        listMono.subscribeOn(Schedulers.immediate())
                .map(products -> products)
                .subscribe(System.out::println);

    }

}
