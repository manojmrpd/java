package com.spring.reactive.web.concepts.transformingreactivestreams.defaultIfEmpty;

import com.spring.reactive.web.dto.retail.Product;
import com.spring.reactive.web.service.RetailService;
import reactor.core.publisher.Flux;

public class FluxDefaultIfEmpty1 {
    public static void main(String[] args) {
        Flux.fromIterable(RetailService.getOrders())
                .flatMapIterable(order -> order.getProducts())
                .filter(product -> product.getPrice() > 1000)
                .defaultIfEmpty(Product.builder().productName("Default product")
                        .productId(1012).price(1020.00).category("electronics")
                        .quantity(10).inStock(true).build())
                .subscribe(System.out::println);
    }
}
