package com.spring.reactive.web.flux.concatMapIterable;

import com.spring.reactive.web.service.RetailService;
import reactor.core.publisher.Flux;

public class FluxConcatMapIterable1 {
    public static void main(String[] args) {
        Flux.fromIterable(RetailService.getOrders())
                .concatMapIterable(order -> order.getProducts()).log()
                .map(product -> product.getProductName()).log()
                .subscribe(System.out::println);
    }
}
