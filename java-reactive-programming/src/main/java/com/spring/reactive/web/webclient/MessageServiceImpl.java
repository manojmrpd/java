package com.spring.reactive.web.webclient;

import com.azure.core.util.BinaryData;
import com.azure.messaging.servicebus.ServiceBusReceivedMessage;
import com.azure.messaging.servicebus.ServiceBusReceivedMessageContext;
import com.manulife.ap.service.IngeniumService;
import com.manulife.ap.service.MessageService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Objects;
import static com.manulife.ap.constants.ApplicationConstants.JP_JCUS_ING_ROUTER_SERVICE;
@Service
@Slf4j
public class MessageServiceImpl implements MessageService {
    @Autowired
    IngeniumService ingeniumService;
    @Override
    public void processMessage(ServiceBusReceivedMessageContext context) {
        ServiceBusReceivedMessage message = Objects.nonNull(context) ? context.getMessage() : null;
        if (Objects.isNull(message)) {
            log.error(JP_JCUS_ING_ROUTER_SERVICE + "Message is null");
            context.deadLetter();
        }
        String messageId = message.getMessageId();
        long sequenceNumber = message.getSequenceNumber();
        log.info(JP_JCUS_ING_ROUTER_SERVICE + "Processing message Id: {}, Sequence #: {} ", messageId, sequenceNumber);
        BinaryData payload = message.getBody();
        String data = payload.toString();
        if (StringUtils.isBlank(data) || "null".equals(data)) {
            log.error(JP_JCUS_ING_ROUTER_SERVICE + "Message payload is null or blank for message id {} ", messageId);
            context.deadLetter();
        } else {
            ingeniumService.handleCwsRegisteredUser(context, messageId, sequenceNumber, data);
        }
    }
}
