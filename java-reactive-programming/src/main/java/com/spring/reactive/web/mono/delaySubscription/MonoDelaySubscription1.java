package com.spring.reactive.web.mono.delaySubscription;

import com.spring.reactive.web.service.RetailService;
import reactor.core.publisher.Mono;

import java.time.Duration;

public class MonoDelaySubscription1 {
    // Use delaySubscription() when you want to delay the subscription to a Mono for a specific duration
    public static void main(String[] args) {
        Mono.fromCallable(() -> RetailService.getOrders()).log()
                .delaySubscription(Duration.ofMillis(1)).log()
                .subscribe(System.out::println);
    }
}
