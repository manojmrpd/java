package com.spring.reactive.web.concepts.transformingreactivestreams.flatMapMany;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public class MonoFlatMapMany1 {

    public static void main(String[] args) {
        Mono.just("Alex").map(String::toUpperCase)
                .flatMapMany(MonoFlatMapMany1::splitText_array)
                .subscribe(System.out::println);
    }

    private static Flux<?> splitText_list(String text) {
        List<String> charlist = List.of(text.split(""));
        return Flux.just(charlist);
    }

    private static Flux<?> splitText_array(String text) {
        String[] charArray = text.split("");
        return Flux.fromArray(charArray);
    }
}
