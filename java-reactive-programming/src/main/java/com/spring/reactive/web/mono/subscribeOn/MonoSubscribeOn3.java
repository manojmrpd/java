package com.spring.reactive.web.mono.subscribeOn;

import com.spring.reactive.web.service.RetailService;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

public class MonoSubscribeOn3 {
    public static void main(String[] args) {
        // Use subscribeOn to specify the Scheduler on which a Mono subscribes to its source
        Mono.fromCallable(() -> RetailService.getProducts()).subscribeOn(Schedulers.parallel())
                .subscribe(products -> System.out.println(products),
                        throwable -> System.out.println(throwable.getMessage()),
                        () -> System.out.println("Completed"));
    }
}
