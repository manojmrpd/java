package com.spring.reactive.web.flux.concatWith;

import reactor.core.publisher.Flux;

public class FluxConcatWith2 {
    public static void main(String[] args) {
        Flux.just("Alex", "Slice").concatWith(Flux.just("Robin", "Paul"))
                .concatWith(Flux.just("Stalin", "Rose"))
                .map(String::toUpperCase)
                .subscribe(System.out::println);
    }
}
