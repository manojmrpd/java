package com.spring.reactive.web.mono.share;

import com.spring.reactive.web.service.RetailService;
import reactor.core.publisher.Mono;

public class MonoShare2 {
    public static void main(String[] args) {
        Mono.fromCallable(() -> RetailService.getProducts())
                .share().log()
                .subscribe(System.out::println);
    }
}
