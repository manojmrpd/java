package com.spring.reactive.web.concepts.exceptionhandlingreactivestreams.retryWhen;

import com.spring.reactive.web.exceptions.StockPriceException;
import reactor.core.publisher.Flux;
import reactor.util.retry.Retry;

import java.time.Duration;

public class FluxRetryWhen {

    public static Flux<Integer> getStockPrice() {
        return Flux.range(1, 10)
                .map(price -> {
                    if (price.equals(5)) {
                        throw new IllegalStateException("Invalid stock price");
                    }
                    return price;
                })
                .mergeWith(Flux.range(11, 10))
                .onErrorMap(ex -> new StockPriceException(ex.getMessage()))
                .retryWhen(Retry.backoff(3, Duration.ofMillis(500)))
                .log();
    }
}
