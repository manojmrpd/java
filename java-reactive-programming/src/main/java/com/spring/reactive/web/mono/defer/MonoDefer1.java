package com.spring.reactive.web.mono.defer;

import reactor.core.publisher.Mono;

public class MonoDefer1 {
    public static void main(String[] args) {
        //Use Mono.defer() to delay the creation of a Mono until subscription
        Mono.defer(() -> {
            return Mono.just("Hello");
        }).log().subscribe(System.out::println);
    }
}
