package com.spring.reactive.web.concepts.combiningreactivestreams.zip;

import reactor.core.publisher.Flux;

public class FluxZip1 {
    public static void main(String[] args) {

        // Combines these two streams using the zip method, which returns a new Flux stream that emits tuples containing corresponding elements from the two original streams.
        Flux.zip(Flux.just(1, 2, 3), Flux.just(4, 5, 6, 7, 8))
                .subscribe(System.out::println);
    }
}
