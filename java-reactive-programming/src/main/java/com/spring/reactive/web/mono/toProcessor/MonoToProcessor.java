package com.spring.reactive.web.mono.toProcessor;

import reactor.core.publisher.Mono;
import reactor.core.publisher.MonoProcessor;

public class MonoToProcessor {
    public static void main(String[] args) {
        // Not recommended and deprecated
        MonoProcessor<String> processor = Mono.fromCallable(() -> "Manoj")
                .toProcessor();
        String userName = processor.block();
        System.out.println(userName);
    }
}
