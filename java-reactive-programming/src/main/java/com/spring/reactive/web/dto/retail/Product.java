package com.spring.reactive.web.dto.retail;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
@ToString
public class Product {

    private Integer productId;
    private String productName;
    private Double price;
    private String category;
    private Integer quantity;
    private Boolean inStock;
}
