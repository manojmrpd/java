package com.spring.reactive.web.mono.share;

import reactor.core.publisher.Mono;

public class MonoShare1 {
    public static void main(String[] args) {
        //Mono.share(), you can cache the retrieved value and replay it to subsequent subscribers, avoiding redundant execution for each subscriber.
        Mono<String> userName = Mono.fromCallable(() -> "manoj")
                .share().log();
        userName.subscribe(System.out::println);
    }
}
