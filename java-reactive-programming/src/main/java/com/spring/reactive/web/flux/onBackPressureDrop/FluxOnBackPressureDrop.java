package com.spring.reactive.web.flux.onBackPressureDrop;

import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

public class FluxOnBackPressureDrop {

    public static void main(String[] args) {
        // Create a fast producer emitting integers from 1 to 1000
        Flux<Integer> fastProducer = Flux.range(1, 1000)
                .subscribeOn(Schedulers.parallel()).log();

        // Slow consumer that processes each element with a delay of 10 milliseconds
        Flux<Integer> slowConsumer = Flux.range(1, 10)
                .delayElements(java.time.Duration.ofMillis(10)).log();

        // Simulate backpressure by subscribing the slow consumer to the fast producer
        fastProducer.onBackpressureDrop() // Handle backpressure by dropping excess elements
                .subscribe(integer -> System.out.println("Element received at backpressure drop: " + integer));

        // Wait for a while to see the backpressure in action
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
