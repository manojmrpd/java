package com.spring.reactive.web.flux.onErrorContinue;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;

@Slf4j
public class FluxOnErrorContinue {
    public static void main(String[] args) {
        getStockPrice().subscribe(System.out::println);
    }

    private static Flux<Integer> getStockPrice() {
        return Flux.range(1, 10)
                .map(price -> {
                    if (price.equals(5)) {
                        throw new IllegalStateException("Invalid stock price");
                    }
                    return price;
                })
                .mergeWith(Flux.range(11, 10))
                .onErrorContinue((ex, price) -> {
                    log.error("Exception occurred", ex);
                }).log();
    }
}
