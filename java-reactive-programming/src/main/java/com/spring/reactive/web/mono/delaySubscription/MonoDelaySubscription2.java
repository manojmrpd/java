package com.spring.reactive.web.mono.delaySubscription;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

public class MonoDelaySubscription2 {
    public static void main(String[] args) {
        Flux<Long> timerFlux = Flux.interval(Duration.ofSeconds(1)); // Emits every second
        Mono<String> dataMono = Mono.just("Delayed data")
                .delaySubscription(timerFlux.take(1)); // Delay by the first emission from timerFlux
        dataMono.subscribe(data -> System.out.println("Received data after 1 second"));
    }
}
