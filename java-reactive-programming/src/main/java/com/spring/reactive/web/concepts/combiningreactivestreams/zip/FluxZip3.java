package com.spring.reactive.web.concepts.combiningreactivestreams.zip;

import com.spring.reactive.web.dto.retail.Product;
import com.spring.reactive.web.service.RetailService;
import reactor.core.publisher.Flux;

import java.util.stream.Collectors;

public class FluxZip3 {

    // It uses the Flux.zip method to combine two Flux streams of prices and quantities into a single Flux stream. The zip method takes a function as an argument, which multiplies the price and quantity together to produce a new stream of results.
    // However, the result of the zip operation is not being subscribed to or processed further.
    public static void main(String[] args) {
        Flux<Double> zip = Flux.zip(Flux.fromIterable(RetailService.getProducts().stream().map(Product::getPrice).collect(Collectors.toUnmodifiableList()))
                , Flux.fromIterable(RetailService.getProducts().stream().map(Product::getQuantity).collect(Collectors.toUnmodifiableList()))
                , (price, quantity) -> price * quantity);
        zip.subscribe(System.out::println);
    }
}
