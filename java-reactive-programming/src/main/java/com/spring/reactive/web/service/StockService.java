package com.spring.reactive.web.service;

import com.spring.reactive.web.dto.stocks.ExchangeRate;
import com.spring.reactive.web.dto.stocks.StockPrice;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;

public class StockService {


    public static List<StockPrice> getStockPrices() {
        return Arrays.asList(
                new StockPrice("TSLA", 100.0, "USD", Instant.now()),
                new StockPrice("GOOG", 100.0, "USD", Instant.now()),
                new StockPrice("AAPL", 100.0, "USD", Instant.now()));
    }

    public static List<ExchangeRate> getExchangeRates() {
        return Arrays.asList(
                new ExchangeRate("USD", "INR", 75.0, Instant.now()),
                new ExchangeRate("USD", "EUR", 55.0, Instant.now()),
                new ExchangeRate("USD", "AUD", 65.0, Instant.now()));
    }

}
