package com.spring.reactive.web.flux.flatMapIterable;

import com.spring.reactive.web.service.RetailService;
import reactor.core.publisher.Flux;

public class FluxFlatMapIterable1 {
    public static void main(String[] args) {
        Flux.fromIterable(RetailService.getOrders())
                .flatMapIterable(order -> order.getProducts())
                .filter(product -> product.getPrice() > 900)
                .switchIfEmpty(Flux.error(new RuntimeException("No products found")))
                .subscribe(System.out::println);
    }
}
