package com.spring.reactive.web.flux.zip;

import reactor.core.publisher.Flux;

public class FluxZip2 {
    public static void main(String[] args) {
        // The Flux.zip method is then used to combine the two Flux streams into a single Flux stream of tuples.
        // Each tuple contains an element from flux1 and an element from flux2.
        Flux<Integer> flux1 = Flux.just(1, 2, 3);
        Flux<Integer> flux2 = Flux.just(4, 5, 6, 7, 8);
        //The map method is then used to transform each tuple into the product of the two elements.
        Flux.zip(flux1, flux2).map(tuple -> tuple.getT1() * tuple.getT2())
                .subscribe(System.out::println);
    }
}
