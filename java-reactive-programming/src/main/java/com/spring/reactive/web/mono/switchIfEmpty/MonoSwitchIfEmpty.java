package com.spring.reactive.web.mono.switchIfEmpty;

import com.spring.reactive.web.dto.retail.Product;
import com.spring.reactive.web.service.RetailService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public class MonoSwitchIfEmpty {
    public static void main(String[] args) {
        List<Product> products = RetailService.getProducts();
        Flux.fromIterable(products).filter(product -> product.getCategory().equals("BABYCARE"))
                .switchIfEmpty(Mono.error(new RuntimeException("No products found")))
                .subscribe(System.out::println);
    }
}
