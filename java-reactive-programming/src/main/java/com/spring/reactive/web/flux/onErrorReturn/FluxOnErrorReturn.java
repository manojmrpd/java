package com.spring.reactive.web.flux.onErrorReturn;

import reactor.core.publisher.Flux;

public class FluxOnErrorReturn {

    public static void main(String[] args) {
        getStockPrice().subscribe(System.out::println);
    }

    private static Flux<Integer> getStockPrice() {
        return Flux.range(1, 10)
                .concatWith(Flux.error(new IllegalStateException("Invalid stock price")))
                .onErrorReturn(11).log();
    }
}
