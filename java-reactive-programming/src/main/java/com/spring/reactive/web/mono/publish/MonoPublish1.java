package com.spring.reactive.web.mono.publish;

import reactor.core.publisher.Mono;

public class MonoPublish1 {
    public static void main(String[] args) {
        // Use publish() when you need precise control over the connection and emission behavior
        // of a Mono
        Mono.fromCallable(() -> "manoj")
                .publish(Mono::single).log()
                .subscribe(System.out::println);
    }
}
