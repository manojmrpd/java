package com.spring.reactive.web.flux.onBackPressureBuffer;

import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

public class FluxOnBackPressureBuffer {
    public static void main(String[] args) {

        // Simulated stream emitting elements at a high rate
        Flux<Integer> fastProducer = Flux.range(1, 1000)
                .subscribeOn(Schedulers.parallel()).log();

        // Slow consumer that processes each element with a delay of 10 milliseconds
        Flux<Integer> slowConsumer = Flux.range(1, 10)
                .delayElements(java.time.Duration.ofMillis(10));

        // Applying onBackpressureBuffer() to handle backpressure by buffering
        fastProducer.onBackpressureBuffer(100) // Buffer size limit
                .subscribe(integer -> System.out.println("Element received at backpressure buffer: " + integer));

        // Subscribing the slow consumer
        slowConsumer.subscribe();

        // Wait for a while to see the backpressure handling in action
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
