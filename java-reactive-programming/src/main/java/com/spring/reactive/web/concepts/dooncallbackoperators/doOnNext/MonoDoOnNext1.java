package com.spring.reactive.web.concepts.dooncallbackoperators.doOnNext;

import com.spring.reactive.web.dto.retail.Product;
import com.spring.reactive.web.service.RetailService;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;

public class MonoDoOnNext1 {
    public static void main(String[] args) {
        // The code within doOnNext() is executed only once for the element emitted by the Mono
        Mono.just(RetailService.getProducts())
                .doOnNext(products -> getInStockProducts(products).subscribe(System.out::println))
                .subscribe(System.out::println);
    }

    private static Mono<List<Product>> getInStockProducts(List<Product> products) {
        List<Product> productList = products.stream().filter(product -> product.getInStock().booleanValue() == Boolean.TRUE).collect(Collectors.toList());
        System.out.println(productList);
        return Mono.just(productList);
    }
}
