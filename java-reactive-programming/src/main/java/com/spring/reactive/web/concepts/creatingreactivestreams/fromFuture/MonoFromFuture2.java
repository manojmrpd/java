package com.spring.reactive.web.concepts.creatingreactivestreams.fromFuture;

import com.spring.reactive.web.service.RetailService;
import reactor.core.publisher.Mono;

import java.util.concurrent.CompletableFuture;

public class MonoFromFuture2 {
    public static void main(String[] args) {
        Mono.fromFuture(CompletableFuture.supplyAsync(() -> RetailService.getProducts()))
                .subscribe(products -> System.out.println(products),
                        throwable -> System.out.println(throwable.getMessage()),
                        () -> System.out.println("Completed"));
    }
}
