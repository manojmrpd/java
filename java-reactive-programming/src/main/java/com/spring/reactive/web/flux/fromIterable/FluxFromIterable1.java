package com.spring.reactive.web.flux.fromIterable;

import com.spring.reactive.web.flux.tranform.FluxTransform1;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.function.Function;

public class FluxFromIterable1 {
    public static void main(String[] args) {
        Flux.fromIterable(List.of("Alex", "Slice", "Lucid"))
                .flatMap(FluxFromIterable1::splitText)
                .subscribe(System.out::println);
    }

    private static Flux<?> splitText(String s) {
        List<String> charlist = List.of(s.split(""));
        return Flux.fromIterable(charlist);
    }
}
