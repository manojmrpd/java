package com.spring.reactive.web.concepts.transformingreactivestreams.defaultIfEmpty;

import reactor.core.publisher.Mono;

public class MonoOperators {

    // Function to create Mono with a default value using defaultIfEmpty
    public static Mono<String> namesMono_map_filter(int stringLength) {
        return Mono.just("alex")
                .map(name -> {
                    if (name.length() == stringLength) {
                        return name;
                    } else {
                        return "";
                    }
                })
                .filter(name -> !name.isEmpty());
    }

    // Function to create Mono with a default value using switchIfEmpty
    public static Mono<String> namesMono_map_filter_switchIfEmpty(int stringLength) {
        Mono<String> originalMono = Mono.just("alex")
                .map(name -> {
                    if (name.length() == stringLength) {
                        return name;
                    } else {
                        return "";
                    }
                })
                .filter(name -> !name.isEmpty());

        return originalMono.switchIfEmpty(Mono.just("Default Value"));
    }

    public static void main(String[] args) {
        // Test defaultIfEmpty
        namesMono_map_filter(4)
                .defaultIfEmpty("Default Value")
                .subscribe(value -> System.out.println("Value from defaultIfEmpty: " + value));

        // Test switchIfEmpty
        namesMono_map_filter_switchIfEmpty(4)
                .subscribe(value -> System.out.println("Value from switchIfEmpty: " + value));
    }
}
