package com.spring.reactive.web.dto.retail;

import lombok.*;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
@ToString
public class Order {
    private Integer id;
    private String description;

    private Double total;
    private LocalDate orderDate;
    private List<Product> products;
}
