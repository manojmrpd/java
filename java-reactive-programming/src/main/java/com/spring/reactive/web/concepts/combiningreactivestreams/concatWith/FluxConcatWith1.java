package com.spring.reactive.web.concepts.combiningreactivestreams.concatWith;

import reactor.core.publisher.Flux;

public class FluxConcatWith1 {
    public static void main(String[] args) {

        //Combine Flux reactive stream with Mono or Flux using ConcatWith
        Flux.just(1, 2, 3).concatWith(Flux.just(4, 5, 6))
                .filter(number -> number % 2 == 0)
                .subscribe(System.out::println);
    }
}
