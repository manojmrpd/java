package com.spring.reactive.web.service;

import static com.costco.mobile.bff.homepage.commons.constants.HomeBFFConstants.CACHE_PRODUCT_NODE;
import static com.costco.mobile.bff.homepage.commons.constants.HomeBFFConstants.CHILD_CATALOG_DATA;
import static com.costco.mobile.bff.homepage.commons.constants.HomeBFFConstants.ERROR_MSG_NODE;
import static com.costco.mobile.bff.homepage.commons.constants.HomeBFFConstants.FILTERED_CHILD_ITEM;
import static com.costco.mobile.bff.homepage.commons.constants.HomeBFFConstants.PUBLISHED;
import static com.costco.mobile.bff.homepage.commons.constants.HomeBFFConstants.STATUS_NODE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.costco.mobile.bff.homepage.commons.constants.HomeBFFConstants;
import com.costco.mobile.bff.homepage.commons.exception.AdbutlerServiceException;
import com.costco.mobile.bff.homepage.commons.exception.InventoryServiceException;
import com.costco.mobile.bff.homepage.commons.exception.ObjectMappingException;
import com.costco.mobile.bff.homepage.commons.exception.ProductServiceException;
import com.costco.mobile.bff.homepage.commons.mapper.CriteoDataMapper;
import com.costco.mobile.bff.homepage.commons.util.HomepageServiceUtil;
import com.costco.mobile.bff.homepage.domain.entity.HomePageEntity;
import com.costco.mobile.bff.homepage.domain.entity.HomePageRequest;
import com.costco.mobile.bff.homepage.domain.entity.criteo.Carousel;
import com.costco.mobile.bff.homepage.domain.entity.criteo.CarouselData;
import com.costco.mobile.bff.homepage.domain.entity.criteo.CriteoCarouselData;
import com.costco.mobile.bff.homepage.domain.entity.criteo.CriteoCarouselResponse;
import com.costco.mobile.bff.homepage.domain.entity.product.Item;
import com.costco.mobile.bff.homepage.domain.entity.product.ProductCarouselData;
import com.costco.mobile.bff.homepage.domain.service.adapter.AdbutlerAdapterService;
import com.costco.mobile.bff.homepage.domain.service.adapter.CacheHelperService;
import com.costco.mobile.bff.homepage.domain.service.adapter.CriteoAdapterService;
import com.costco.mobile.bff.homepage.domain.service.adapter.DistributionCentersAdapterService;
import com.costco.mobile.bff.homepage.domain.service.adapter.InventoryAPIAdapterService;
import com.costco.mobile.bff.homepage.domain.service.adapter.RestProductAPIAdapterService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;


import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;
import reactor.util.function.Tuple2;


@Slf4j
@Service
public class HomePageService {


    private RestProductAPIAdapterService restProductAPIAdapterService;
    private CriteoAdapterService criteoAdapterService;
    private AdbutlerAdapterService adbutlerAdapterService;
    private InventoryAPIAdapterService inventoryAPIAdapterService;
    private DistributionCentersAdapterService distributionCentersAdapterService;
    private HomepageServiceUtil homepageServiceUtil;
    private CriteoDataMapper criteoDataMapper;
    private CacheHelperService cacheHelper;
    @Value("${spring.data.redis.redisEnabled}")
    private boolean redisEnabled;
    private final Scheduler scheduler = Schedulers.newParallel("HomePageService", 10);
    @Autowired
    private ObjectMapper jacksonObjectMapper;


    public HomePageService(RestProductAPIAdapterService restProductAPIAdapterService, CriteoAdapterService criteoAdapterService,
                           AdbutlerAdapterService adbutlerAdapterService, InventoryAPIAdapterService inventoryAdapterService,
                           DistributionCentersAdapterService distributionCentersAdapterService, HomepageServiceUtil homepageServiceUtil,
                           CriteoDataMapper criteoDataMapper,ObjectMapper jacksonObjectMapper, CacheHelperService cacheHelper) {
        this.restProductAPIAdapterService = restProductAPIAdapterService;
        this.criteoAdapterService = criteoAdapterService;
        this.adbutlerAdapterService = adbutlerAdapterService;
        this.inventoryAPIAdapterService = inventoryAdapterService;
        this.distributionCentersAdapterService = distributionCentersAdapterService;
        this.homepageServiceUtil = homepageServiceUtil;
        this.criteoDataMapper = criteoDataMapper;
        this.jacksonObjectMapper = jacksonObjectMapper;
        this.cacheHelper = cacheHelper;
    }
    public Mono<HomePageEntity> getData(HomePageRequest homePageRequest) {


        Mono<JsonNode> distributionCentersMono = distributionCentersAdapterService
                .getDistributioncentersData(homePageRequest.getZipcode(),homePageRequest.getLocale()).onErrorResume(error -> {
                    log.error(
                            "GetCriteoAndDistributionCenterData: Could not retrieve data from Distribution Center Service");
                    return homepageServiceUtil.createEmptyDCMono();
                }).cache().publishOn(scheduler);


        return Mono.zip(
                        (fetchAdbutlerData(homePageRequest).onErrorResume(Exception.class, this::handleAdButlerServiceError))
                                .switchIfEmpty(Mono.fromCallable(JsonNodeFactory.instance::nullNode)),
                        fetchCriteoData(homePageRequest, distributionCentersMono)
                                .onErrorResume(Exception.class, this::handleCriteoServiceError)
                                .switchIfEmpty(Mono.fromCallable(CriteoCarouselData::new)),
                        fetchProductCarouselData(homePageRequest, distributionCentersMono).flatMap(productList -> {


                            List<ProductCarouselData> productCarouselDataList = productList.stream().map(jsonNode -> {
                                try {
                                    return applyProductCarouselBusinessRules(jacksonObjectMapper.treeToValue(jsonNode, ProductCarouselData.class), homePageRequest);


                                } catch (Exception e) {
                                    log.error("ProductMappingError: Could not map product list response due to an error,skipping the item", e);
                                    return null;
                                }
                            }).filter(Objects::nonNull).toList();


                            return Mono.just(productCarouselDataList);
                        }).switchIfEmpty(Mono.fromCallable(() -> new ArrayList<>())))
                .map(tuple -> homepageServiceUtil.buildHomePageResponse(tuple)).publishOn(scheduler);
    }


    private Mono<JsonNode> fetchAdbutlerData(HomePageRequest homePageRequest) {


        if(homePageRequest.getAdbutlerRequest() == null || homePageRequest.getAdbutlerRequest().isEmpty()){
            return Mono.empty();
        }
        return adbutlerAdapterService.getAdbutlerData(homePageRequest.getAdbutlerRequest());
    }


    private Mono<CriteoCarouselData> fetchCriteoData(HomePageRequest homePageRequest, Mono<JsonNode> distributionCentersMono) {


        if(homePageRequest.getCriteoRequest() == null){
            return Mono.empty();
        }


        if(homepageServiceUtil.hasAnyInventoryAwareCriteoRequest(homePageRequest)) {
            return getCriteoAndDistributionCenterData(homePageRequest, distributionCentersMono)
                    .zipWhen(criteoData -> fetchProductAndInventoryData(homePageRequest, criteoData, distributionCentersMono),
                            (criteoData, carouselList) -> criteoData);
        } else{
            return fetchAllCriteoPlacements(homePageRequest)
                    .zipWhen(criteoData -> fetchProductAndInventoryData(homePageRequest, criteoData, distributionCentersMono),
                            (criteoData, products) -> criteoData);
        }
    }

    private Mono<List<JsonNode>> fetchProductCarouselData(HomePageRequest homePageRequest, Mono<JsonNode> dcMono) {

        if(homePageRequest.getProductCarouselRequest() == null){
            return Mono.empty();
        }
        if (homepageServiceUtil.hasAnyInventoryAware(homePageRequest)) {
            return invokeProductInventoryForProductCarousel(homePageRequest, dcMono);
        } else {
            return invokeProductInventoryForProductCarousel(homePageRequest, null);
        }
    }

    private Mono<List<List<JsonNode>>> fetchProductAndInventoryData(HomePageRequest homePageRequest, CriteoCarouselData criteoData, Mono<JsonNode> distributionCentersMono) {

        return Flux.fromIterable(criteoData.getCarouselDataList().stream()
                        .filter(data -> !CollectionUtils.isEmpty(data.getPlacements())).toList())
                .flatMap( criteoCarouselData -> {
                    return Flux.fromIterable(criteoCarouselData.getPlacements())
                            .parallel()
                            .flatMap(placement -> {
                                if(placement.getItemIds()==null || placement.getItemIds().isEmpty()) {
                                    log.error(
                                            "fetchProductAndInventoryData: Received empty product list from criteo for "+ placement.getPlacementId()+" skipping product and inventory call");
                                    return Mono.empty();
                                }


                                if(criteoCarouselData.isInventoryAwareListings()){
                                    return populateCriteoProductAndInventoryDetails(homePageRequest, distributionCentersMono, placement);
                                }else{
                                    return populateCriteoProductDetails(homePageRequest, placement);
                                }
                            })
                            .sequential()
                            .collectList();
                }).collectList();
    }


    private Mono<JsonNode> populateCriteoProductAndInventoryDetails(HomePageRequest homePageRequest, Mono<JsonNode> distributionCentersMono, Carousel placement) {
        return getProductDataWithCache( placement.getItemIds(), homePageRequest.getLocale())
                .zipWhen(productData -> fetchInventoryInBatches(homePageRequest, productData, distributionCentersMono),
                        (productData, inventoryData) -> {
                            Map<String, JsonNode> productMap = homepageServiceUtil.populateProductChildDataWithInventory(productData, inventoryData);
                            JsonNode jsonNode = homepageServiceUtil.formatCriteoCarouselResponse(productMap);
                            try {
                                CriteoCarouselResponse response = jacksonObjectMapper.treeToValue(jsonNode, CriteoCarouselResponse.class);
                                populateCriteoProductChildInventoryData(placement, response, homePageRequest);
                            } catch (JsonProcessingException e) {
                                throw new ObjectMappingException("Error while mapping CriteoCarouselResponse", e);
                            }
                            return productData;
                        })
                .onErrorResume(error -> {
                    log.error("HomePageService Error populateCriteoProductAndInventoryDetails", error);
                    return homepageServiceUtil.setResponseForProductServiceError(placement);
                });
    }


    private Mono<JsonNode> populateCriteoProductDetails(HomePageRequest homePageRequest, Carousel placement) {
        return getProductDataWithCache( placement.getItemIds(), homePageRequest.getLocale())
                .map(productData -> {
                    Map<String, JsonNode> productMap = homepageServiceUtil.populateProductChildData(productData);
                    JsonNode criteoJsonNode = homepageServiceUtil.formatCriteoCarouselResponse(productMap);
                    try {
                        CriteoCarouselResponse response = jacksonObjectMapper.treeToValue(criteoJsonNode, CriteoCarouselResponse.class);
                        populateCriteoProductChildInventoryData(placement, response, homePageRequest);
                    } catch (JsonProcessingException e) {
                        throw new ObjectMappingException("Error while mapping CriteoCarouselResponse", e);
                    }
                    return productData;
                })
                .onErrorResume(error -> {
                    log.error("HomePageService Error populateCriteoProductDetails", error);
                    return homepageServiceUtil.setResponseForProductServiceError(placement);
                });
    }


    private Mono<List<JsonNode>> invokeProductInventoryForProductCarousel(HomePageRequest homePageRequest, Mono<JsonNode> distributionCentersMono) {


        return Flux.fromIterable(homePageRequest.getProductCarouselRequest())
                .parallel()
                .flatMap(singleCarousel -> {
                    if(singleCarousel.isInventoryAwareListings()){
                        return getProductDataWithCache(singleCarousel.getProductIds(), homePageRequest.getLocale())
                                .zipWhen(productData -> fetchInventoryInBatches(homePageRequest, productData, distributionCentersMono),
                                        (productData, inventoryData) -> {
                                            Map<String, JsonNode> productMap = homepageServiceUtil.populateProductChildDataWithInventory(productData, inventoryData);
                                            return  homepageServiceUtil.formatCarouselResponse(singleCarousel, productMap);
                                        } )
                                .onErrorResume(error -> {
                                    log.error("HomePageService Error invokeProductInventoryForProductCarousel", error);
                                    return homepageServiceUtil.setResponseForProductServiceError(singleCarousel);
                                });
                    }else{
                        return getProductDataWithCache(singleCarousel.getProductIds(), homePageRequest.getLocale())
                                .map( productData -> {
                                    Map<String, JsonNode> productMap = homepageServiceUtil.populateProductChildData(productData);
                                    return  homepageServiceUtil.formatCarouselResponse(singleCarousel, productMap);
                                }).onErrorResume(error -> {
                                    log.error("HomePageService Error invokeProductInventoryForProductCarousel No Inventory", error);
                                    return homepageServiceUtil.setResponseForProductServiceError(singleCarousel);
                                });
                    }
                })
                .sequential()
                .collectList();
    }


    public Mono<JsonNode> getProductDataWithCache(List<String> itemNumbers, String locale) {
        if(redisEnabled){
            log.debug("Cache: Enabled");
            List<String> cachedProducts = new ArrayList<>();
            return (cacheHelper.getProductDataWithCache(itemNumbers.stream().distinct().collect(Collectors.toList()), cachedProducts, CACHE_PRODUCT_NODE)
                    .onErrorResume(error -> {
                        log.error("Cache: Error occurred while retrieving product data from Redis", error);
                        return Mono.just(new ArrayList<>());
                    }))
                    .zipWhen(cachedData -> {
                        List<String> distinctItemNumbers = cacheHelper.filterCachedProductIds(itemNumbers, cachedProducts);
                        if(distinctItemNumbers.size() > 0){
                            return restProductAPIAdapterService.getRestProductData(distinctItemNumbers, locale)
                                    .map( productData -> {
                                        return checkAndAddToCache(productData);})
                                    .onErrorResume(error -> {
                                        return handleCacheError(cachedProducts);


                                    });
                        }else{
                            log.info("Cache: All requested product found in cache");
                            return cacheHelper.createEmptyProductDataResponse();
                        }
                    }, (cachedData, productResponse) -> {
                        if(cachedData.size() == 0){
                            return productResponse;
                        }
                        return cacheHelper.mergeProductData(cachedData, productResponse);
                    });
        }else{
            return restProductAPIAdapterService.getRestProductData(itemNumbers, locale);
        }
    }


    private Mono<? extends JsonNode> handleCacheError(List<String> cachedProducts) {
        if(cachedProducts.size() == 0) {
            throw new ProductServiceException(HttpStatus.NO_CONTENT, "AdapterException-Rest-Product-Service : No content found in cache or from product rest service");
        }else {
            return cacheHelper.createEmptyProductDataResponse();
        }
    }


    private JsonNode checkAndAddToCache(JsonNode productData) {
        if(productData.get("catalogData") != null && !productData.get("catalogData").isEmpty()){
            cacheHelper.addProductDataToCache(productData);
        }
        return productData;
    }


    private void populateCriteoProductChildInventoryData(Carousel placement, CriteoCarouselResponse criteoCarouselResponse, HomePageRequest homePageRequest) {

        Map<String, Item> productMap = criteoCarouselResponse.getItems().stream().collect(Collectors.toMap(
                Item::getItemNumber,
                product -> homepageServiceUtil.applyProductDerivedFields(product, homePageRequest)));

        placement.getItems().parallelStream()
                .forEach(item -> criteoDataMapper.populateCriteoPlacementData(item, productMap.get(item.getParentSKU())));

        placement.getItems().removeIf(item -> {
            if(item.getProductDetails() == null){
                log.info("Removed following item from the original Criteo placement list - {} ", item.getParentSKU());
                return true;
            }
            return false;
        });
    }

    private Mono<List<JsonNode>> fetchInventoryInBatches(HomePageRequest homePageRequest, JsonNode productData,Mono<JsonNode> distributionCentersMono) {

        homepageServiceUtil.removeUnpublishedProducts(productData);
        List<String> childList = new ArrayList<>();
        Stream<JsonNode> childItemStream = StreamSupport.stream(productData.get(CHILD_CATALOG_DATA).spliterator(), true);
        ArrayNode filteredChildArray = JsonNodeFactory.instance.arrayNode();
        filteredChildArray.addAll(childItemStream
                .filter(data -> data.get("buyable").asText().equals("1") && data.get(PUBLISHED).asText().equals("1"))
                .map( childNode -> {
                    childList.add(childNode.get("itemNumber").asText());
                    return childNode;
                }).toList());

        ((ObjectNode)productData).set(FILTERED_CHILD_ITEM, filteredChildArray);
        return fetchInventoryData(createBatches(childList), distributionCentersMono, homePageRequest.getWarehouseNumber(),homePageRequest.getLocale(), homePageRequest.getZipcode());
    }

    private List<List<String>> createBatches(List<String> childList) {
        homepageServiceUtil.getSortedList(childList);
        int batchSize = 10;
        return IntStream.range(0, (childList.size() + batchSize - 1) / batchSize)
                .mapToObj(i -> childList.subList(i * batchSize, Math.min((i + 1) * batchSize, childList.size())))
                .toList();
    }


    private Mono<List<JsonNode>> fetchInventoryData(List<List<String>> childSkuBatchList, Mono<JsonNode> distributionCentersMono, String warehouseNumber, String locale, String zipcode) {
        return Flux.fromIterable(childSkuBatchList).parallel()
                .flatMap(placementChildSkuList -> {
                    return distributionCentersMono.flatMap( dc ->
                                    inventoryAPIAdapterService.getInventoryData(dc.get("distributionCenters"), warehouseNumber, placementChildSkuList, locale, zipcode)).
                            onErrorResume(Exception.class, this::handleInventoryServiceError);

                }).sequential()
                .collectList();
    }


    private Mono<CriteoCarouselData> getCriteoAndDistributionCenterData(HomePageRequest homePageRequest, Mono<JsonNode> dcMono) {
        return Mono.zip(fetchAllCriteoPlacements(homePageRequest), dcMono)
                .map(this::buildCriteoAndDistributionCenterData);
    }


    private Mono<CriteoCarouselData> fetchAllCriteoPlacements(HomePageRequest homePageRequest) {
        return Flux.fromIterable(homePageRequest.getCriteoRequest().getPlacements()).parallel()
                .flatMap(placementRequest -> criteoAdapterService.getCriteoData(homePageRequest.getCriteoRequest(), placementRequest)
                        .map(criteoResponse ->
                                criteoDataMapper.populateCarouselData(criteoResponse, placementRequest))
                        .onErrorResume(error -> handleCriteoError(placementRequest))).sequential()
                .collectList()
                .map(list -> {
                    CriteoCarouselData criteodata = new CriteoCarouselData();
                    criteodata.setCarouselDataList(list);
                    return criteodata;
                });

    }

    private ProductCarouselData applyProductCarouselBusinessRules(ProductCarouselData singlePrdctCarousel, HomePageRequest homePageRequest) {
        if (singlePrdctCarousel != null && singlePrdctCarousel.getItems() != null) {
            singlePrdctCarousel.getItems().forEach(item ->
                    homepageServiceUtil.applyProductDerivedFields(item, homePageRequest)
            );
        }
        return singlePrdctCarousel;
    }

    private Mono<CarouselData> handleCriteoError(HashMap<String, String> placement) {
        CarouselData carouselData = getFallbackCriteoData("ERROR: Could not retrieve data from Criteo Service");
        carouselData.setPageId(placement.get("page-id"));
        carouselData.setEventType(placement.get("event-type"));
        return Mono.just(carouselData);
    }

    private CriteoCarouselData buildCriteoAndDistributionCenterData(Tuple2<CriteoCarouselData, JsonNode> tuple) {
        tuple.getT1().setDistributionCenters(tuple.getT2().get(HomeBFFConstants.DISTRIBUTION_CENTERS_NODE));
        return tuple.getT1();
    }

    private Mono<CriteoCarouselData> handleCriteoServiceError(Exception exception) {
        log.error("An Error occurred in invoking Criteo Service" , exception);
        return Mono.just(new CriteoCarouselData());
    }
    private Mono<JsonNode> handleAdButlerServiceError(Exception exception) {
        log.error("An Error occurred in invoking Adbulter Service" , exception);
        if (exception instanceof AdbutlerServiceException exp) {
            return Mono.just(getFallbackAdbutlerData(exp.getReason()));
        } else {
            return Mono.just(getFallbackAdbutlerData("ERROR: Could not retrieve data from Adbulter Service"));
        }
    }

    private CarouselData getFallbackCriteoData(String message) {
        CarouselData fallbackObj = new CarouselData();
        fallbackObj.setErrorMessage(message);
        fallbackObj.setStatus(HomeBFFConstants.ERROR);
        return fallbackObj;
    }

    private JsonNode getFallbackAdbutlerData(String message) {
        return JsonNodeFactory.instance.objectNode().
                put(STATUS_NODE, HomeBFFConstants.ERROR).put(ERROR_MSG_NODE, message);
    }

    private Mono<ObjectNode> handleInventoryServiceError(Exception ex) {
        log.error("An Error occurred in invoking Inventory Service" , ex);
        if (ex instanceof InventoryServiceException exp) {
            return Mono.just(getFallbackInventoryData(exp.getReason()));


        } else {
            return Mono.just(getFallbackInventoryData("ERROR: Could not retrieve data from Inventory Service"));
        }
    }

    private ObjectNode getFallbackInventoryData(String message) {
        return JsonNodeFactory.instance.objectNode().
                put(HomeBFFConstants.INVENTORY_STATUS_TEXT, HomeBFFConstants.ERROR).put(ERROR_MSG_NODE, message);
    }

}
