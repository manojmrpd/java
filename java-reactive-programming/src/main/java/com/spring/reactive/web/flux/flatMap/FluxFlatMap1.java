package com.spring.reactive.web.flux.flatMap;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class FluxFlatMap1 {
    public static void main(String[] args) {
        Flux.just(1,2,3).flatMap(number -> Mono.just(number * 2))
                .log()
                .subscribe(System.out::println);
    }
}
