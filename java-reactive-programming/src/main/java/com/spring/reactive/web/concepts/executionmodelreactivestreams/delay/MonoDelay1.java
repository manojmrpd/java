package com.spring.reactive.web.concepts.executionmodelreactivestreams.delay;

import reactor.core.publisher.Mono;

import java.time.Duration;

public class MonoDelay1 {
    public static void main(String[] args) {
        Mono.delay(Duration.ofMillis(1))
                .doOnSuccess(time -> System.out.println(time)).log()
                .subscribe();
    }
}
