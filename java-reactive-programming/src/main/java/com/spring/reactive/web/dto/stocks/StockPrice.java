package com.spring.reactive.web.dto.stocks;

import lombok.*;

import java.time.Instant;

@Getter
@Setter
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
@ToString
public class StockPrice {

    private String symbol;
    private double price;
    private String currency;
    private Instant timestamp;

}
