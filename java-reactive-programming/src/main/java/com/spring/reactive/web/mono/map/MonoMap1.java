package com.spring.reactive.web.mono.map;

import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.List;

public class MonoMap1 {
    public static void main(String[] args) {
        Mono.just("text").map(String::toUpperCase)
                .flatMap(MonoMap1::splitText)
                .subscribe(System.out::println);
    }

    private static Mono<?> splitText(String s) {
        List<String> charlist = List.of(s.split(""));
        return Mono.just(charlist);
    }
}
