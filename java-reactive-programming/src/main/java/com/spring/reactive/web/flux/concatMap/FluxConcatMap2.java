package com.spring.reactive.web.flux.concatMap;

import com.spring.reactive.web.service.RetailService;
import reactor.core.publisher.Flux;

public class FluxConcatMap2 {
    public static void main(String[] args) {
        Flux.fromIterable(RetailService.getOrders())
                .concatMap(order -> Flux.fromIterable(order.getProducts())).log()
                .flatMap(product -> RetailService.saveProducts(product))
                .subscribe(System.out::println);
    }
}
