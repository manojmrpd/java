package com.spring.reactive.web.concepts.creatingreactivestreams.fromCallable;

import com.spring.reactive.web.service.RetailService;
import reactor.core.publisher.Mono;

public class MonoFromCallable2 {
    public static void main(String[] args) {
        Mono.fromCallable(() -> RetailService.getProducts()).log()
                .filter(products -> products.stream().anyMatch(product -> product.getCategory().equalsIgnoreCase("Electronics"))).log()
                .subscribe(products -> System.out.println(products),
                        throwable -> System.out.println(throwable.getMessage()),
                        () -> System.out.println("Completed"));
    }
}
