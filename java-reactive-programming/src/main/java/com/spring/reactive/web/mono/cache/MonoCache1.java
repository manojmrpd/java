package com.spring.reactive.web.mono.cache;

import reactor.core.publisher.Mono;

public class MonoCache1 {
    //Use Mono.cache(Duration ttl) for caching with controlled expiration to balance performance and data freshness.
    public static void main(String[] args) {
        Mono.fromCallable(() -> "manoj")
                .cache().log()
                .subscribe(System.out::println);
    }
}
