package com.spring.reactive.web.mono.concatWith;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class MonoConcatWith1 {

    public static void main(String[] args) {

        //Combine Mono reactive stream with Mono or Flux using ConcatWith
        Mono.just(1).concatWith(Flux.just(2, 3, 4, 5))
                .filter(number -> number % 2 == 0)
                .subscribe(System.out::println);
    }
}
