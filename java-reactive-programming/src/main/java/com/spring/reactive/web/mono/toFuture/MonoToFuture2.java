package com.spring.reactive.web.mono.toFuture;

import com.spring.reactive.web.dto.retail.Product;
import com.spring.reactive.web.service.RetailService;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class MonoToFuture2 {
    public static final String ELECTRONICS = "Electronics";

    public static void main(String[] args) {
        try {
            List<Product> electronicProducts = Mono.fromCallable(() -> RetailService.getProducts())
                    .filter(products -> products.stream().anyMatch(product -> product.getCategory().equalsIgnoreCase(ELECTRONICS)))
                    .toFuture().get();
            System.out.println(electronicProducts);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }

    }
}
