package com.spring.reactive.web.concepts.combiningreactivestreams.merge;

import reactor.core.publisher.Flux;

public class FluxMerge1 {

    public static void main(String[] args) {

        Flux.just(1,2,3).mergeWith(Flux.just(4,5,6))
                .filter(num -> num % 2 ==0)
                .subscribe(System.out::println);
    }
}
