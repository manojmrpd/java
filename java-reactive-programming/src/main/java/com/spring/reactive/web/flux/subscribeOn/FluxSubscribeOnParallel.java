package com.spring.reactive.web.flux.subscribeOn;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

import static java.lang.Thread.sleep;

@Slf4j
public class FluxSubscribeOnParallel {

    public Flux<String> subcribeOnParallel() {
        Flux<String> flux1 = Flux.just("Alex", "Bin", "Charlie")
                .map(FluxSubscribeOnParallel::toUpperCase)
                .subscribeOn(Schedulers.parallel())
                .map(name -> {
                    log.info("Name in Flux1 {}", name);
                    return name;
                })
                .log();

        Flux<String> flux2 = Flux.just("Ded", "Egg", "Francis")
                .map(FluxSubscribeOnParallel::toUpperCase)
                .subscribeOn(Schedulers.parallel())
                .map(name -> {
                    log.info("Name in Flux2 {}", name);
                    return name;
                }).log();

        return flux1.mergeWith(flux2);
    }

    private static String toUpperCase(String s) {
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return s.toUpperCase();
    }
}
