package com.spring.reactive.web.concepts.creatingreactivestreams.just;

import reactor.core.publisher.Mono;

public class MonoJust1 {
    public static void main(String[] args) {
        getMessage().subscribe(message -> System.out.println(message));
    }

    private static Mono<String> getMessage() {
        return Mono.just("Welcome to the Web flux");
    }
}
