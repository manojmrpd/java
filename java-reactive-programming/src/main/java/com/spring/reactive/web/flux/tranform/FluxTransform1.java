package com.spring.reactive.web.flux.tranform;

import com.spring.reactive.web.mono.transform.MonoTransform1;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.function.Function;

public class FluxTransform1 {

    public static void main(String[] args) {
        Function<Flux<String>, Flux<String>> filterMap
                = name -> name.map(String::toUpperCase);

        Flux.fromIterable(List.of("Alex", "Slice", "Lucid"))
                .transform(filterMap)
                .flatMap(FluxTransform1::splitText)
                .subscribe(System.out::println);
    }

    private static Flux<?> splitText(String s) {
        List<String> charlist = List.of(s.split(""));
        return Flux.fromIterable(charlist);
    }
}
