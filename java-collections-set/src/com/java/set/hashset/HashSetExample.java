package com.java.set.hashset;

import java.util.HashSet;
import java.util.Set;

public class HashSetExample {

    public static void main(String[] args) {
        // Creating a HashSet to store unique integers
        Set<Integer> numberSet = new HashSet<>();

        // Adding elements to the HashSet
        numberSet.add(5);
        numberSet.add(10);
        numberSet.add(15);
        numberSet.add(20);
        numberSet.add(10); // Adding a duplicate element

        // Displaying the HashSet
        System.out.println("HashSet: " + numberSet);

        // Checking for the existence of an element
        int numberToCheck = 10;
        if (numberSet.contains(numberToCheck)) {
            System.out.println(numberToCheck + " exists in the HashSet.");
        } else {
            System.out.println(numberToCheck + " does not exist in the HashSet.");
        }

        // Removing an element from the HashSet
        int numberToRemove = 15;
        if (numberSet.remove(numberToRemove)) {
            System.out.println(numberToRemove + " has been removed from the HashSet.");
        } else {
            System.out.println(numberToRemove + " does not exist in the HashSet.");
        }

        // Displaying the HashSet after removal
        System.out.println("HashSet after removal: " + numberSet);

        // Iterating over the elements of the HashSet
        System.out.println("Elements of the HashSet:");
        for (Integer num : numberSet) {
            System.out.println(num);
        }

        // Clearing the HashSet
        numberSet.clear();
        System.out.println("HashSet after clearing: " + numberSet);
    }
}
