package com.java8.functionalinterface.function;

import java.util.function.Function;

public class Function1 {
    public static void main(String[] args) {

        Function<String, Integer> function1 = s -> s.length();
        Integer length = function1.apply("Hello");
        System.out.println("Length of the message is: " + length);

        Function<String, String> function2 = s -> s.toUpperCase();
        String upperCase = function2.apply("hello");
        System.out.println("Upper case of the message is: " + upperCase);

        // Function to calculate a 10% discount on a price
        Function<Double, Double> discount = price -> price * 0.9;
        double originalPrice = 100.0;
        double discountedPrice = discount.apply(originalPrice); // discountedPrice will be 90.0
        System.out.println("Discounted Price is: " + discountedPrice);

    }
}
