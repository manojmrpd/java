package com.java8.functionalinterface.function;

import com.java8.dto.Product;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Function2 {

    public static void main(String[] args) {

        List<Product> products = getProducts();
        Function<Product, Boolean> function = product -> product.getProductPrice() > 99;
        List<Boolean> checkPrice = products.stream().map(function).collect(Collectors.toList());
        System.out.println(checkPrice);

        Predicate<Product> predicate = product -> product.getProductPrice() > 99;
        List<Product> filteredProduct =
                products.stream().filter(predicate).collect(Collectors.toList());
        System.out.println(filteredProduct);

    }

    private static List<Product> getProducts() {
        return Arrays.asList(new Product(10, "Samsung", 98, null),
                new Product(11, "Apple", 125, null));
    }
}
