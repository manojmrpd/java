package com.java8.functionalinterface.functional.predicate.negate;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class PredicateNegateExample {

	public static void main(String[] args) {
		Predicate<String> startWithA = x -> x.startsWith("A");
		List<String> list = Arrays.asList("A", "AA", "AAA", "B", "BB", "BBB");
		List<String> negateList = list.stream().filter(startWithA.negate()).collect(Collectors.toList());
		System.out.println(negateList.toString());
	}
}
