package com.java8.functionalinterface.functional.predicate.or;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class PredicateOrExample {

	public static void main(String[] args) {
		Predicate<String> lengthIs3 = x -> x.length() == 3;
		Predicate<String> startWithA = x -> x.startsWith("A");
		List<String> list = Arrays.asList("A", "AA", "AAA", "B", "BB", "BBB");
		List<String> orList = list.stream().filter(lengthIs3.or(startWithA)).collect(Collectors.toList());
		System.out.println(orList.toString());
	}
}
