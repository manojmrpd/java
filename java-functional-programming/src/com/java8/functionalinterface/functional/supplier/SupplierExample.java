package com.java8.functionalinterface.functional.supplier;

import java.util.function.Supplier;

public class SupplierExample {

	public static void main(String[] args) {

		Supplier<Double> supplier = () -> Math.random();
		Double result = supplier.get();
		System.out.println(result);
	}
}
