package com.java8.functionalinterface.functional.bifunction;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;

public class BiFunctionExample {

	public static void main(String[] args) {

		BiFunction<Integer, Integer, Integer> biFunction = (a, b) -> a + b;
		Integer result = biFunction.apply(10, 20);
		System.out.println(result);

		BiFunction<Integer, Integer, Double> biFunction2 = (x1, x2) -> Math.pow(x1, x2);
		Double result1 = biFunction2.apply(2, 4);
		System.out.println(result1);

		BiFunction<Integer, Integer, List<Integer>> biFunction3 = (x1, x2) -> Arrays.asList(x1 + x2);
		List<Integer> result2 = biFunction3.apply(2, 4);
		System.out.println(result2);
	}
}
