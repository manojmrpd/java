package com.java8.functionalinterface.functional.biconsumer;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;

public class BiConsumerExample {

	public static void main(String[] args) {

		BiConsumer<Integer, Integer> addTwo = (x, y) -> System.out.println(x + y);
		addTwo.accept(1, 2);

		List<Integer> list = Arrays.asList(1, 2, 3, 4, 3, 2, 4, 5);
		Map<Integer, Long> map = list.stream()
				.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
		System.out.println(map.toString());
		// ForEach takes BiConsumer as an input
		map.forEach((k, v) -> System.out.println("Key " + k + " Value " + v));
	}
}
