package com.java8.functionalinterface.functional.function;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class FunctionExample {

	public static void main(String[] args) {
		
		List<String> list = Arrays.asList("hyd", "bang", "chen", "del");
		Function<String, String> function = city -> city.toUpperCase();
		List<String> result = list.stream().map(function).collect(Collectors.toList());
		System.out.println(result);
	}
}
