package com.java8.functionalinterface.functional.function;

import java.util.function.Function;

public class FunctionAndThen {

	public static void main(String[] args) {
		Function<Integer, Double> function = a -> a / 2.0;
		function = function.andThen(a -> 3 * a);
		Double result = function.apply(10);
		System.out.println(result);
	}
}
