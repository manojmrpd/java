package com.java8.functionalinterface.functional.function;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FunctionUserExample {

    public static void main(String[] args) {

        List<User> users = Arrays.asList(
                new User("usr01", "manoj", 32,
                        Arrays.asList(new Address("adr01", "madhapur", "hyderabad", 500084),
                                new Address("adr02", "main road", "maripeda", 506315))),
                new User("usr02", "ashwini", 30, Arrays.asList(new Address("adr01", "kondapur", "hyderabad", 500084),
                        new Address("adr02", "main road", "tanamcherla", 506316))));


        Function<User, List<Address>> function =
                user -> user.getAddress().stream().filter(address -> address.getPincode().equals(500084)).collect(Collectors.toList());

        List<List<Address>> result = users.stream().map(function).collect(Collectors.toList());
        System.out.println(result);
    }
}
