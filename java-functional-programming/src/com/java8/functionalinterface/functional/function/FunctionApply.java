package com.java8.functionalinterface.functional.function;

import java.util.function.Function;

public class FunctionApply {

	public static void main(String[] args) {
		Function<Integer, Double> function = a -> a / 2.0;
		Double result = function.apply(11);
		System.out.println(result);
	}
}
