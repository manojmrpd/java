package com.java8.functionalinterface.functional.function;

public class Address {
	
	private String id;
	private String street;
	private String city;
	private Integer pincode;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Integer getPincode() {
		return pincode;
	}
	public void setPincode(Integer pincode) {
		this.pincode = pincode;
	}
	@Override
	public String toString() {
		return "Address [id=" + id + ", street=" + street + ", city=" + city + ", pincode=" + pincode + "]";
	}
	public Address(String id, String street, String city, Integer pincode) {
		super();
		this.id = id;
		this.street = street;
		this.city = city;
		this.pincode = pincode;
	}
	
	

}
