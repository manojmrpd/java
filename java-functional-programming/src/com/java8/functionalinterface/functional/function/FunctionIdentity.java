package com.java8.functionalinterface.functional.function;

import java.util.function.Function;

public class FunctionIdentity {

	public static void main(String[] args) {
		Function<Integer, Integer> function = Function.identity();
		Integer result = function.apply(10);
		System.out.println(result);
	}
}
