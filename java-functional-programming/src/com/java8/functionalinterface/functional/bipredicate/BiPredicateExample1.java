package com.java8.functionalinterface.functional.bipredicate;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

public class BiPredicateExample1 {

	public static void main(String[] args) {

		List<Domain> domains = Arrays.asList(new Domain("google.com", 1), new Domain("i-am-spammer.com", 10),
				new Domain("mkyong.com", 0), new Domain("microsoft.com", 2));

		BiPredicate<String, Integer> biPredicate = (name, score) -> {
			return name.equalsIgnoreCase("google.com") || score == 0;
		};

		List<Domain> result1 = filterBadDomain(domains, biPredicate);
		System.out.println(result1);

		List<Domain> result2 = filterBadDomain(domains, (name, score) -> score == 0);
		System.out.println(result2);

		List<Domain> result3 = filterBadDomain(domains, (name, score) -> name.startsWith("m") || score == 10);
		System.out.println(result3);

	}

	public static List<Domain> filterBadDomain(List<Domain> domainList, BiPredicate<String, Integer> biPredicate) {
		return domainList.stream().filter(x -> biPredicate.test(x.getName(), x.getScore()))
				.collect(Collectors.toList());
	}
}
