package com.java8.functionalinterface.functional.bipredicate;

import java.util.function.BiPredicate;

public class BiPredicateExample {

	public static void main(String[] args) {

		BiPredicate<String, Integer> biPredicate = (x, y) -> {
			return x.length() == y;
		};
		boolean test = biPredicate.test("java", 4);
		System.out.println(test);
	}
}
