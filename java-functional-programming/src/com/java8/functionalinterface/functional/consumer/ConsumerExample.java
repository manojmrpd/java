package com.java8.functionalinterface.functional.consumer;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class ConsumerExample {

	public static void main(String[] args) {

		// Consumer interface example-1.
		Consumer<Integer> consumer = a -> System.out.println("Printing integer: " + a);
		consumer.accept(10);

		// Consumer interface example-2
		Consumer<List<Integer>> modify = list -> {
			for (int i = 0; i < list.size(); i++) {
				list.set(i, 2 * list.get(i));
			}
		};
		List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6);
		modify.accept(list);
		list.stream().forEach(a -> System.out.println(a));
	}

}
