package com.java8.functionalinterface.functional.consumer;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class ConsumerExample2 {

	public static void main(String[] args) {

		Consumer<List<Integer>> display = list -> {
			list.stream().forEach(a -> System.out.println(a));
		};
		Consumer<List<Integer>> modify = list -> {
			for (int i = 0; i < list.size(); i++) {
				list.set(i, 2 * list.get(i));
			}
		};
		List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
		modify.andThen(display).accept(list);
	}
}
