package com.java8.functionalinterface.bifunction;

import java.util.function.BiFunction;

public class BiFunction2 {

    public static void main(String[] args) {

        BiFunction<Integer, Integer, Double> biFunction = (length, width) -> (double) length * width;
        int length = 5, width = 10;
        double area = biFunction.apply(length, width);
        System.out.println(area);
    }
}
