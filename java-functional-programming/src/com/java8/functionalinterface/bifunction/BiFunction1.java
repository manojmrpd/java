package com.java8.functionalinterface.bifunction;

import java.util.function.BiFunction;

public class BiFunction1 {

    public static void main(String[] args) {
        BiFunction<String, String, String> biFunction =
                (firstName, secondName) -> firstName+ " " +secondName;
        String fullName = biFunction.apply("Alex", "Player");
        System.out.println(fullName);
    }
}
