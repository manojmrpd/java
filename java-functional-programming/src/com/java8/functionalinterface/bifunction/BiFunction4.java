package com.java8.functionalinterface.bifunction;

import java.util.Map;
import java.util.function.BiFunction;

public class BiFunction4 {
    public static void main(String[] args) {
        BiFunction<String, Integer, Map.Entry<String, Integer>> biFunction =
                (key, value) -> Map.entry(key, value);

        String key = "item_id";
        Integer value = 123;
        Map.Entry<String, Integer> entry = biFunction.apply(key, value);
        System.out.println(entry);
    }
}
