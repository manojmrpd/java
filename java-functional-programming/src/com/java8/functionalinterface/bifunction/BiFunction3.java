package com.java8.functionalinterface.bifunction;

import com.java8.dto.Product;

import java.util.function.BiFunction;

public class BiFunction3 {
    public static void main(String[] args) {
        BiFunction<Product, Product, Integer> compareProducts =
                (product, product2) -> {
                    int priceComparision = product.getProductPrice().compareTo(product2.getProductPrice());
                    return priceComparision != 0 ? priceComparision :
                            product.getProductName().compareTo(product2.getProductName());
                };

        Product product1 = new Product(1, "Laptop", 1000, null);
        Product product2 = new Product(2, "Phone", 800, null);
        int comparisonResult = compareProducts.apply(product1, product2);
        System.out.println(comparisonResult);
    }
}
