package com.java8.functionalinterface.consumer;

import com.java8.functionalinterface.bipredicate.Product;

import java.util.Optional;
import java.util.function.Consumer;

public class Consumer1 {
    public static void main(String[] args) {
        // Consumer to update the price of a product
        Consumer<Product> updatePrice = product -> product.setPrice(product.getPrice() * 0.9); // 10% discount

        Optional<Product> productOptional = getProducts();
        productOptional.ifPresent(updatePrice);
        System.out.println(productOptional.map(product -> product.getPrice()));
    }

    private static Optional<Product> getProducts() {
        return Optional.ofNullable(new Product(1, "Laptop", 180.00, "Electronics", 15));
    }
}
