package com.java8.functionalinterface.consumer;

import com.java8.dto.Product;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

public class Consumer2 {

    public static void main(String[] args) {

        // Consumer interface example.
        Consumer<Integer> consumer = integer -> System.out.println("Printing integer: " + integer);
        consumer.accept(10);

        // Consumer interface real-time example-1
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
        list.stream().forEach(i -> System.out.print(" " + i));

        // Consumer interface real-time example-2
        Optional<Product> productOptional = Optional.ofNullable(new Product(1, "Apple", 124, null));
        productOptional.ifPresent(product -> System.out.println("Product Name is: " + product.getProductName()));

    }

}
