package com.java8.functionalinterface.predicate;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Predicate2 {

	public static void main(String[] args) {

		// Predicate interface example
		Predicate<Integer> predicate = i -> i % 2 == 0;
		System.out.println("Check if given number is even or odd: " + predicate.test(12));

		// Predicate interface real-time example
		List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
		System.out.println("Original List: " + list.toString());
		
		List<Integer> filteredList = list.stream().filter(i -> i % 2 == 0).collect(Collectors.toList());
		System.out.println("Filtered List: " + filteredList.toString());

	}

}
