package com.java8.functionalinterface.predicate;

import java.util.function.Predicate;

public class Predicate1 {
    public static void main(String[] args) {
        // Predicate to check if a username is valid (must be alphanumeric and at least 6 characters long)
        Predicate<String> isValidUsername = username -> username.matches("^[a-zA-Z0-9_]+$") && username.length() >= 6;
        String username = "john123";
        boolean valid = isValidUsername.test(username);
        System.out.println(valid);
    }
}
