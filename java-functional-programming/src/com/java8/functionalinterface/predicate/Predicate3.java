package com.java8.functionalinterface.predicate;

import com.java8.optional.empty.Order;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Predicate3 {
    public static void main(String[] args) {
        List<Order> orders = getOrder();
        Predicate<Order> isRecentOrder =
                order -> order.getOrderDate().isAfter(LocalDate.now().minusMonths(1));
        List<Order> recentOrders =
                orders.stream().filter(isRecentOrder).collect(Collectors.toList());
        System.out.println(recentOrders);

    }


    private static List<Order> getOrder() {
        return Arrays.asList(new Order(1, "Order chopper", 1240.00, LocalDate.now()),
                new Order(2, "Order shopper", 2460.00, LocalDate.of(2024, 01, 01)));
    }
}
