package com.java8.functionalinterface.supplier;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

public class Supplier1 {

	public static void main(String[] args) {

		// Supplier interface example.
		Supplier<String> supplier = () -> "supplier example";
		System.out.println("Printing: " + supplier.get());

		// Supplier interface real-time example.
		Supplier<LocalDate> today = () -> LocalDate.now();
		LocalDate currentDate = today.get();
		System.out.println(currentDate);

		// Supplier interface real-time example.
		List<String> list = Arrays.asList("a","b");
		String str = list.stream().findAny().orElseGet(() -> "java example");
		System.out.println(str);

	}

}
