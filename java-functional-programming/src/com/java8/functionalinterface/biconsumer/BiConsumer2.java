package com.java8.functionalinterface.biconsumer;

import com.java8.functionalinterface.bipredicate.Product;

import java.util.function.BiConsumer;

public class BiConsumer2 {
    public static void main(String[] args) {

        // BiConsumer to compare two products by price and display information about the cheaper one
        BiConsumer<Product, Product> biConsumer = (product1, product2) -> {
            if (product1.getPrice() < product2.getPrice()) {
                System.out.println("Cheaper product: " + product1);
            } else {
                System.out.println("Cheaper product: " + product2);
            }
        };

        Product product1 = new Product(1, "Laptop", 1000.0, "Electronics", 20);
        Product product2 = new Product(2, "Phone", 800.0, "Electronics", 35);
        biConsumer.accept(product1, product2);
    }
}
