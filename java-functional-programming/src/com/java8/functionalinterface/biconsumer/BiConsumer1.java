package com.java8.functionalinterface.biconsumer;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

public class BiConsumer1 {
    public static void main(String[] args) {
        BiConsumer<String, Integer> biConsumer = (key, value) -> System.out.println(key + ": " + value);

        Map<String, Integer> productMap = new HashMap<>();
        productMap.put("Laptop", 1000);
        productMap.put("Phone", 500);

        productMap.forEach(biConsumer);

    }
}
