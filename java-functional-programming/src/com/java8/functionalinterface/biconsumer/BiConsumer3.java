package com.java8.functionalinterface.biconsumer;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.stream.IntStream;

public class BiConsumer3 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie");

        // BiConsumer to print each name in uppercase and its index
        BiConsumer<String, Integer> biConsumer =
                (name, index) -> System.out.println(index + 1 + ". " + name.toUpperCase());

        IntStream.range(0, names.size()).forEach(i -> biConsumer.accept(names.get(i), i));

    }
}
