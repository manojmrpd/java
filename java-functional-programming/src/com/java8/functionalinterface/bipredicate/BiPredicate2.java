package com.java8.functionalinterface.bipredicate;

import com.java8.functionalinterface.bipredicate.Product;

import java.util.function.BiPredicate;

public class BiPredicate2 {
    public static void main(String[] args) {

        // BiPredicate to compare products by price and then category (if prices are equal)
        BiPredicate<Product, Product> compareProducts = (product1, product2) -> {
            int priceComparison = Double.compare(product1.getPrice(), product2.getPrice());
            return priceComparison == 0 ? product1.getCategory().equals(product2.getCategory()) : priceComparison < 0;
        };
        Product product1 = new Product(1, "Laptop", 1000.0, "Electronics", 20);
        Product product2 = new Product(2, "Phone", 800.0, "Electronics", 35);

        boolean areProductsComparable = compareProducts.test(product1, product2);
        System.out.println(areProductsComparable);
        // areProductsComparable will be true (product1 is more expensive or both are equally priced and in the same category)

    }
}
