package com.java8.functionalinterface.bipredicate;

import java.util.function.BiPredicate;

public class BiPredicate1 {
    public static void main(String[] args) {
        BiPredicate<String, Integer> biPredicate =
                (gender, age) -> gender.equals("male") && age >= 18;
        boolean maleAdults = biPredicate.test("female", 25);
        System.out.println(maleAdults);

    }
}
