package com.java8.functionalinterface.bipredicate;

import com.java8.optional.empty.Order;

import java.time.LocalDate;
import java.util.function.BiPredicate;

public class BiPredicate3 {
    public static void main(String[] args) {
        // BiPredicate to check if an order is recent (placed within the last week) and has a total amount exceeding a threshold
        BiPredicate<Order, Double> isRecentAndExpensiveOrder = (order, threshold) ->
                order.getOrderDate().isAfter(LocalDate.now().minusWeeks(1)) && order.getTotal() > threshold;

        Order recentOrder = getOrderById(1);
        double minimumAmount = 100.0;

        if (isRecentAndExpensiveOrder.test(recentOrder, minimumAmount)) {
            System.out.println("Processed recent expensive order: " + recentOrder);
        } else {
            System.out.println("No expensive recent orders");
        }

    }

    private static Order getOrderById(int i) {
        return new Order(i, "Order shopper", 2460.00, LocalDate.of(2024, 03, 01));
    }
}
