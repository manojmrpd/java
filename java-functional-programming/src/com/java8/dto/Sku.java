package com.java8.dto;

public class Sku {

	private Integer skuId;
	private String skuName;
	private Integer skuPrice;

	public Integer getSkuId() {
		return skuId;
	}

	public void setSkuId(Integer skuId) {
		this.skuId = skuId;
	}

	public String getSkuName() {
		return skuName;
	}

	public void setSkuName(String skuName) {
		this.skuName = skuName;
	}

	public Integer getSkuPrice() {
		return skuPrice;
	}

	public void setSkuPrice(Integer skuPrice) {
		this.skuPrice = skuPrice;
	}

	public Sku(Integer skuId, String skuName, Integer skuPrice) {
		super();
		this.skuId = skuId;
		this.skuName = skuName;
		this.skuPrice = skuPrice;
	}

	@Override
	public String toString() {
		return "Sku [skuId=" + skuId + ", skuName=" + skuName + ", skuPrice=" + skuPrice + "]";
	}
	
}
