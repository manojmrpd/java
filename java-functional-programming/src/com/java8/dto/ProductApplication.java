package com.java8.dto;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ProductApplication {

	public static void main(String[] args) {
		
		Product smartphone = new Product(1, "smartphone", 50, Arrays.asList(new Sku(10, "apple", 60)));
		Product laptop = new Product(2, "laptop", 50, Arrays.asList(new Sku(20, "dell", 50)));
		Product tv = new Product(3, "tv", 30, Arrays.asList(new Sku(30, "sony", 40)));
		List<Product> product = Arrays.asList(smartphone, laptop, tv);
		 
		//Get the products list List<Product> whose sum of ((product.productPrice + sku.skuPrice) >= 100) using Java8 streams
		List<Product> list = product.stream().filter(i -> i.getProductPrice() + i.getSku().get(0).getSkuPrice() >= 100).collect(Collectors.toList());
		System.out.println(list.toString());
	}

}
