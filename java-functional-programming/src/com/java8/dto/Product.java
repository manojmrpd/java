package com.java8.dto;

import java.util.List;

public class Product {

	private Integer productId;
	private String productName;
	private Integer productPrice;
	private List<Sku> sku;

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(Integer productPrice) {
		this.productPrice = productPrice;
	}

	public List<Sku> getSku() {
		return sku;
	}

	public void setSku(List<Sku> sku) {
		this.sku = sku;
	}

	public Product(Integer productId, String productName, Integer productPrice, List<Sku> sku) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.productPrice = productPrice;
		this.sku = sku;
	}

	@Override
	public String toString() {
		return "Product [productId=" + productId + ", productName=" + productName + ", productPrice=" + productPrice
				+ ", sku=" + sku + "]";
	}

}
