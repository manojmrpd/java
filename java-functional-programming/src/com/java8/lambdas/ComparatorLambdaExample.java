package com.java8.lambdas;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ComparatorLambdaExample {

	public static void main(String[] args) {
		/**
		 * Prior JAVA 8
		 */
		Comparator<Integer> comparator = new Comparator<Integer>() {
			@Override
			public int compare(Integer o1, Integer o2) {
				return o1.compareTo(o2);
			}
		};
		System.out.println("The difference between two integer is : " + comparator.compare(8, 8));

		/**
		 * Now JAVA 8 Lambda expression
		 */
		List<Integer> numbers = Arrays.asList(3, 1, 5, 2);
		Collections.sort(numbers, (a, b) -> a.compareTo(b));

		List<Integer> evenNumbers =
				numbers.stream().filter(p -> p % 2 == 0).collect(Collectors.toList());
		System.out.println(evenNumbers);

		List<Integer> oddNumbers =
				numbers.stream().filter(p -> p % 2 != 0).collect(Collectors.toList());
		System.out.println(oddNumbers);


	}

}
