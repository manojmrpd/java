package com.java8.basics;

public class ThreadExecution {

    public static void main(String[] args) throws InterruptedException {
        long startTime = System.currentTimeMillis();

        Thread sleepThread = new Thread(() -> {
            try {
                Thread.sleep(1000); // Sleep for 1 second
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        sleepThread.start();

        // Your code to be measured here (e.g., a loop or a function call)

        sleepThread.join(); // Wait for the sleep thread to finish

        long endTime = System.currentTimeMillis();

        long elapsedTime = endTime - startTime;
        long sleepDuration = sleepThread.isAlive() ? 0 : 1000; // Handle potential remaining sleep time

        System.out.println("Elapsed time: " + elapsedTime + " ms");
        System.out.println("Estimated execution time: " + (elapsedTime - sleepDuration) + " ms");
    }
}
