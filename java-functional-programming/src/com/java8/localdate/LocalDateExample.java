package com.java8.localdate;

import java.time.LocalDate;
import java.time.Month;

public class LocalDateExample {

	public static void main(String[] args) {
		
		LocalDate actualDate = LocalDate.now().minusDays(90);
		System.out.println(actualDate);
		LocalDate cancellationDate = LocalDate.of(2023, Month.APRIL, 2);
		System.out.println(cancellationDate);
		if(cancellationDate.compareTo(actualDate) <= 0) {
			System.out.println("if");
		} else {
			System.out.println("else");
		}
	}
}
