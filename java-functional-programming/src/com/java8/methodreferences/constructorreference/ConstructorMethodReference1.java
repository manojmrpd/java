package com.java8.methodreferences.constructorreference;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ConstructorMethodReference1 {

    public static void main(String[] args) {
        List<Person> persons = Arrays.asList(
                new Person(25),
                new Person(30),
                new Person(18)
        );
        List<Person> adults = persons.stream().filter(p -> p.getAge() >= 20).filter(Person::isAdult).collect(Collectors.toList());
        System.out.println(adults);
    }
}
