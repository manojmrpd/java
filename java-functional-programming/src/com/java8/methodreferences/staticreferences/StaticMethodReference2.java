package com.java8.methodreferences.staticreferences;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StaticMethodReference2 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(10, -5, 20, -12);
        List<Integer> absoluteNumbers = numbers.stream().map(Math::abs).collect(Collectors.toList());
        System.out.println(absoluteNumbers);
    }
}
