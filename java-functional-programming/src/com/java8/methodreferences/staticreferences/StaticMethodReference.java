package com.java8.methodreferences.staticreferences;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class StaticMethodReference {

    public static void main(String[] args) {

        /**
         * PRIOR Java-8
         */
        List<String> names = Arrays.asList("Peter", "anna", "Mike");
        Collections.sort(names, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s1.compareToIgnoreCase(s2);
            }
        });
        System.out.println("Sorting names prior java-8: " + names);

        /**
         * Now Java8 Static Method reference example
         */
        Collections.sort(names, String::compareToIgnoreCase);
        System.out.println("Sorting names using java-8: " + names);

    }
}
