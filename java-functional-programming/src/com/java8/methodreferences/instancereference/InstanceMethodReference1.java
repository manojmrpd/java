package com.java8.methodreferences.instancereference;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class InstanceMethodReference1 {

    public static void main(String[] args) {

        List<String> words = Arrays.asList("apple", "banana", "orange", "pineapple");
        words.stream().filter(word -> word.length() > 5)
                .map(String::toUpperCase)
                .collect(Collectors.toList());
        System.out.println(words);

    }
}
