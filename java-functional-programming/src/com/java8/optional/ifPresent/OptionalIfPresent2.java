package com.java8.optional.ifPresent;

import com.java8.functionalinterface.functional.function.User;

import java.util.Optional;

public class OptionalIfPresent2 {
    public static void main(String[] args) {
        String username = "admin";
        String password = "password";
        Optional<User> user = authenticateUser(username, password);
        user.ifPresent(loggedInUser -> {
            System.out.println("Welcome back, " + loggedInUser);
        });
        String anonymousUser = user.orElse(new User("100", "Grey", 25, null)).getName();
        System.out.println("Welcome, " +anonymousUser);

    }

    private static Optional<User> authenticateUser(String username, String password) {
        return Optional.ofNullable(null);
    }

}