package com.java8.optional.ifPresent;

import java.util.Optional;

public class OptionalIfPresent1 {

    public static void main(String[] args) {
        Optional<String> nameOptional = Optional.ofNullable(getUserName());
        nameOptional.ifPresent(name -> saveUser(name));
    }

    private static void saveUser(String name) {
        System.out.println("Saved the user details in database: " + name);
    }

    private static String getUserName() {
        return "Manoj";
    }
}
