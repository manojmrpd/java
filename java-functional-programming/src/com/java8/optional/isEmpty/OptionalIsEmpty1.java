package com.java8.optional.isEmpty;

import java.util.Optional;

public class OptionalIsEmpty1 {

    public static void main(String[] args) {
        Optional<String> nameOptional = Optional.ofNullable(getUserName());
        if (nameOptional.isEmpty()) {
            String anonymousUser = nameOptional.orElse("Anonymous User");
            System.out.println("Hello, " + anonymousUser);
        } else {
            String name = nameOptional.get();
            System.out.println("Hello, " + name);
        }
    }

    private static String getUserName() {
        return null;
    }

}
