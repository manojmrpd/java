package com.java8.optional.orElse;

import java.util.Optional;

public class OptionalOrElse1 {
    //safe usage of get()
    public static void main(String[] args) {
        Optional<String> nameOptional = Optional.ofNullable(getUserName());
        String anonymousUser = nameOptional.orElse("Anonymous User");
        System.out.println("Hello, " + anonymousUser);
    }

    private static String getUserName() {
        return "Manoj";
    }
}
