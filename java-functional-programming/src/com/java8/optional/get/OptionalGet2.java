package com.java8.optional.get;

import java.util.Optional;

public class OptionalGet2 {

    //safe usage of get()
    public static void main(String[] args) {
        Optional<String> nameOptional = Optional.ofNullable(getUserName());
        if (nameOptional.isPresent()) {
            String name = nameOptional.get();
            System.out.println("Hello, " + name);
        } else {
            String anonymousUser = nameOptional.orElse("Anonymous User");
            System.out.println("Hello, " + anonymousUser);
        }
    }

    private static String getUserName() {
        return null;
    }
}
