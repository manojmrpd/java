package com.java8.optional.get;

import java.util.NoSuchElementException;
import java.util.Optional;

public class OptionalGet1 {
    public static void main(String[] args) {

        //Unsafe usage of get()
        Optional<String> emptyOptional = Optional.empty();
        try {
            String name = emptyOptional.get(); // Throws NoSuchElementException
            System.out.println("Hello, " + name + "!"); // This won't be executed
        } catch (NoSuchElementException e) {
            System.out.println("Optional is empty!");
        }
    }
}
