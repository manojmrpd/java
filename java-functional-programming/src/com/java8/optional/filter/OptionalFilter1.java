package com.java8.optional.filter;

import com.java8.dto.Product;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class OptionalFilter1 {

    public static void main(String[] args) {

        Optional<List<Product>> productOptional = getAllProducts();
        Optional<List<Product>> filteredProducts = productOptional.map(products -> products.stream().
                filter(product -> Optional.ofNullable(product.getProductPrice()).map(price -> price >= 100).orElse(false)).collect(Collectors.toList()));
        filteredProducts.ifPresent(products -> System.out.println(products));
    }

    private static Optional<List<Product>> getAllProducts() {
        return Optional.ofNullable(Arrays.asList(new Product(121, "apple", 125, null),
                new Product(122, "samsung", null, null)));
    }
}
