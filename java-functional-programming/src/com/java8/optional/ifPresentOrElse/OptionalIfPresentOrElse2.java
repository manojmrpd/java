package com.java8.optional.ifPresentOrElse;

import com.java8.dto.Product;

import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

public class OptionalIfPresentOrElse2 {

    public static void main(String[] args) {
        Integer productId = 2;
        Set<Inventory> inventorySet = new CopyOnWriteArraySet<>();
        Optional<Product> optionalProduct = getProductById(productId);
        optionalProduct.ifPresentOrElse(product -> inventorySet.add(new Inventory(product.getProductId(),
                        product.getProductName())),
                () -> inventorySet.add(new Inventory(productId, "default name")));
        System.out.println(inventorySet.toString());

    }

    private static Optional<Product> getProductById(Integer i) {
        return Optional.ofNullable(null);
    }
}
