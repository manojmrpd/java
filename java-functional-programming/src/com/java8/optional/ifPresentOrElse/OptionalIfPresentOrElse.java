package com.java8.optional.ifPresentOrElse;

import java.util.Optional;

public class OptionalIfPresentOrElse {
    public static void main(String[] args) {
        Optional<String> emailOptional = Optional.ofNullable(getEmail());
        emailOptional.ifPresentOrElse(email -> sendEmail(email), () -> System.out.println("email " +
                "sent failed"));
    }

    private static void sendEmail(String email) {
        System.out.println("Email sent successfully");
    }

    private static String getEmail() {
        return "manojmrpd@gmail.com";
    }
}
