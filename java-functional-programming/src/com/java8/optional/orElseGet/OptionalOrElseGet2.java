package com.java8.optional.orElseGet;

import com.java8.dto.Product;

import java.util.Optional;

public class OptionalOrElseGet2 {

    public static void main(String[] args) {
        Optional<Product> optionalProduct = getProductById(10);
        if (optionalProduct.isPresent()) {
            Product product = optionalProduct.get();
            System.out.println("Product details are: " + product);
        } else {
            optionalProduct.orElseGet(() -> {
                throw new RuntimeException("Product Not Found in database");
            });
        }
    }

    private static Optional<Product> getProductById(int i) {
        return Optional.ofNullable(null);
    }
}
