package com.java8.optional.orElseGet;

import com.java8.dto.Product;

import java.util.Optional;

public class OptionalOrElseGet1 {

    public static void main(String[] args) {
        Integer productId = 2;
        Optional<Product> optionalProduct = getProductById(productId);
        Product apple = optionalProduct.orElseGet(() -> createProduct(productId));
        System.out.println("Product details are: " + apple);

    }

    private static Product createProduct(Integer i) {
        return new Product(1, "apple", 100, null);
    }

    private static Optional<Product> getProductById(Integer i) {
        return Optional.ofNullable(new Product(i, "Samsung", 100, null));
    }
}
