package com.java8.optional.orElseGet;

import com.java8.dto.Product;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class OptionalOrElseGet3 {
    public static void main(String[] args) {
        Optional<List<Product>> purchaseHistory = getPurchaseHistory();
        List<Product> recommendedProducts = purchaseHistory.orElseGet(() -> getRecommendedProducts());
        System.out.println(recommendedProducts);

    }

    private static List<Product> getRecommendedProducts() {
        return Arrays.asList(new Product(121, "apple", 125, null),
                new Product(122, "samsung", 125, null));
    }

    private static Optional<List<Product>> getPurchaseHistory() {
        return Optional.ofNullable(null);
    }
}
