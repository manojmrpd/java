package com.java8.optional.of;

import com.java8.stream.intermediate.sorted.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class OptionalOfExample2 {

    public static void main(String[] args) {
        Optional<List<User>> optionalUsers = getDatabaseRecords();
        if (optionalUsers.isPresent()) {
            List<User> users = optionalUsers.get();

            // Bad approach
            List<User> maleUsers =
                    users.stream().filter(p -> p.getGender().equals("male")).collect(Collectors.toList());
            System.out.println("Null Pointer Exception is thrown");

            // Good approach
            List<User> male =
                    users.stream().filter(user -> Optional.ofNullable(user.getGender()).filter(gender -> gender.equals("female")).isPresent())
                            .collect(Collectors.toList());
            System.out.println(male);
        } else {
            System.out.println("No records found from database");
        }


    }
    private static Optional<List<User>> getDatabaseRecords() {
        List<User> userList = new ArrayList<>();
        userList.add(new User("10", "sharma", "male", 25));
        userList.add(new User("10", "khan", null, 22));
        userList.add(new User("10", "roy", "female", 23));
        return Optional.ofNullable(userList);
    }
}
