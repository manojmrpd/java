package com.java8.optional.of;

import java.util.List;

import com.java8.functionalinterface.functional.function.Address;

public class User {

	private String id;
	private String userName;
	private String gender;
	private Integer age;
	private List<Address> address;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public List<Address> getAddress() {
		return address;
	}

	public void setAddress(List<Address> address) {
		this.address = address;
	}

	public User(String id, String userName, String gender, Integer age, List<Address> address) {
		super();
		this.id = id;
		this.userName = userName;
		this.gender = gender;
		this.age = age;
		this.address = address;
	}

	public User() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", userName=" + userName + ", gender=" + gender + ", age=" + age + ", address="
				+ address + "]";
	}

}
