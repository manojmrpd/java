package com.java8.optional.of;

import java.util.Optional;

public class OptionalOfExample1 {

    public static void main(String[] args) {

        User user = new User("103", "lakshman", null, 22, null);

        // Optional empty
        Optional<Object> emptyObj = Optional.empty();
        System.out.println(emptyObj);

        // Optional of() method - Null Pointer exception is thrown
        Optional<String> optionalOf = Optional.of(user.getUserName());
        String userName = optionalOf.get();
        System.out.println(userName);
        String newName = optionalOf.map(String::toUpperCase).orElseGet(() -> "manoj");
        System.out.println(newName);

        Optional<String> gender = Optional.ofNullable(user.getGender());
        System.out.println(gender);
        if (gender.isPresent()) {
            gender.get();
        } else {
            gender.ifPresent(s -> System.out.println("Gender is: " + s));
            System.out.println("Gender is:" + gender.orElse("male"));
            System.out.println(gender.orElseThrow(() -> new IllegalArgumentException("Gender not present")));
        }
    }
}
