package com.java8.optional.map;

import com.java8.dto.Product;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class OptionalMap1 {

    public static void main(String[] args) {

        Optional<List<Product>> productOptional = getAllProducts();
        productOptional.map(products -> products.stream().
                        map(product -> Optional.ofNullable(product.getProductPrice()).map(price -> price * 0.9)).collect(Collectors.toList()))
                .ifPresentOrElse(discountedPrice -> System.out.println(discountedPrice),
                        () -> System.out.println("Discounts are not applicable"));
    }

    private static Optional<List<Product>> getAllProducts() {
        return Optional.ofNullable(Arrays.asList(new Product(121, "apple", 125, null),
                new Product(122, "samsung", null, null)));
    }
}
