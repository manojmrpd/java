package com.java8.optional.isPresent;

import java.util.Optional;

public class OptionalIsPresent1 {

    //safe usage of isPresent()
    public static void main(String[] args) {
        Optional<String> nameOptional = Optional.ofNullable(getUserName());
        if (nameOptional.isPresent()) {
            String name = nameOptional.get();
            System.out.println("Hello, " + name);
        } else {
            String anonymousUser = nameOptional.orElse("Anonymous User");
            System.out.println("Hello, " + anonymousUser);
        }
    }

    private static String getUserName() {
        return null;
    }
}
