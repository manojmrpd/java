package com.java8.optional.flatMap;

import com.java8.dto.Product;

import java.util.Optional;

public class Order {

    private Integer id;

    private String description;
    private Optional<Product> product;

    public Order(Integer id, String description, Optional<Product> product) {
        this.id = id;
        this.description = description;
        this.product = product;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Optional<Product> getProduct() {
        return product;
    }

    public void setProduct(Optional<Product> product) {
        this.product = product;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", product=" + product +
                '}';
    }
}
