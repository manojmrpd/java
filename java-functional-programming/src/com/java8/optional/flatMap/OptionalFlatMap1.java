package com.java8.optional.flatMap;

import com.java8.dto.Product;

import java.util.Optional;

public class OptionalFlatMap1 {

    public static void main(String[] args) {
        Optional<Order> orderOptional = getOrderById(2);
        String productName = orderOptional.flatMap(order -> order.getProduct().
                filter(product -> Optional.ofNullable(product.getProductName()).isPresent())
                .map(product -> product.getProductName())).orElse("empty");
        System.out.println("Product Name is : " + productName);
    }

    private static Optional<Order> getOrderById(int i) {
        return Optional.ofNullable(new Order(2, "pepejeans shirt black",
                Optional.ofNullable(new Product(12, null,
                        245
                        , null))));
    }
}
