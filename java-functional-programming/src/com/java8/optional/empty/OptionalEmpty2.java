package com.java8.optional.empty;

import java.util.Optional;

public class OptionalEmpty2 {
    public static void main(String[] args) {
        Customer customer = new Customer();
        Optional<Customer> customerOptional = Optional.ofNullable(customer);
        if (customerOptional.isPresent()) {
            Optional<Order> orderOptional = customerOptional.flatMap(customer1 -> Optional.ofNullable(customer.getOrder()));
            if (orderOptional.isPresent()) {
                System.out.println("Order details are: " + orderOptional.get());
            } else {
                System.out.println("Empty order");
            }
        } else {
            System.out.println("Empty customer");
        }
    }
}
