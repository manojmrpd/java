package com.java8.optional.empty;

import java.time.LocalDate;

public class Order {
    private Integer id;
    private String description;

    private Double total;
    private LocalDate orderDate;

    public Order(Integer id, String description, Double total, LocalDate orderDate) {
        this.id = id;
        this.description = description;
        this.total = total;
        this.orderDate = orderDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", orderDate=" + orderDate +
                '}';
    }
}
