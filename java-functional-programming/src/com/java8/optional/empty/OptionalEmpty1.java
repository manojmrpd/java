package com.java8.optional.empty;

import java.util.Optional;

public class OptionalEmpty1 {

    public static void main(String[] args) {

        Optional<Object> object = Optional.empty();
        if (object.isPresent()) {
            System.out.println("Not a empty optional");
        } else {
            System.out.println("Empty optional");
        }
    }
}
