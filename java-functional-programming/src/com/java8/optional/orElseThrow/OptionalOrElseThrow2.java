package com.java8.optional.orElseThrow;

import com.java8.dto.Product;

import java.util.Optional;

public class OptionalOrElseThrow2 {

    public static void main(String[] args) {
        try {
            Optional<Product> optionalProduct = getProductById(2);
            Product product = optionalProduct.orElseThrow(() -> {
                throw new RuntimeException("Product Not Found in database");
            });
            System.out.println("Product details are: " + product);
        } catch (RuntimeException e) {
            System.out.println("Product Not Found in database");
        }
    }

    private static Optional<Product> getProductById(Integer i) {
        return Optional.ofNullable(new Product(i, "Samsung", 100, null));
    }
}
