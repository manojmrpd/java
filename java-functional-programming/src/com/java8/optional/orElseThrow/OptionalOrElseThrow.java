package com.java8.optional.orElseThrow;

import java.util.NoSuchElementException;
import java.util.Optional;

public class OptionalOrElseThrow {

    public static void main(String[] args) {
        try {
            Optional<String> emailOptional = Optional.ofNullable(getEmail());
            String email = emailOptional.orElseThrow();
            System.out.println("email is sent successfully from: " + email);
        } catch (NoSuchElementException e) {
            System.out.println("Email is not present");
        }
    }

    private static String getEmail() {
        return "manojmrpd@gmail.com";
    }
}
