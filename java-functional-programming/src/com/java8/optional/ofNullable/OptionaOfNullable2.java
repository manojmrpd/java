package com.java8.optional.ofNullable;

import com.java8.stream.intermediate.sorted.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class OptionaOfNullable2 {

    public static void main(String[] args) {
        Optional<List<User>> optionalUsers = getUsersFromDB();
        if (optionalUsers.isPresent()) {
            List<User> users = optionalUsers.get();

            // Bad approach
            List<User> maleUsers =
                    users.stream().filter(p -> p.getGender().equals("male")).collect(Collectors.toList());
            System.out.println("A Null Pointer Exception is thrown");

            // Good approach-1
            List<User> female = users.stream().filter(user -> Optional.ofNullable(user.getGender()).isPresent() && user.getGender().equals("female")).collect(Collectors.toList());
            System.out.println(female);

            // Good approach-2
            List<User> male =
                    users.stream().filter(user -> Optional.ofNullable(user.getGender()).filter(gender -> gender.equals("male")).isPresent())
                            .collect(Collectors.toList());
            System.out.println(male);
        } else {
            System.out.println("No records found from database");
        }
    }

    private static Optional<List<User>> getUsersFromDB() {
        List<User> userList = new ArrayList<>();
        userList.add(new User("10", "sharma", "male", 25));
        userList.add(new User("10", "khan", null, 22));
        userList.add(new User("10", "roy", "female", 23));
        return Optional.ofNullable(userList);
    }
}
