package com.java8.optional.ofNullable;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.java8.functionalinterface.functional.function.Address;
import com.java8.functionalinterface.functional.function.User;

public class OptionalOfNullable1 {

	public static void main(String[] args) {

		List<User> users = Arrays.asList(
				new User("101", "manoj", 32,
						Arrays.asList(new Address("addr111", "madhapur", "hyderabad", 500084),
								new Address("addr112", "kondapur", null, 500084))),
				new User("102", "ashwini", 30, Arrays.asList(new Address("addr113", "ashoka",
								"maripeda", 506315),
						new Address("addr114", "kaman road", "tanamcherla", 506315))));

		List<User> usersList =
				users.stream().filter(p -> p.getAddress().stream().filter(address -> Optional.ofNullable(address.getCity()).isPresent())
				.anyMatch(s -> s.getCity().equals("hyderabad"))).collect(Collectors.toList());
		System.out.println(usersList);

	}

}
