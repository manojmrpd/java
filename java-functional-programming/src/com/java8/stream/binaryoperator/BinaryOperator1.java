package com.java8.stream.binaryoperator;

import java.util.Arrays;
import java.util.List;

public class BinaryOperator1 {
    public static void main(String[] args) {
        List<Integer> temperatures = Arrays.asList(15, 22, 18, 25);
        Integer result = temperatures.stream().reduce(0, Integer::max);
        System.out.println(result);
    }
}
