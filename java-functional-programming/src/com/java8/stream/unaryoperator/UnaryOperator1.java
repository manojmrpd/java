package com.java8.stream.unaryoperator;

import java.util.Arrays;
import java.util.List;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

public class UnaryOperator1 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4);
        List<Integer> result = numbers.stream().map(
                UnaryOperator.identity()).map(integer -> integer + 1).collect(Collectors.toList());
        System.out.println(result);
    }
}
