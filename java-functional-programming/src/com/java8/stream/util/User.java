package com.java8.stream.util;

import java.util.List;

public class User {

	private String id;
	private String userName;
	private String gender;
	private Integer age;
	private List<String> city;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public List<String> getCity() {
		return city;
	}

	public void setCity(List<String> city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", userName=" + userName + ", gender=" + gender + ", age=" + age + ", city=" + city
				+ "]";
	}

	public User(String id, String userName, String gender, Integer age, List<String> city) {
		super();
		this.id = id;
		this.userName = userName;
		this.gender = gender;
		this.age = age;
		this.city = city;
	}

}
