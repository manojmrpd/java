package com.java8.stream.util;

public class Transaction {
    private Integer id;
    private String name;
    private boolean isCommit;
    private boolean isRollback;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCommit() {
        return isCommit;
    }

    public void setCommit(boolean commit) {
        isCommit = commit;
    }

    public boolean isRollback() {
        return isRollback;
    }

    public void setRollback(boolean rollback) {
        isRollback = rollback;
    }

    public Transaction(Integer id, String name, boolean isCommit, boolean isRollback) {
        this.id = id;
        this.name = name;
        this.isCommit = isCommit;
        this.isRollback = isRollback;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", isCommit=" + isCommit +
                ", isRollback=" + isRollback +
                '}';
    }
}
