package com.java8.stream.util;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Stream;

public class StreamOfCollection {

	public static void main(String[] args) {
		
		Collection<String> list = Arrays.asList("a", "b", "c");
		Stream<String> stream = list.stream();
		System.out.println(stream.count());
	}
}
