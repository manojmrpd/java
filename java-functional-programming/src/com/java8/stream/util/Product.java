package com.java8.stream.util;

public class Product {
    private Integer productId;
    private String productName;
    private Double price;
    private String category;
    private Integer inStock;

    private Boolean isStockAvailable;

    public Boolean getStockAvailable() {
        return isStockAvailable;
    }

    public void setStockAvailable(Boolean stockAvailable) {
        isStockAvailable = stockAvailable;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getInStock() {
        return inStock;
    }

    public Product(Integer productId, String productName, Double price, String category, Integer inStock, Boolean isStockAvailable) {
        this.productId = productId;
        this.productName = productName;
        this.price = price;
        this.category = category;
        this.inStock = inStock;
        this.isStockAvailable = isStockAvailable;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productId=" + productId +
                ", productName='" + productName + '\'' +
                ", price=" + price +
                ", category='" + category + '\'' +
                ", inStock=" + inStock +
                ", isStockAvailable=" + isStockAvailable +
                '}';
    }

    public void setInStock(Integer inStock) {
        this.inStock = inStock;
    }
}
