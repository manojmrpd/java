package com.java8.stream.util;


import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

public class CommonService {

    public static List<Product> getProducts() {
        Product product1 = new Product(1, "Laptop", 1000.0, "Electronics", 20, true);
        Product product2 = new Product(2, "Phone", 800.0, "Electronics", 35, true);
        Product product3 = new Product(3, "Speaker", 300.00, "Electronics", 0, false);
        Product product4 = new Product(4, "Ear Phones", 200.00, "Electronics", 0, false);
        Product product5 = new Product(5, "T-Shirt", 100.00, "Fashion", 30, true);
        Product product6 = new Product(6, "Jeans", 90.00, "Fashion", 45, true);
        Product product7 = new Product(7, "Watch", 290.00, "Fashion", 0, false);
        Product product8 = new Product(8, "T-Shirt", 120.00, "Fashion", 12, true);
        Product product9 = new Product(9, "Toys", 120.00, "Kids", 9, true);
        Product product10 = new Product(10, "Books", 120.00, "Kids", 5, true);
        return Arrays.asList(product1, product2, product3, product4, product5, product6, product7
                , product8, product9, product10);
    }

    public static List<Book> getBooks() {
        Book book1 = new Book("The Lord of the Rings", "J.R.R. Tolkien", 1954);
        Book book2 = new Book("The Hobbit", "J.R.R. Tolkien", 1937);
        Book book3 = new Book("Nineteen Eighty-Four", "George Orwell", 1949);
        Book book4 = new Book("Animal Farm", "George Orwell", 1945);
        Book book5 = new Book("Lord of the Flies", "William Golding", 1954);
        return Arrays.asList(book1, book2, book3, book4, book5);
    }

    public static List<Order> getOrders() {
        Order order1 = new Order(101, "Apple Iphone-13", 1250.00, LocalDate.now(), getProducts());
        Order order2 = new Order(102, "Samsung S7 Edge", 950.00, LocalDate.now(), getProducts());
        return Arrays.asList(order1, order2);
    }
}
