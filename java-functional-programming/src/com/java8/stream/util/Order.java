package com.java8.stream.util;

import java.time.LocalDate;
import java.util.List;

public class Order {

    private Integer id;
    private String description;

    private Double total;
    private LocalDate orderDate;
    private List<Product> products;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public LocalDate getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Order(Integer id, String description, Double total, LocalDate orderDate, List<Product> products) {
        this.id = id;
        this.description = description;
        this.total = total;
        this.orderDate = orderDate;
        this.products = products;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", total=" + total +
                ", orderDate=" + orderDate +
                ", products=" + products +
                '}';
    }
}
