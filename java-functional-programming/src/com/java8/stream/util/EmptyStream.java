package com.java8.stream.util;

import java.util.stream.Stream;

public class EmptyStream {

	public static void main(String[] args) {
		
		// Empty stream
		Stream<String> stream = Stream.empty();
		// count of the stream elements
		System.out.println(stream.count());
		
	}
}
