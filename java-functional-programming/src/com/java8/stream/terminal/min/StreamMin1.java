package com.java8.stream.terminal.min;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class StreamMin1 {
    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        Optional<Product> lowestPriceProduct =
                products.stream().min(Comparator.comparing(Product::getPrice));
        lowestPriceProduct.ifPresent(product -> System.out.println(product));
    }
}
