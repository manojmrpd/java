package com.java8.stream.terminal.min;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.OptionalInt;

public class StreamMin2 {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(3, 5, 1, 4, 2, 6);

        // Lowest element in the array approach-1
        list.stream().min(Comparator.comparing(integer -> integer)).ifPresent(integer -> System.out.println(integer));

        // Lowest element in the array approach-2
        list.stream().mapToInt(value -> value).min().ifPresent(value -> System.out.println(value));

    }
}
