package com.java8.stream.terminal.forEachOrdered;

import java.util.Arrays;
import java.util.List;

public class StreamForEachOrdered3 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 4, 2, 5, 3);
        numbers.stream()
                .forEachOrdered(number -> {
                    if (number % 2 == 0) {
                        System.out.println("Even number: " + number);
                    }
                });
    }
}
