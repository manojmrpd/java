package com.java8.stream.terminal.forEachOrdered;

import java.util.Arrays;
import java.util.List;

public class StreamForEachOrdered1 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie", "Apple", "Andrea");
        names.stream().filter(name -> name.startsWith("A"))
                .forEachOrdered(s -> System.out.println(s));
    }
}
