package com.java8.stream.terminal.forEachOrdered;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.Comparator;
import java.util.List;

public class StreamForEachOrdered2 {
    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        products.stream().sorted(Comparator.comparing(Product::getProductName)
                        .thenComparing(Product::getProductName).thenComparing(Product::getPrice))
                .forEachOrdered(product -> System.out.println(product));
    }
}
