package com.java8.stream.terminal.allMatch;

import java.util.Arrays;
import java.util.List;

public class StreamAllMatch1 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, -4);
        boolean isPositive = numbers.stream().allMatch(integer -> integer > 0);
        System.out.println(isPositive);

    }
}
