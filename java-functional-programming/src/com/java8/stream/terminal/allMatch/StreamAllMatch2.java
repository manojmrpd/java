package com.java8.stream.terminal.allMatch;

import java.util.Arrays;
import java.util.List;

public class StreamAllMatch2 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(2, 4, 6, 8);
        boolean isEven = numbers.stream().allMatch(integer -> integer % 2 == 0);
        boolean isOdd = numbers.stream().allMatch(integer -> integer % 2 == 1);
        if (isEven) {
            System.out.println("Even numbers: " + numbers);
        } else if (isOdd) {
            System.out.println("Odd numbers: " + numbers);
        }
    }
}
