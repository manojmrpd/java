package com.java8.stream.terminal.noneMatch;

import java.util.Arrays;
import java.util.List;

public class StreamNonMatch1 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(2, 4, 6, 8);
        boolean isEven = numbers.stream().noneMatch(integer -> integer % 2 != 0);
        System.out.println(isEven);
    }
}
