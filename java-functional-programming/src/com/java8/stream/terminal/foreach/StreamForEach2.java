package com.java8.stream.terminal.foreach;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StreamForEach2 {

    public static void main(String[] args) {

        Map<String, String> map = new HashMap<>();
        map.put("1", "polity");
        map.put("2", "economy");
        map.put("3", "telangana");
        map.put("4", "telangana");
        map.replace("4", "energy");

        map.forEach((k, v) -> System.out.println(k + " : " + v));
        map.keySet().stream().forEach(key -> System.out.println(key));
        map.entrySet().stream().map(Map.Entry::getKey).forEach(key -> System.out.println(key));
        map.entrySet().stream().map(Map.Entry::getValue).forEach(value -> System.out.println(value));
    }
}
