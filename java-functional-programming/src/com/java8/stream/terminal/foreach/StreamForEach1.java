package com.java8.stream.terminal.foreach;

import java.util.Arrays;
import java.util.List;

public class StreamForEach1 {
    public static void main(String[] args) {
        // Consumer interface real-time example
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
        list.stream().forEach(number -> System.out.println(" " + number));

    }
}
