package com.java8.stream.terminal.sum;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.List;

public class StreamSum2 {
    private static final String FASHION = "Fashion";

    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        int totalInStockProductsOfFashion = products.stream()
                .filter(product -> product.getCategory().equalsIgnoreCase(FASHION))
                .mapToInt(Product::getInStock).sum();
        System.out.println(totalInStockProductsOfFashion);
    }
}
