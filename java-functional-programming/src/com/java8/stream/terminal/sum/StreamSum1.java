package com.java8.stream.terminal.sum;

import java.util.Arrays;
import java.util.List;

public class StreamSum1 {
    public static void main(String[] args) {
        List<Integer> dailySales = Arrays.asList(100, 150, 200, 120, 300);
        int totalSalesOfWeek = dailySales.stream().mapToInt(value -> value).sum();
        System.out.println(totalSalesOfWeek);
    }
}
