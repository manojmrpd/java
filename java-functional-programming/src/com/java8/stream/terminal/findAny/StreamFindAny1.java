package com.java8.stream.terminal.findAny;

import java.util.Arrays;
import java.util.List;

public class StreamFindAny1 {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(3, 5, 1, 4, 2, 6);
        list.stream().filter(integer -> integer >= 4).findAny().ifPresent(integer -> System.out.println(integer));
    }
}
