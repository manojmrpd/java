package com.java8.stream.terminal.findAny;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Order;
import com.java8.stream.util.Product;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class StreamFindAny2 {
    public static void main(String[] args) {
        List<Order> orders = CommonService.getOrders();
        Set<Product> electronics = orders.stream().flatMap(order -> order.getProducts().stream()
                        .filter(product -> product.getCategory().equals("Electronics")))
                .findAny().stream().collect(Collectors.toSet());
        System.out.println(electronics);
    }
}
