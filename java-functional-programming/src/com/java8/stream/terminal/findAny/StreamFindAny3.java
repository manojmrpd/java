package com.java8.stream.terminal.findAny;

import java.util.stream.IntStream;

public class StreamFindAny3 {
    public static void main(String[] args) {
        int[] numbers = {1, 3, 5, 7, 9, 12};
        IntStream.of(numbers).filter(value -> value % 2 == 0).findAny()
                .ifPresent(value -> System.out.println(value));
    }
}
