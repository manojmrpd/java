package com.java8.stream.terminal.max;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class StreamMax1 {
    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        Optional<Product> highestPriceProducts =
                products.stream().max(Comparator.comparing(Product::getPrice));
        highestPriceProducts.ifPresent(product -> System.out.println(product));
    }
}
