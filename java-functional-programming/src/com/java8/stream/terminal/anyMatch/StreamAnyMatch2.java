package com.java8.stream.terminal.anyMatch;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.java8.stream.util.Book;
import com.java8.stream.util.CommonService;

public class StreamAnyMatch2 {

	public static void main(String[] args) {

		List<Book> books = CommonService.getBooks();
		Set<Book> bookSet = books.stream().collect(Collectors.toSet());
		Set<Book> booksList = getBooks(bookSet);
		System.out.println(booksList.toString());

	}

	private static Set<Book> getBooks(Set<Book> bookSet) {

		Set<Book> releaseYearSet = bookSet.stream().filter(p -> p.getReleaseYear() == 1954).collect(Collectors.toSet());
		boolean authorCheck = releaseYearSet.stream().anyMatch(p -> p.getAuthor().equals("William Golding"));
		System.out.println(authorCheck);
		return releaseYearSet;
		

	}
}
