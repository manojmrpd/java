package com.java8.stream.terminal.anyMatch;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.List;

public class StreamAnyMatch1 {
    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        boolean isPriceCriteriaMeet =
                products.stream().anyMatch(product -> product.getCategory().equalsIgnoreCase(
                        "Electronics"));
        System.out.println(isPriceCriteriaMeet);

    }
}
