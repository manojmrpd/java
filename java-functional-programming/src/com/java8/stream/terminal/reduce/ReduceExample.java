package com.java8.stream.terminal.reduce;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class ReduceExample {

	public static void main(String[] args) {

		List<String> words = Arrays.asList("Hyderabad", "Bangalore", "Chennai", "Mumbai");
		List<Integer> numbers = Arrays.asList(10, 2, 3, 5, 6, 2);

		Optional<String> opt1 = words.stream()
				.reduce((city1, city2) -> city1.length() > city2.length() ? city1 : city2);
		opt1.ifPresent(System.out::println);

		Optional<String> opt2 = words.stream().reduce((word1, word2) -> word1.concat(word2));
		opt2.ifPresent(System.out::println);

		Optional<Integer> opt3 = numbers.stream().reduce((num1, num2) -> num1 + num2);
		opt3.ifPresent(System.out::println);

	}

}
