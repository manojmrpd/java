package com.java8.stream.terminal.findFirst;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class StreamFindFirst1 {

    public static void main(String[] args) {

        List<Integer> list = Arrays.asList(3, 5, 1, 4, 2, 6);
        //Second large number
        list.stream().distinct().sorted(Comparator.reverseOrder()).skip(1).findFirst().ifPresent(integer -> System.out.println(integer));

        //Third large number
        list.stream().distinct().sorted(Comparator.reverseOrder()).skip(2).findFirst().ifPresent(integer -> System.out.println(integer));
    }
}
