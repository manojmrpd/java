package com.java8.stream.terminal.findFirst;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamFindFirst2 {
    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        List<Product> firstExpensiveProduct =
                products.stream().filter(product -> product.getPrice() > 500.00).findFirst().stream().collect(Collectors.toList());
        System.out.println(firstExpensiveProduct);
    }
}
