package com.java8.stream.terminal.findFirst;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class StreamFindFirst3 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList(" ", "", "Alice", "Bob");
        names.stream().filter(name -> !name.isBlank()).findFirst().ifPresent(name -> System.out.println(name));

    }
}
