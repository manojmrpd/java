package com.java8.stream.terminal.count;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class StreamCount {
    public static void main(String[] args) {
        String text = "This is a sentence with words of varying lengths.";
        StringBuilder output =
                text.chars().mapToObj(value -> (char) value).filter(character -> Character.isLetter(character))
                        .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append);
        System.out.println(output);

        // Best approach
        long totalWords = Arrays.stream(text.split(" ")).filter(s -> s.length() >= 5).count();
        System.out.println(totalWords);
    }
}
