package com.java8.stream.terminal.average;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.List;

public class StreamAverage2 {
    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        double averagePriceOfProduct = products.stream().mapToDouble(Product::getPrice).average().getAsDouble();
        System.out.println(averagePriceOfProduct);
    }
}
