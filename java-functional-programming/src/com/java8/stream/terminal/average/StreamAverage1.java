package com.java8.stream.terminal.average;

import java.util.Arrays;
import java.util.List;
import java.util.OptionalDouble;

public class StreamAverage1 {
    public static void main(String[] args) {
        List<Double> temperatures = Arrays.asList(25.5, 28.3, 23.1, 27.2);
        OptionalDouble averageOptional =
                temperatures.stream().mapToDouble(value -> value).average();
        double averageTemparature = averageOptional.getAsDouble();
        System.out.println(averageTemparature);


    }
}
