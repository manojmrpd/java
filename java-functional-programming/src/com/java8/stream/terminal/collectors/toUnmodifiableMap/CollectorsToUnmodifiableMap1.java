package com.java8.stream.terminal.collectors.toUnmodifiableMap;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CollectorsToUnmodifiableMap1 {
    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        Map<String, Double> productPrices =
                products.stream().collect(Collectors.toMap(Product::getProductName,
                        Product::getPrice, (price1, price2) -> price1 + price2));
        System.out.println(productPrices);
    }
}
