package com.java8.stream.terminal.collectors.counting;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CollectorsCounting1 {
    public static void main(String[] args) {
        String lines = "I am an indian and proud of india";
        List<String> words = Arrays.stream(lines.split(" ")).collect(Collectors.toList());

        Long noOfWords =
                words.stream().collect(Collectors.collectingAndThen(Collectors.counting(), aLong -> aLong));
        System.out.println(noOfWords);
        
        Map<String, Long> wordCount =
                words.stream().collect(Collectors.groupingBy(word -> word, Collectors.counting()));
        System.out.println(wordCount);

    }
}
