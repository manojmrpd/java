package com.java8.stream.terminal.collectors.counting;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CollectorsCounting3 {
    public static void main(String[] args) {
        String city = "Hyderabad";
        Map<Character, Long> charCount = city.chars().mapToObj(value -> (char) value)
                .collect(Collectors.groupingBy(character -> character, Collectors.counting()));
        System.out.println(charCount);
    }
}
