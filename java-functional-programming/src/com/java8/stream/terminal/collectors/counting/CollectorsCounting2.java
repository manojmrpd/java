package com.java8.stream.terminal.collectors.counting;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CollectorsCounting2 {
    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        Map<String, Long> categories = products.stream().collect(Collectors.groupingBy(Product::getProductName,
                Collectors.counting()));
        System.out.println(categories);


    }
}
