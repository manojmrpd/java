package com.java8.stream.terminal.collectors.toArray;

import java.util.Arrays;
import java.util.List;

public class CollectorsToArray {
    public static void main(String[] args) {
        List<String> names = List.of("Alice", "Bob", "Charlie");
        // Create an array with enough capacity
        String[] namesArray = new String[names.size()];
        Object[] objectArray = names.stream().toArray();
        String[] stringArray = names.stream().toArray(value -> namesArray);
        String output = Arrays.toString(stringArray);
        String result = Arrays.toString(objectArray);
        System.out.println(output);
        System.out.println(result);


    }
}
