package com.java8.stream.terminal.collectors.toUnmodifiableSet;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class CollectorsToUnmodifiableSet {
    private static final String HOME_APPLIANCES = "Home Appliances";

    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        Set<String> categories =
                products.stream().map(Product::getCategory).collect(Collectors.toUnmodifiableSet());
        System.out.println(categories);
        try {
            categories.add(HOME_APPLIANCES);
        } catch (UnsupportedOperationException e) {
            System.out.println("Exception occurred while adding elements to unmodifiable set:"
                    + e.getMessage());
        }
    }
}
