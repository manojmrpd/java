package com.java8.stream.terminal.collectors.partitioningBy;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CollectorsPartitioningBy2 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(5, -3, 10, -2);

        Map<Boolean, List<Integer>> partitioningBy =
                numbers.stream().collect(Collectors.partitioningBy(integer -> integer > 0));
        System.out.println(partitioningBy);

        Map<Boolean, List<Integer>> groupingBy =
                numbers.stream().collect(Collectors.groupingBy(integer -> integer > 0));
        System.out.println(groupingBy);
    }
}
