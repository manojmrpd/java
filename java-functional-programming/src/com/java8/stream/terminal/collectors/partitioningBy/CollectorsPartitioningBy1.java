package com.java8.stream.terminal.collectors.partitioningBy;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CollectorsPartitioningBy1 {
    private static final String ELECTRONICS = "Electronics";

    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        Map<Boolean, List<Product>> paritiondMap = products.stream().collect(
                Collectors.partitioningBy(Product::getStockAvailable));
        System.out.println(paritiondMap);

    }
}
