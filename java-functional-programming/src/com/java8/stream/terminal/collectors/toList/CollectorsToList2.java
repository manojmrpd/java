package com.java8.stream.terminal.collectors.toList;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.List;
import java.util.stream.Collectors;

public class CollectorsToList2 {
    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        List<String> categories =
                products.stream().map(Product::getCategory).collect(Collectors.toList());
        System.out.println(categories );
    }
}
