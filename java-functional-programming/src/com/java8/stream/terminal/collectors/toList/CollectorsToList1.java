package com.java8.stream.terminal.collectors.toList;

import java.util.List;
import java.util.stream.Collectors;

public class CollectorsToList1 {
    public static void main(String[] args) {
        List<String> names = List.of("Alice", "Bob", "Charlie");
        List<String> upperCase = names.stream().map(String::toUpperCase).toList();
        System.out.println(upperCase);
        List<String> lowerCase =
                names.stream().map(String::toLowerCase).collect(Collectors.toList());
        System.out.println(lowerCase);
    }
}
