package com.java8.stream.terminal.collectors.joining;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CollectorsJoining2 {
    public static void main(String[] args) {
        List<String> locations = Arrays.asList("Home", "Electronics", "Laptops");
        String breadcrumb = locations.stream()
                .collect(Collectors.joining(" > ", "", " (You are here)"));
        System.out.println(breadcrumb);
    }
}
