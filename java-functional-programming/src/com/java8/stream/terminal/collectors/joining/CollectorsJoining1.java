package com.java8.stream.terminal.collectors.joining;

import java.util.List;
import java.util.stream.Collectors;

public class CollectorsJoining1 {
    public static void main(String[] args) {
        List<String> names = List.of("Alice", "Bob", "Charlie");
        String defaultJoining = names.stream().collect(Collectors.joining());
        System.out.println(defaultJoining);
        String delimiterJoining = names.stream().collect(Collectors.joining(", "));
        System.out.println(delimiterJoining);
    }
}
