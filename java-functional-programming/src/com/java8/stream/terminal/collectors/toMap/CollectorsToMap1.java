package com.java8.stream.terminal.collectors.toMap;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CollectorsToMap1 {
    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        Map<String, Double> productPrices =
                products.stream().collect(Collectors.toMap(Product::getProductName, Product::getPrice));
        System.out.println(productPrices);
        Map<String, Integer> productStock = products.stream().collect(Collectors.toMap(Product::getProductName, Product::getInStock));
        System.out.println(productStock);
    }
}
