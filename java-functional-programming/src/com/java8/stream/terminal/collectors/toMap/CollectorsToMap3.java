package com.java8.stream.terminal.collectors.toMap;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CollectorsToMap3 {
    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        //Custom Logic for Merging Values
        Map<String, Integer> productMap = products.stream()
                .collect(Collectors.toMap(Product::getProductName, Product::getInStock,
                (stock1, stock2) -> Math.addExact(stock1, stock2)));
        System.out.println(productMap);
    }
}
