package com.java8.stream.terminal.collectors.toMap;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CollectorsToMap2 {
    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        Map<String, String> duplicate =
                products.stream().collect(Collectors.toMap(Product::getCategory
                        , Product::getProductName));
        // Throws java.lang.IllegalStateException: Duplicate key Electronics
        // (attempted merging values Laptop and Phone)
        System.out.println(duplicate);
        Map<String, String> unique =
                products.stream().collect(Collectors.toMap(Product::getCategory, Product::getProductName
                        , (oldKey, newKey) -> newKey));
        //Handling duplicate keys by overriding new Key
        System.out.println(unique);

    }
}
