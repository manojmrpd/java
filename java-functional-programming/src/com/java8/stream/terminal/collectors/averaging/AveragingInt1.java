package com.java8.stream.terminal.collectors.averaging;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class AveragingInt1 {
    public static void main(String[] args) {
        List<Integer> scores = Arrays.asList(85, 92, 78, 95);
        Double average = scores.stream().collect(Collectors.averagingInt(score -> score));
        System.out.println(average);

    }
}
