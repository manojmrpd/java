package com.java8.stream.terminal.collectors.averaging;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Order;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class AveragingDouble2 {
    public static void main(String[] args) {
        List<Order> orders = CommonService.getOrders();
        Map<Integer, Double> result = orders.stream().collect(Collectors.groupingBy(Order::getId,
                Collectors.averagingDouble(Order::getTotal)));
        System.out.println(result);
    }
}
