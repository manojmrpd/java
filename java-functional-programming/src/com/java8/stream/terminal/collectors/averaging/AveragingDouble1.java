package com.java8.stream.terminal.collectors.averaging;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class AveragingDouble1 {
    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        Map<String, Double> averageStocks =
                products.stream().collect(Collectors.groupingBy(Product::getCategory,
                        Collectors.averagingDouble(Product::getPrice)));
        System.out.println(averageStocks);
    }
}
