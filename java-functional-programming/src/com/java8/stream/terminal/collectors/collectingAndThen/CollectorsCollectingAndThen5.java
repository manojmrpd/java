package com.java8.stream.terminal.collectors.collectingAndThen;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Order;
import com.java8.stream.util.Product;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CollectorsCollectingAndThen5 {
    public static void main(String[] args) {
        List<Order> orders = CommonService.getOrders();
        Double average = orders.stream().flatMap(order -> order.getProducts().stream())
                .collect(Collectors.collectingAndThen(Collectors.toList(),
                        lists -> lists.stream().mapToDouble(Product::getPrice).average().orElse(0.0)));
        System.out.println(average);
    }
}
