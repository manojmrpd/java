package com.java8.stream.terminal.collectors.collectingAndThen;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class CollectorsCollectingAndThen2 {
    public static void main(String[] args) {
        List<String> colors = Arrays.asList("Red", "Green", "Blue", "Red");
        HashSet<String> unique =
                colors.stream().collect(Collectors.collectingAndThen(Collectors.toList(),
                HashSet::new));
        System.out.println(unique);
    }
}
