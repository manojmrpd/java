package com.java8.stream.terminal.collectors.collectingAndThen;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CollectorsCollectingAndThen1 {
    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        Map<Integer, List<Product>> output = products.stream().collect(Collectors.collectingAndThen(Collectors
                .groupingBy(Product::getProductId), map -> map.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (products1, products2) -> products2))));
        System.out.println(output);
    }
}
