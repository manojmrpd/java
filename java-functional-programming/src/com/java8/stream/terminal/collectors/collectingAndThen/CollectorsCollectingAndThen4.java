package com.java8.stream.terminal.collectors.collectingAndThen;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CollectorsCollectingAndThen4 {
    private static final String ELECTRONICS = "Electronics";

    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        Map<String, Long> map =
                products.stream().filter(product -> product.getCategory().equalsIgnoreCase(ELECTRONICS))
                        .collect(Collectors.collectingAndThen(Collectors.toList(),
                                productList -> productList.stream()
                                .collect(Collectors.groupingBy(Product::getCategory, Collectors.counting()))));
        System.out.println(map);
    }
}
