package com.java8.stream.terminal.collectors.collectingAndThen;

import java.util.*;
import java.util.stream.Collectors;

public class CollectorsCollectingAndThen3 {
    public static void main(String[] args) {
        List<String> colors = Arrays.asList("Red", "Green", "Blue", "Red");
        Collection<String> collect = colors.stream().collect(Collectors.collectingAndThen(Collectors.toList(),
                Collections::unmodifiableCollection));
        System.out.println(collect);
        collect.add("Yellow"); // throws UnsupportedOperationException

    }
}
