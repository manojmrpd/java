package com.java8.stream.terminal.collectors.reducing;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CollectorsReducing2 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(10, 20, 30, 40);
        Integer sum = numbers.stream().collect(Collectors.reducing(0, Integer::sum));
        System.out.println(sum);
    }
}
