package com.java8.stream.terminal.collectors.reducing;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CollectorsReducing4 {
    public static void main(String[] args) {
        List<String> words = Arrays.asList("Hello", "World", "!");
        String output = words.stream().collect(Collectors.reducing("", (s, s2) -> s + "#" + s2));
        System.out.println(output);

    }
}
