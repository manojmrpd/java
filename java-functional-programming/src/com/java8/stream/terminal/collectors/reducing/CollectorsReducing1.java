package com.java8.stream.terminal.collectors.reducing;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class CollectorsReducing1 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(10, 20, 30, 40);
        Optional<Integer> numberOptional =
                numbers.stream().collect(Collectors.reducing((number1, number2) -> number1 + number2));
        System.out.println(numberOptional.get());

    }
}
