package com.java8.stream.terminal.collectors.reducing;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class CollectorsReducing3 {
    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        Optional<Product> expensivePriceProduct =
                products.stream().collect(Collectors.reducing((product, product2) ->
                        product.getPrice() > product2.getPrice() ? product : product2));
        System.out.println(expensivePriceProduct.get());
    }
}
