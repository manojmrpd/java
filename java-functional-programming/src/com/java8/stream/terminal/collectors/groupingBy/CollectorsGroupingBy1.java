package com.java8.stream.terminal.collectors.groupingBy;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CollectorsGroupingBy1 {
    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        Map<String, List<Product>> groupingByCategory =
                products.stream().collect(Collectors.groupingBy(Product::getCategory));
        System.out.println(groupingByCategory);
    }
}
