package com.java8.stream.terminal.collectors.toCollection;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

public class CollectorsToCollection1 {
    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        CopyOnWriteArrayList<Product> productList = products.stream()
                .collect(Collectors.toCollection(CopyOnWriteArrayList::new));
        System.out.println(productList);

    }
}
