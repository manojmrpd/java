package com.java8.stream.terminal.collectors.toCollection;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.List;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.stream.Collectors;

public class CollectorsToCollection2 {
    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        CopyOnWriteArraySet<Product> productList = products.stream()
                .collect(Collectors.toCollection(CopyOnWriteArraySet::new));
        System.out.println(productList);
    }
}
