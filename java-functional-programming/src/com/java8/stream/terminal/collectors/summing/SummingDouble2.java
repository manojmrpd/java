package com.java8.stream.terminal.collectors.summing;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Order;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class SummingDouble2 {
    public static void main(String[] args) {
        List<Order> orders = CommonService.getOrders();
        Map<String, Double> result =
                orders.stream().collect(Collectors.groupingBy(Order::getDescription,
                        Collectors.summingDouble(Order::getTotal)));
        System.out.println(result);
    }
}
