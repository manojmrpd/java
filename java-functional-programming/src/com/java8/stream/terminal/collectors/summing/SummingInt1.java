package com.java8.stream.terminal.collectors.summing;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class SummingInt1 {
    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        Map<String, Integer> map =
                products.stream().collect(Collectors.groupingBy(Product::getCategory,
                        Collectors.summingInt(Product::getInStock)));
        System.out.println(map);
    }
}
