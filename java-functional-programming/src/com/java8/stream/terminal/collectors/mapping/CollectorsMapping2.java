package com.java8.stream.terminal.collectors.mapping;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Order;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CollectorsMapping2 {
    public static void main(String[] args) {
        List<Order> orders = CommonService.getOrders();
        Map<String, List<Double>> mapping =
                orders.stream().collect(Collectors.groupingBy(Order::getDescription,
                        Collectors.mapping(Order::getTotal, Collectors.toList())));
        System.out.println(mapping);
    }
}
