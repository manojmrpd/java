package com.java8.stream.terminal.collectors.mapping;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CollectorsMapping1 {
    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        Map<String, List<String>> categories =
                products.stream().collect(Collectors.groupingBy(Product::getCategory,
                        Collectors.mapping(Product::getProductName, Collectors.toList())));
        System.out.println(categories);

    }
}
