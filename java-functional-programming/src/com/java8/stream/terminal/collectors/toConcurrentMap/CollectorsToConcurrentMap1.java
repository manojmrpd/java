package com.java8.stream.terminal.collectors.toConcurrentMap;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

public class CollectorsToConcurrentMap1 {
    public static void main(String[] args) {
        String sentence = "The number of the elements in the stream is five";
        List<String> words = Arrays.stream(sentence.split(" ")).collect(Collectors.toList());
        ConcurrentMap<String, Long> map =
                words.stream().collect(Collectors.toConcurrentMap(word -> word, word -> 1L,
                (count1, count2) -> count1 + count2));
        System.out.println(map);
    }
}
