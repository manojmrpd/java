package com.java8.stream.terminal.collectors.toConcurrentMap;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.List;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

public class CollectorsToConcurrentMap2 {
    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        ConcurrentMap<String, Double> map =
                products.stream().collect(Collectors.toConcurrentMap(Product::getProductName,
                        Product::getPrice, (price1, price2) -> price1 + price2));
        System.out.println(map);

    }
}
