package com.java8.stream.terminal.collectors.toSet;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class CollectorsToSet1 {
    public static void main(String[] args) {
        List<String> words = List.of("apple", "banana", "apple", "orange");
        Set<String> uniqueWords = words.stream().collect(Collectors.toSet());
        System.out.println(uniqueWords);
    }
}
