package com.java8.stream.terminal.collectors.toSet;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class CollectorsToSet2 {
    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        Set<String> categories =
                products.stream().map(Product::getCategory).collect(Collectors.toUnmodifiableSet());
        System.out.println(categories   );
    }
}
