package com.java8.stream.terminal.collectors.maxBy;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class CollectorsMaxBy1 {
    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        Optional<Product> expensiveProduct =
                products.stream().collect(Collectors.maxBy(Comparator.comparingDouble(Product::getPrice)));
        expensiveProduct.ifPresent(product -> System.out.println(product));
    }
}
