package com.java8.stream.terminal.collectors.minBy;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class CollectorsMinBy1 {
    private static final String ELECTRONICS = "Electronics";
    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        Optional<Product> cheapestProduct =
                products.stream()
                        .filter(product -> product.getCategory().equalsIgnoreCase("Electronics"))
                        .collect(Collectors.minBy(Comparator.comparingDouble(Product::getPrice)));
        cheapestProduct.ifPresent(product -> System.out.println(product));
    }
}
