package com.java8.stream.terminal.collectors.flatMapping;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Order;
import com.java8.stream.util.Product;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CollectorsFlatMapping2 {
    public static void main(String[] args) {
        List<Order> orders = CommonService.getOrders();
        Map<Integer, Map<String, List<Product>>> flatMapping =
                orders.stream().collect(Collectors.groupingBy(Order::getId,
                        Collectors.flatMapping(order -> order.getProducts().stream(),
                                Collectors.groupingBy(Product::getCategory))));
        System.out.println(flatMapping);

    }
}
