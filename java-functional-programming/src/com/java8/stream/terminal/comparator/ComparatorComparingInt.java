package com.java8.stream.terminal.comparator;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ComparatorComparingInt {
    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        List<Product> result = products.stream().sorted(
                Comparator.comparingInt(Product::getInStock)).collect(Collectors.toList());
        System.out.println(result);
    }
}
