package com.java8.stream.terminal.comparator;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ComparatorReverseOrder {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(3, 1, 4, 2);
        List<Integer> results =
                numbers.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
        System.out.println(results);
    }
}
