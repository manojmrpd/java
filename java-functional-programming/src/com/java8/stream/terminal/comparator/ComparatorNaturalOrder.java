package com.java8.stream.terminal.comparator;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ComparatorNaturalOrder {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(3, 1, 4, 2);
        List<Integer> results =
                numbers.stream().sorted(Comparator.naturalOrder()).collect(Collectors.toList());
        System.out.println(results);
    }
}
