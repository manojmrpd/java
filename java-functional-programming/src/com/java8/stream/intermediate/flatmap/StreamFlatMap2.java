package com.java8.stream.intermediate.flatmap;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Order;
import com.java8.stream.util.Product;

import java.util.List;
import java.util.stream.Collectors;

public class StreamFlatMap2 {
    public static void main(String[] args) {
        List<Order> orders = CommonService.getOrders();
        List<Product> products =
                orders.stream().flatMap(order -> order.getProducts().stream()).collect(Collectors.toList());
        System.out.println(products);
    }
}
