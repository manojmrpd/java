package com.java8.stream.intermediate.flatmap;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StreamFlatMap3 {
    private static final String ELECTRONICS = "Electronics";
    private static final String FASHION = "Fashion";

    public static void main(String[] args) {
        Map<String, List<Product>> categories = getProductsByCategory();
        List<Product> products =
                categories.entrySet().stream().flatMap(product -> product.getValue().stream().sorted(Comparator.comparing(Product::getCategory))).collect(Collectors.toList());
        System.out.println(products);
    }

    private static Map<String, List<Product>> getProductsByCategory() {
        Map<String, List<Product>> map = new HashMap<>();
        map.put(ELECTRONICS,
                CommonService.getProducts().stream().filter(product -> product.getCategory().equalsIgnoreCase(ELECTRONICS)).collect(Collectors.toList()));
        map.put(FASHION,
                CommonService.getProducts().stream().filter(product -> product.getCategory().equalsIgnoreCase(FASHION)).collect(Collectors.toList()));
        return map;
    }
}
