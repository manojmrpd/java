package com.java8.stream.intermediate.map;

import com.java8.stream.util.User;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


public class StreamMap3 {

	public static void main(String[] args) {
		
		List<User> list = getUsers();
		List<String> userNames = list.stream().map(user -> user.getUserName()).collect(Collectors.toList());
		System.out.println(userNames.toString());
		
		List<List<String>> cities = list.stream().map(user -> user.getCity()).collect(Collectors.toList());
		System.out.println(cities.toString());
		
		List<String> cityList = list.stream().flatMap(user -> user.getCity().stream()).collect(Collectors.toList());
		System.out.println(cityList.toString());

	}

	private static List<User> getUsers() {
		User user1 = new User("101", "ramu", "male", 23, Arrays.asList("ayodhya", "bhadradri"));
		User user2 = new User("102", "sita", "female", 21, Arrays.asList("ayodhya", "warangal"));
		User user3 = new User("103", "lakshman", "male", 22, Arrays.asList("ayodhya", "khammam"));
		return Arrays.asList(user1, user2, user3);
	}

}
