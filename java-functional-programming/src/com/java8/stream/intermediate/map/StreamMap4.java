package com.java8.stream.intermediate.map;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamMap4 {
    public static void main(String[] args) {
        Map<String, Integer> productPrices = new HashMap<>();
        productPrices.put("apple", 100);
        productPrices.put("banana", 50);

        // Stream of values
        Stream<Integer> priceStream = productPrices.values().stream();
        // Apply map() to transform values
        List<Double> discountedPrices = priceStream.map(price -> price * 0.9).collect(Collectors.toList());
        System.out.println(discountedPrices);

    }
}
