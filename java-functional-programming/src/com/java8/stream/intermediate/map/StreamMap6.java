package com.java8.stream.intermediate.map;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StreamMap6 {
    public static void main(String[] args) {
        Map<String, Integer> productCodes = new HashMap<>();
        productCodes.put("apple", 123);
        productCodes.put("banana", 456);
        productCodes.put("avocado", 245);
        List<String> productCodeOfA =
                productCodes.entrySet().stream().map(stringIntegerEntry -> stringIntegerEntry.getKey())
                .filter(s -> s.startsWith("a")).collect(Collectors.toList());
        System.out.println(productCodeOfA);
    }
}
