package com.java8.stream.intermediate.map;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.List;
import java.util.stream.Collectors;

public class StreamMap2 {
    private static final String ELECTRONICS = "Electronics";

    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        List<String> electronicProducts =
                products.stream().filter(product -> product.getCategory().equalsIgnoreCase(ELECTRONICS))
                        .map(product -> product.getProductName()).collect(Collectors.toList());
        System.out.println(electronicProducts);
    }
}
