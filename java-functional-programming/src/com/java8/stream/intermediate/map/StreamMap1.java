package com.java8.stream.intermediate.map;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamMap1 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie");
        List<String> upperCaseNames = names.stream().map(String::toUpperCase).collect(Collectors.toList());
        System.out.println(upperCaseNames);

    }
}
