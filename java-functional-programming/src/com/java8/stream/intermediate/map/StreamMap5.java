package com.java8.stream.intermediate.map;

import com.java8.stream.util.FirstPremiumInfo;
import com.java8.stream.util.PolicyInfo;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.function.Function;

public class StreamMap5 {

	public static void main(String[] args) {
		PolicyInfo policyInfo = getPolicyInfo();
		System.out.println(policyInfo);
		System.out.println(calculateLumpSumPremium(policyInfo));
	}

	private static String calculateLumpSumPremium(PolicyInfo policyInfo) {

		Function<FirstPremiumInfo, BigDecimal> mapper = firstPremiumInfo -> {
			if (null != firstPremiumInfo.getCurrencyPay() && firstPremiumInfo.getCurrencyPay().equals("JPY")) {
				return new BigDecimal(firstPremiumInfo.getFirstPremiumAmount());
			} else {
				return new BigDecimal(firstPremiumInfo.getFirstPremiumAmount()).multiply(BigDecimal.valueOf(2.0));
			}
		};
		BigDecimal amountInfo = policyInfo.getFirstPremiumInfo().stream().map(mapper).reduce(BigDecimal.ZERO,
				BigDecimal::add);
		return amountInfo.toString();
	}

	private static PolicyInfo getPolicyInfo() {
		PolicyInfo policyInfo = new PolicyInfo();
		policyInfo.setLumpSumPremium("1000");
		policyInfo.setFirstPremiumInfo(
				Arrays.asList(new FirstPremiumInfo("JPY", "200.45"), new FirstPremiumInfo("NZ", "300.46"),
						new FirstPremiumInfo("JPY", "250.75"), new FirstPremiumInfo("USD", "350.20")));
		return policyInfo;
	}

}
