package com.java8.stream.intermediate.skip;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class StreamSkip3 {
    public static void main(String[] args) {

        List<Integer> list = Arrays.asList(3, 5, 1, 4, 2, 6);

        //Second large number
        list.stream().distinct().sorted(Comparator.reverseOrder()).skip(1).findFirst().ifPresent(integer -> System.out.println(integer));

        //Third large number
        list.stream().distinct().sorted(Comparator.reverseOrder()).skip(2).findFirst().ifPresent(integer -> System.out.println(integer));

        //Second small number
        list.stream().distinct().sorted().skip(1).findFirst().ifPresent(integer -> System.out.println(integer));

        //Third small number
        list.stream().distinct().sorted().skip(2).findFirst().ifPresent(integer -> System.out.println(integer));
    }
}
