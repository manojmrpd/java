package com.java8.stream.intermediate.skip;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamSkip2 {

    public static void main(String[] args) {

        List<Product> products = CommonService.getProducts();

        int page = 2; // Assuming page size is 5
        int offset = (page - 1) * 5;
        List<Product> currentPageResults = products.stream()
                .skip(offset) // Skip elements for previous pages
                .limit(5) // Get 5 results for the current page
                .collect(Collectors.toList());
        currentPageResults.forEach(product -> System.out.println(product));

    }
}
