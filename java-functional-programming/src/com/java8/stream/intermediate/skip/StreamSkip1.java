package com.java8.stream.intermediate.skip;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamSkip1 {
    public static void main(String[] args) {

        List<String> lines = readLinesFromCSV("data.csv");

        List<String> dataLines = lines.stream()
                .skip(1) // Skip the first line (header)
                .collect(Collectors.toList());
        System.out.println(dataLines);
    }

    private static List<String> readLinesFromCSV(String s) {
        return Arrays.asList("STATUS", "PENDING", "ENFORCED", "CANCELLED");
    }
}
