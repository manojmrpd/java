package com.java8.stream.intermediate.flatMapToLong;

import java.util.Arrays;
import java.util.List;

public class StreamFlatMapToLong {
    public static void main(String[] args) {
        List<String> numbers = Arrays.asList("123", "456", "789");
        long totalCharacterCount = numbers.stream().flatMapToLong(s -> s.chars().mapToLong(Character::charCount)).sum();
        System.out.println(totalCharacterCount);

    }
}
