package com.java8.stream.intermediate.distinct;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamDistinct2 {
    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        List<Product> uniqueProducts = products.stream().distinct().collect(Collectors.toList());
        System.out.println(uniqueProducts);

    }
}
