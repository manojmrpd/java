package com.java8.stream.intermediate.limit;

import com.java8.stream.util.Transaction;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamLimit2 {
    public static void main(String[] args) {
        List<Transaction> transactions = getPendingTransactions();
        List<Transaction> first10ForProcessing = transactions.stream()
                .limit(10) // Process only the first 10 transactions
                .collect(Collectors.toList());
        System.out.println(first10ForProcessing);

    }

    private static List<Transaction> getPendingTransactions() {
        Transaction transaction1 = new Transaction(121, "Read", true, false);
        Transaction transaction2 = new Transaction(122, "Write", true, false);
        Transaction transaction3 = new Transaction(123, "Delete", true, true);
        return Arrays.asList(transaction1, transaction2, transaction3);
    }
}
