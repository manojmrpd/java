package com.java8.stream.intermediate.sorted;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamSorted1 {

    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie");

        List<String> sortedNames = names.stream()
                .sorted() // Sorts in natural order (ascending)
                .collect(Collectors.toList());
        System.out.println(sortedNames);

    }
}
