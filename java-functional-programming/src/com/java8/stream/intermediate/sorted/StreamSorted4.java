package com.java8.stream.intermediate.sorted;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class StreamSorted4 {

	public static void main(String[] args) {

		HashMap<String, String> books = new HashMap<>();
		books.put("111", "economy");
		books.put("444", "polity");
		books.put("333", "history");
		books.put("222", "geography");

		System.out.println("======= Sorting by Key: ========= ");
		// Sorting by Key
		books.entrySet().stream().sorted((o1, o2) -> o1.getKey().compareTo(o2.getKey()))
				.forEach(s -> System.out.println(s));

		System.out.println("======== Sorting by Value: =========");
		// Sorting by Value
		books.entrySet().stream().sorted((o1, o2) -> o1.getValue().compareTo(o2.getValue()))
				.forEach(s -> System.out.println(s));

		System.out.println("======== Sorting Key in Ascending order by Map.Entry Comparing approach: =========");
		books.entrySet().stream().sorted(Map.Entry.comparingByKey()).forEach(System.out::println);
		
		System.out.println("======== Sorting Value in Ascending order by Map.Entry Comparing approach: =========");
		books.entrySet().stream().sorted(Map.Entry.comparingByValue()).forEach(System.out::println);

		HashMap<String, User> users = new HashMap<>();
		users.put("uid121", new User("101", "ramu", "male", 23));
		users.put("uid122", new User("102", "sita", "female", 21));
		users.put("uid123", new User("103", "lakshman", "male", 22));
		users.put("uid124", new User("104", "ravan", "male", 24));

		// sorting value Object based on age in ascending order.
		System.out.println("======== Sorting Value object based on Age in Ascending order: =========");
		users.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.comparing(User::getAge)))
				.forEach(System.out::println);

		// sorting value Object based on age in descending order.
		System.out.println("======== Sorting Value object based on Age in Descending order: =========");
		users.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.comparing(User::getAge).reversed()))
				.forEach(System.out::println);

	}

}
