package com.java8.stream.intermediate.sorted;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class StreamSorted3 {

    public static void main(String[] args) {

        List<User> list = getUsers();
        System.out.println("Original List: " + list.toString());

        // Sorting ascending order
        List<User> sortedAscList = list.stream().sorted((o1, o2) -> o1.getAge().compareTo(o2.getAge()))
                .collect(Collectors.toList());
        System.out.println("Ascending order: " + sortedAscList.toString());

        // Sorting descending order
        List<User> sortedDscList = list.stream().sorted((o1, o2) -> o2.getAge().compareTo(o1.getAge()))
                .collect(Collectors.toList());
        System.out.println("Descending order: " + sortedDscList.toString());

        // sorting for age
        List<User> comparingAge = list.stream().sorted(Comparator.comparing(u -> u.getAge()))
                .collect(Collectors.toList());
        System.out.println("Sorting based on age: " + comparingAge.toString());

        // sorting for name using method reference
        List<User> comparingName = list.stream().sorted(Comparator.comparing(User::getUserName))
                .collect(Collectors.toList());
        System.out.println("Sorting based on name: " + comparingName.toString());

    }

    private static List<User> getUsers() {
        User user1 = new User("101", "ramu", "male", 23);
        User user2 = new User("102", "sita", "female", 21);
        User user3 = new User("103", "lakshman", "male", 22);
        User user4 = new User("104", "ravan", "male", 24);

        return Arrays.asList(user1, user2,
                user3, user4);
    }

}
