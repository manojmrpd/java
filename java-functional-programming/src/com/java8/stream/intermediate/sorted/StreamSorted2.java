package com.java8.stream.intermediate.sorted;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class StreamSorted2 {

    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        // Sorting In Ascending Order
        List<Product> sortedProductsInAscendingOrder =
                products.stream().sorted(Comparator.comparing(Product::getProductName)).collect(Collectors.toList());
        System.out.println(sortedProductsInAscendingOrder);

        // Sorting In Descending Order
        List<Product> sortedProductsInDescendingOrder = products.stream().sorted(Comparator.comparing(Product::getProductName,
                Comparator.reverseOrder())).collect(Collectors.toList());
        System.out.println(sortedProductsInDescendingOrder);
    }
}