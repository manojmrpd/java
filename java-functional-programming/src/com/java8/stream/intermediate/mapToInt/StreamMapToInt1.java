package com.java8.stream.intermediate.mapToInt;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class StreamMapToInt1 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie");
        IntStream intStream = names.stream().mapToInt(String::length);
        double averageLength = intStream.average().getAsDouble();
        System.out.println(averageLength);
    }
}
