package com.java8.stream.intermediate.mapToInt;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.List;

public class StreamMapToInt2 {
    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        int totalInStockProduct = products.stream().mapToInt(Product::getInStock).sum();
        System.out.println(totalInStockProduct);

    }
}
