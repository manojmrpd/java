package com.java8.stream.intermediate.mapToDouble;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.List;

public class StreamMapToDouble {
    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        double totalPriceOfProducts = products.stream().mapToDouble(Product::getPrice).sum();
        System.out.println(totalPriceOfProducts);
    }
}
