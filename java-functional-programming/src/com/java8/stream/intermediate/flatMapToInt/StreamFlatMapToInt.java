package com.java8.stream.intermediate.flatMapToInt;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Order;
import com.java8.stream.util.Product;

import java.util.List;
import java.util.stream.IntStream;

public class StreamFlatMapToInt {
    public static void main(String[] args) {
        List<Order> orders = CommonService.getOrders();
        orders.stream().flatMapToInt(order -> order.getProducts().stream().mapToInt(Product::getInStock)).findAny().ifPresent(value -> System.out.println(value));
    }
}
