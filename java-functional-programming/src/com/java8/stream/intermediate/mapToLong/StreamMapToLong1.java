package com.java8.stream.intermediate.mapToLong;

import java.util.Arrays;
import java.util.List;
import java.util.OptionalLong;
import java.util.stream.LongStream;

public class StreamMapToLong1 {
    public static void main(String[] args) {
        List<String> timestamps = Arrays.asList("2024-03-02T12:34:56", "2024-03-01T10:15:20");
        long count = timestamps.stream().mapToLong(timestamp -> Long.parseLong(timestamp.replace("T",
                "-"))).peek(value -> System.out.println(value)).count();
        System.out.println(count);
    }
}
