package com.java8.stream.intermediate.peek;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamPeek1 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);

        numbers.stream()
                .filter(n -> n % 2 == 0) // Filter even numbers
                .peek(System.out::println) // Print each even number
                .collect(Collectors.toList()); // Collect even numbers (unchanged)

    }
}
