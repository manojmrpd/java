package com.java8.stream.intermediate.peek;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamPeek2 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie");
        names.stream().peek(name -> System.out.println(name)).collect(Collectors.toList());

    }
}
