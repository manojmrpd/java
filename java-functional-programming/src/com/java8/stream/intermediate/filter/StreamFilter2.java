package com.java8.stream.intermediate.filter;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import com.java8.stream.util.Book;
import com.java8.stream.util.CommonService;

public class StreamFilter2 {

    public static void main(String[] args) {

        List<Book> books = CommonService.getBooks();

        Map<Integer, List<Book>> map1 = books.stream().filter(p -> p.getReleaseYear() == 1954)
                .collect(Collectors.groupingBy(Book::getReleaseYear));
        System.out.println(map1.toString());

        Map<String, List<String>> map2 = books.stream().filter(p -> p.getAuthor().contains("George")).collect(
                Collectors.groupingBy(Book::getAuthor, Collectors.mapping(Book::getTitle, Collectors.toList())));
        System.out.println(map2.toString());

        List<Book> bookList = Optional.ofNullable(map1.get(1954)).get();
        System.out.println(bookList.toString());

        List<String> title = Optional.ofNullable(map2.get("George Orwell")).get();
        System.out.println(title.toString());

    }
}
