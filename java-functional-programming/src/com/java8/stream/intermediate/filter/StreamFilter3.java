package com.java8.stream.intermediate.filter;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class StreamFilter3 {
    public static void main(String[] args) {
        List<Product> products = CommonService.getProducts();
        List<Product> expensiveProducts =
                products.stream().filter(product -> product.getPrice() > 500)
                .sorted(Comparator.comparing(Product::getProductName)).collect(Collectors.toList());
        System.out.println(expensiveProducts);

    }
}
