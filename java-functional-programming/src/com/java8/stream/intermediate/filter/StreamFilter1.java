package com.java8.stream.intermediate.filter;

import java.util.HashMap;
import java.util.Map;

public class StreamFilter1 {

    public static void main(String[] args) {

        Map<String, String> map = new HashMap<>();
        map.put("1", "polity");
        map.put("2", "economy");
        map.put("3", "telangana");
        map.put("4", "energy");

        map.entrySet().stream().filter(value -> value.getValue().startsWith("p")).forEach(t -> System.out.println(t));


    }

}
