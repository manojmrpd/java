package com.java8.stream.intermediate.flatMapToDouble;

import com.java8.stream.util.CommonService;
import com.java8.stream.util.Product;

import java.util.Arrays;
import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

public class StreamFlatMapToDouble {
    public static void main(String[] args) {

        //sum()
        List<Product> products = CommonService.getProducts();
        double totalCostOfProducts = products.stream().flatMapToDouble(product -> DoubleStream.of(product.getPrice())).sum();
        System.out.println(totalCostOfProducts);

        //average()
        double averageCostOfProducts =
                products.stream().flatMapToDouble(product -> DoubleStream.of(product.getPrice())).average().getAsDouble();

        //min()
        List<Integer> numbers = Arrays.asList(1, 4, 2, 8, 5);
        double lowestNumber =
                numbers.stream().flatMapToDouble(number -> DoubleStream.of(number)).min().getAsDouble();
        System.out.println(lowestNumber);

        //max()
        double highestNumber =
                numbers.stream().flatMapToDouble(number -> DoubleStream.of(number)).max().getAsDouble();
        System.out.println(highestNumber);
    }
}
